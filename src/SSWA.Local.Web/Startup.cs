﻿using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Autofac.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using System.Security.Claims;
using System.Collections.Generic;
using SSWA.Local.Application.UseCases.FirstLogin;
using SSWA.Local.Application.UseCases.GetUser;
using SSWA.Local.Application.Services;
using SSWA.Local.Core.Users;
using System.Linq;
using Newtonsoft.Json.Serialization;

namespace SSWA.Local.Web
{
    public class Startup
    {
        private ICurrentUserService _currentUserService;
        private IFirstLoginUseCase _firstLoginUseCase;
        private IGetUserUseCase _getUserUseCase;

        public Startup(
            IConfiguration configuration 
            //ICurrentUserService currentUserService,
            //IFirstLoginUseCase firstLoginUseCase,
            //IGetUserUseCase getUserUseCase
            )
        {
            Configuration = configuration;
            _currentUserService = null; //currentUserService;
            _firstLoginUseCase = null; // firstLoginUseCase;
            _getUserUseCase = null; // getUserUseCase;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Add authentication services
            services.AddAuthentication(options => {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
            .AddCookie(options =>
            {
                options.LoginPath = new PathString("/Account/Login");
                options.LogoutPath = new PathString("/Account/Logout");
                options.AccessDeniedPath = new PathString("/");
            })
            .AddOpenIdConnect("Auth0", options => {

                options.SaveTokens = false;

                // Set the authority to your Auth0 domain
                options.Authority = $"https://{Configuration["Auth0:Domain"]}";

                // Configure the Auth0 Client ID and Client Secret
                options.ClientId = Configuration["Auth0:ClientId"];
                        options.ClientSecret = Configuration["Auth0:ClientSecret"];

                // Set response type to code
                options.ResponseType = "code";

                // Configure the scope
                options.Scope.Clear();
                options.Scope.Add("openid");
                options.Scope.Add("profile");

                // Set the callback path, so Auth0 will call back to http://localhost:5000/signin
                // Also ensure that you have added the URL as an Allowed Callback URL in your Auth0 dashboard
                options.CallbackPath = new PathString("/signin");

                // Configure the Claims Issuer to be Auth0
                options.ClaimsIssuer = "Auth0";

                options.Events = new OpenIdConnectEvents
                {
                    OnTicketReceived = (context) =>
                    {
                        return Task.CompletedTask;
                    },
                    OnTokenValidated = async (context) =>
                    {
                        _firstLoginUseCase = context.HttpContext.RequestServices.GetService<IFirstLoginUseCase>();
                        _getUserUseCase = context.HttpContext.RequestServices.GetService<IGetUserUseCase>();

                        var id = context.Principal?.Claims
                            .FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?
                            .Value;

                        string firstName = context.Principal?.Claims
                                .FirstOrDefault(c => c.Type == ClaimTypes.GivenName)?
                                .Value;

                        string lastName = context.Principal?.Claims
                                        .FirstOrDefault(c => c.Type == ClaimTypes.Surname)?
                                        .Value;

                        string name = $"{firstName} {lastName}";


                        await _firstLoginUseCase.Execute(User.Load(id, null, name, new RolesCollection()));

                        User user = await _getUserUseCase.Execute(id);

                        var claims = new List<Claim>();
                        foreach (string role in user.Roles)
                        {
                            claims.Add(new Claim(ClaimTypes.Role, role));
                        }
                        var appIdentity = new ClaimsIdentity(claims);
                        context.Principal.AddIdentity(appIdentity);

                        return;
                    },
                    OnAuthenticationFailed = (context) =>
                    {
                        return Task.CompletedTask;
                    },
                    OnMessageReceived = (context) =>
                    {
                        return Task.CompletedTask;
                    },
                    OnAuthorizationCodeReceived = (context) =>
                    {
                        return Task.CompletedTask;
                    },
                    OnUserInformationReceived = (context) =>
                    {
                        return Task.CompletedTask;
                    },
                    // handle the logout redirection
                    OnRedirectToIdentityProviderForSignOut = (context) =>
                    {
                        var logoutUri = $"https://{Configuration["Auth0:Domain"]}/v2/logout?client_id={Configuration["Auth0:ClientId"]}";

                        var postLogoutUri = context.Properties.RedirectUri;
                        if (!string.IsNullOrEmpty(postLogoutUri))
                        {
                            if (postLogoutUri.StartsWith("/"))
                            {
                                // transform to absolute
                                var request = context.Request;
                                        postLogoutUri = request.Scheme + "://" + request.Host + request.PathBase + postLogoutUri;
                            }
                            logoutUri += $"&returnTo={ Uri.EscapeDataString(postLogoutUri)}";
                        }

                        context.Response.Redirect(logoutUri);
                        context.HandleResponse();

                        return Task.CompletedTask;
                    },
                    OnRedirectToIdentityProvider = (context) =>
                    {
                        context.ProtocolMessage.SetParameter("connection", Configuration["Auth0:Connection"]);
                        return Task.CompletedTask;
                    }
                };
            });


            services
                .AddMvc(config =>
                {
                    var policy = new AuthorizationPolicyBuilder()
                                     .RequireAuthenticatedUser()
                                     .Build();

                    config.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddJsonOptions(options =>
                    options.SerializerSettings.ContractResolver = new DefaultContractResolver())
                .AddFeatureFolders(new OdeToCode.AddFeatureFolders.FeatureFolderOptions() { FeatureFolderName = "UseCases" })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddKendo();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new ConfigurationModule(Configuration));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseKendo(env);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Home}/{id?}");
            });
        }
    }
}
