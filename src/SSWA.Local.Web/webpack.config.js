﻿const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: './main.js',
    output: {
        filename: 'js/bundle.js',
        path: path.resolve(__dirname, 'wwwroot')
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [{ loader: 'style-loader' }, { loader: 'css-loader' }]
            },
            {
                test: /\.scss$/,
                use: ['raw-loader', 'sass-loader']
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        new CopyWebpackPlugin([
            { from: './node_modules/bootstrap/dist/css/*.*', to: 'css/', flatten: true }
        ]),
        new CopyWebpackPlugin([
            { from: './node_modules/@fortawesome/fontawesome-free/css/all.css', to: 'css/', flatten: true }
        ]),
        new CopyWebpackPlugin([
            { from: './node_modules/@fortawesome/fontawesome-free/css/all.*.css', to: 'css/', flatten: true }
        ])
       
    ],
}