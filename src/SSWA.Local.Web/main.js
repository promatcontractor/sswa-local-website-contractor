﻿jQuery = $ = require("jquery");

import "@progress/kendo-ui";
import "@progress/kendo-ui/js/kendo.aspnetmvc";
import '@progress/kendo-ui/js/cultures/kendo.culture.en-AU';
import "@progress/kendo-theme-bootstrap/dist/all.css";
import "./wwwroot/css/kendo.custom.css";
import "popper.js";

window.jQuery = window.$ = kendo.jQuery;
kendo.culture("en-AU");