﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.ManageAreas;
using SSWA.Local.Application.UseCases.ManageChemicalProducts;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.ChemicalProducts;
using SSWA.Local.Core.Projects;

namespace SSWA.Local.Web.UseCases.ManageChemicalProducts
{
    [Authorize]
    [Route("[controller]")]
    public sealed class ChemicalProductsController : Controller
    {
        private readonly IManageChemicalProductsUseCase _useCase;
        private readonly Presenter _presenter;

        public ChemicalProductsController(
            IManageChemicalProductsUseCase useCase,
            Presenter presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet("Manage")]
        public IActionResult Manage()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Get([DataSourceRequest] DataSourceRequest request)
        {
            var projects = await _useCase.Get();

            var viewModelRecords = projects.Select(x => new ViewModel()
            {
                Id = x.Id,
                Chemical = x.Chemical,
                Type = x.Type,
                Supplier = x.Supplier,
                Quantity = x.Quantity
            });

            var result = new DataSourceResult
            {
                Data = viewModelRecords,
                Total = viewModelRecords.Count()
            };

            return Json(result);
        }

        [HttpPost]
        public ActionResult Post(ViewModel record)
        {
            var chemicalProduct = ChemicalProduct.Load(record.Id, record.Chemical, record.Type, record.Supplier, record.Quantity);

            _useCase.Create(chemicalProduct);

            return Json(record);
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, ViewModel record)
        {
            var chemicalProduct = ChemicalProduct.Load(id, record.Chemical, record.Type, record.Supplier, record.Quantity);

            _useCase.Update(chemicalProduct);

            return Json(record);
        }



    }
}