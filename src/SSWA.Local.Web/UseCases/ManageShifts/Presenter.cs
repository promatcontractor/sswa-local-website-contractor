﻿using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageShifts
{
    public sealed class Presenter
    {
        public IActionResult ViewModel { get; private set; }

        public void Populate(ShiftOutput output, Controller controller)
        {

            ViewModel = controller.View(output);
        }
    }
}
