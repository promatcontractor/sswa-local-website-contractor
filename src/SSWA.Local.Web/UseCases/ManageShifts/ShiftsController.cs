﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application;
using SSWA.Local.Application.UseCases.ManageShifts;

namespace SSWA.Local.Web.UseCases.ManageShifts
{
    [Route("[controller]")]
    public class ShiftsController : Controller
    {
        private readonly IManageShiftsUseCase _useCase;
        private readonly Presenter _presenter;

        public ShiftsController(
            IManageShiftsUseCase useCase,
            Presenter presenter)
        {
            this._useCase = useCase;
            this._presenter = presenter;
        }

        [HttpGet(""), Authorize]
        public async Task<IActionResult> ManageShifts()
        {
            var users = await _useCase.GetUsers();
            ViewData["Users"] = users.ToDictionary(t => t.Id, t => t.Name);

            ShiftOutput output = await _useCase.Execute(null, null, null);
            _presenter.Populate(output, this);
            return _presenter.ViewModel;
        }

        [HttpGet("{date}")]
        public async Task<IActionResult> ManageShifts(DateTime? date)
        {
            var users = await _useCase.GetUsers();
            ViewData["Users"] = users.ToDictionary(t => t.Id, t => t.Name);

            ShiftOutput output = await _useCase.Execute(date, null, null);
            _presenter.Populate(output, this);
            return _presenter.ViewModel;
        }

        [HttpGet("{date}/{shiftType}")]
        public async Task<IActionResult> ManageShifts(DateTime? date, string shiftType)
        {
            var users = await _useCase.GetUsers();
            ViewData["Users"] = users.ToDictionary(t => t.Id, t => t.Name);

            ShiftOutput output = await _useCase.Execute(date, shiftType, null);
            _presenter.Populate(output, this);
            return _presenter.ViewModel;
        }

        [HttpGet("{date}/{shiftType}/{operatorId}")]
        public async Task<IActionResult> ManageShifts(DateTime? date, string shiftType, string operatorId)
        {
            var users = await _useCase.GetUsers();
            ViewData["Users"] = users.ToDictionary(t => t.Id, t => t.Name);

            ShiftOutput output = await _useCase.Execute(date, shiftType, operatorId);
            _presenter.Populate(output, this);

            return _presenter.ViewModel;
        }

        [HttpGet("Search")]
        public IActionResult SearchShift([FromQuery] DateTime date, [FromQuery] string shiftType, [FromQuery] string operatorId)
        {
            return RedirectToAction("ManageShifts", new { date = date.ToString("yyyy-MM-dd"), shiftType, operatorId });
        }

        public async Task<IActionResult> Update(ShiftOutput viewModel)
        {
            await _useCase.AddShift(viewModel.Date, viewModel.ShiftType, viewModel.OperatorId, viewModel);

            return RedirectToAction("ManageShifts", new { date = viewModel.Date.ToString("yyyy-MM-dd"), shiftType = viewModel.ShiftType, operatorId = viewModel.OperatorId });
        }
    }
}