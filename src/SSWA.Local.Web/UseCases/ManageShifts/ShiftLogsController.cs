﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application;
using SSWA.Local.Application.UseCases.ManageShifts;

namespace SSWA.Local.Web.UseCases.ManageShifts
{
    [Route("[controller]")]
    public class ShiftLogsController : Controller
    {
        private readonly IManageShiftsUseCase _useCase;
        private readonly Presenter _presenter;

        public ShiftLogsController(
            IManageShiftsUseCase useCase,
            Presenter presenter)
        {
            this._useCase = useCase;
            this._presenter = presenter;
        }


        [HttpGet]
        public async Task<IActionResult> Get(
            [DataSourceRequest] DataSourceRequest request,
            [FromQuery] DateTime date,
            [FromQuery] string shiftType,
            [FromQuery] string operatorId)
        {

            var logs = await _useCase.GetLogs(date, shiftType, operatorId);

            var result = new DataSourceResult
            {
                Data = logs,
                Total = logs.Count()
            };

            return Json(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post(
            [FromQuery] DateTime date, 
            [FromQuery] string shiftType, 
            [FromQuery] string operatorId, 
            ShiftLogOutput viewModel)
        {

            await _useCase.AddLog(date, shiftType, operatorId, viewModel);

            return Json(viewModel);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, ShiftLogOutput viewModel)
        {

            await _useCase.UpdateLog(id, viewModel);

            return Json(viewModel);
        }

        [HttpGet("GetAreas")]
        public async Task<IActionResult> GetAreas()
        {
            var areas = await _useCase.GetAreas();

            return Json(
                areas.Select(x => new KeyValuePair<string, string>(x.Id, x.Name)));
        }

        [HttpGet("GetTags")]
        public async Task<IActionResult> GetTags(string areaId, string tag)
        {
            if (areaId == "N/A") areaId = "";
            
            var areas = await _useCase.GetEquipment(areaId, tag);

            return Json(
                areas.Select(x => new { x.Id, x.Area, x.PID }));
        }

        [HttpGet("GetPIDs")]
        public async Task<IActionResult> GetPIDs(string tag)
        {
            var areas = await _useCase.GetPIDs(tag);

            return Json(
                areas.Select(x => new KeyValuePair<string, string>(x.Id, x.Id)));
        }

    }
}