﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManagePIDs
{
    public sealed class ViewModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public string Revision { get; set; }

        public DateTime DateModified { get; set; }

        public string  Notes { get; set; }

        public int ProjectId { get; set; }
    }
}
