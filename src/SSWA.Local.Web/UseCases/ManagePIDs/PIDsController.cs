﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.ManagePIDs;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.PIDs;
using SSWA.Local.Core.Projects;

namespace SSWA.Local.Web.UseCases.ManagePIDs
{
    [Authorize]
    [Route("[controller]")]
    public sealed class PIDsController : Controller
    {
        private readonly IManagePIDsUseCase _useCase;
        private readonly Presenter _presenter;

        public PIDsController(
            IManagePIDsUseCase useCase,
            Presenter presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet("Manage")]
        public async Task<IActionResult> Manage()
        {
            var projects = await _useCase.GetProjects();
            ViewData["Projects"] = projects.ToDictionary(p => p.Id, p => p.Name);
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Get([DataSourceRequest] DataSourceRequest request)
        {
            var projects = await _useCase.Get();

            var viewModelRecords = projects.Select(x => new ViewModel()
            {
                Id = x.Id,
                Name = x.PIDTitle,
                Revision = x.Revision,
                DateModified = x.DateModified,
                Notes = x.Notes,
                ProjectId = x.ProjectId
            });

            var result = new DataSourceResult
            {
                Data = viewModelRecords,
                Total = viewModelRecords.Count()
            };

            return Json(result);
        }

        [HttpPost]
        public ActionResult Post(ViewModel record)
        {
            var pid = PID.Load(record.Id, record.Name, record.Revision, record.DateModified, record.Notes, record.ProjectId);

            _useCase.Create(pid);

            return Json(record);
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, ViewModel record)
        {
            var pid = PID.Load(record.Id, record.Name, record.Revision, record.DateModified, record.Notes, record.ProjectId);

            _useCase.Update(pid);

            return Json(record);
        }



    }
}