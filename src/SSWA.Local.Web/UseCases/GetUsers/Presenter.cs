﻿using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.GetUsers
{
    public class Presenter
    {
        public IActionResult ViewModel { get; private set; }

        public void Populate(IReadOnlyCollection<User> output, Controller controller)
        {

            ViewModel = controller.View(output);
        }
    }
}
