﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageChemicalDeliveries
{
    [Authorize]
    [Route("[controller]")]
    public class ChemicalDeliveriesController : Controller
    {
        [HttpGet("Manage")]
        public async Task<IActionResult> Manage()
        {
            return View();
        }
    }
}
