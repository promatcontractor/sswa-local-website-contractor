﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.CreateProject;
using SSWA.Local.Core.Projects;

namespace SSWA.Local.Web.UseCases.ManageProjects
{
    [Authorize]
    [Route("[controller]")]
    public sealed class ProjectsController : Controller
    {
        private readonly IManageProjectsUseCase _createProjectUseCase;
        private readonly Presenter _presenter;

        public ProjectsController(
            IManageProjectsUseCase createProjectUseCase,
            Presenter presenter)
        {
            _createProjectUseCase = createProjectUseCase;
            _presenter = presenter;
        }

        [HttpGet("Manage")]
        public IActionResult Manage()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Get([DataSourceRequest] DataSourceRequest request)
        {
            var projects = await _createProjectUseCase.Get();

            var viewModelRecords = projects.Select(x => new ViewModel()
            {
                Id = x.ProjectId,
                Name = x.Name
            });

            var result = new DataSourceResult
            {
                Data = viewModelRecords,
                Total = viewModelRecords.Count()
            };

            return Json(result);
        }

        [HttpPost]
        public ActionResult Post(ViewModel record)
        {
            var project = Project.Load(record.Id, record.Name);

            _createProjectUseCase.Create(project);

            return Json(record);
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, ViewModel record)
        {
            var project = Project.Load(id, record.Name);

            _createProjectUseCase.Update(project);

            return Json(record);
        }



    }
}