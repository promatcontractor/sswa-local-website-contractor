﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageEquipment
{
    public sealed class ViewModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public string Prefix { get; set; }

        public string INum { get; set; }

        public string Type { get; set; }

        public string OriginalTag { get; set; }

        public string Area { get; set; }

        public int ProjectId { get; set; }

        public string PID { get; set; }

        public string Notes { get; set; }
    }
}
