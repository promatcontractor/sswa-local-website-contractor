﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.ManageEquipment;
using SSWA.Local.Core.Equipment;

namespace SSWA.Local.Web.UseCases.ManageEquipment
{
    [Authorize]
    [Route("[controller]")]
    public sealed class EquipmentController : Controller
    {
        private readonly IManageEquipmentUseCase _useCase;
        private readonly Presenter _presenter;

        public EquipmentController(
            IManageEquipmentUseCase useCase,
            Presenter presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet("Manage")]
        public async Task<IActionResult> Manage()
        {
            var projects = await _useCase.GetProjects();
            ViewData["Projects"] = projects.ToDictionary(p => p.Id, p => p.Name);

            var areas = await _useCase.GetAreas();
            ViewData["Areas"] = areas.ToDictionary(y => y.Id, y => y.Name);

            var pids = await _useCase.GetPIDs();
            ViewData["PIDs"] = pids.ToDictionary(y => y.Id, y => y.Id);

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Get([DataSourceRequest] DataSourceRequest request)
        {
            var equipment = await _useCase.Get(request.Page, request.PageSize);

            var count = await _useCase.Count();

            var viewModelRecords = equipment.Select(x => new ViewModel()
            {
                Id = x.Id,
                Name = x.Description,
                Prefix = x.Prefix,
                Type = x.Type,
                INum = x.Inum,
                Notes = x.Notes,
                OriginalTag = x.TagOriginal,
                ProjectId = x.ProjectId,
                Area = x.Area,
                PID = x.PID
            });


            var result = new DataSourceResult
            {
                Data = viewModelRecords,
                Total = count
            };

            return Json(result);
        }

        [HttpPost]
        public ActionResult Post(ViewModel record)
        {
            var equipment = Equipment.Load(record.Id, record.Name, record.Prefix, record.INum, record.Type, record.Notes, record.OriginalTag, record.Area, record.PID, record.ProjectId);

            _useCase.Create(equipment);

            return Json(record);
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, ViewModel record)
        {
            //var project = Equipment.Load(id, record.Name);

            var equipment = Equipment.Load(record.Id, record.Name, record.Prefix, record.INum, record.Type, record.Notes, record.OriginalTag, record.Area, record.PID, record.ProjectId);

            _useCase.Update(equipment);

            return Json(record);
        }



    }
}