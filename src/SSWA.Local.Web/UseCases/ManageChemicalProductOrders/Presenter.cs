﻿using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application;
using SSWA.Local.Application.UseCases.CreateProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageChemicalProductOrders
{
    public sealed class Presenter
    {
        public IActionResult ViewModel { get; private set; }

        public void Populate(ProjectOutput response)
        {

            if (response == null)
            {
                ViewModel = new NoContentResult();
                return;
            }

            ViewModel = new RedirectToActionResult("", "", null);
        }

    }
}
