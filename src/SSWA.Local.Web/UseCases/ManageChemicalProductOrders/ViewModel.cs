﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageChemicalProductOrders
{
    public sealed class ViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int ChemicalProductId { get; set; }

        [Required]
        [StringLength(64)]
        public string Status { get; set; }

        public DateTime? ExpectedDeliveryDate { get; set; }

        public DateTime? ExpectedDeliveryTime { get; set; }

        public string ArrivalAcknowledgedById { get; set; }

        public DateTime? IdentifiedAt { get; set; }

        public DateTime? OrderedAt { get; set; }

        public DateTime? ReceivedAt { get; set; }

        public string Comments { get; set; }
    }
}
