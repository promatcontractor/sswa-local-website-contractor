﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.ManageAreas;
using SSWA.Local.Application.UseCases.ManageChemicalProductOrders;
using SSWA.Local.Application.UseCases.ManageChemicalProducts;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.ChemicalProducts;
using SSWA.Local.Core.Projects;
using SSWA.Local.Web.UseCases.Shared;

namespace SSWA.Local.Web.UseCases.ManageChemicalProductOrders
{
    [Authorize]
    [Route("[controller]")]
    public sealed class ChemicalOrdersController : Controller
    {
        private readonly IManageChemicalProductOrdersUseCase _useCase;
        private readonly Presenter _presenter;

        public ChemicalOrdersController(
            IManageChemicalProductOrdersUseCase useCase,
            Presenter presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet("Manage")]
        public async Task<IActionResult> Manage()
        {
            var users = await _useCase.GetUsers();
            ViewData["Users"] = users.ToDictionary(t => t.Id, t => t.Name);

            var chemicalProducts = await _useCase.GetChemicalProducts();
            ViewData["ChemicalProducts"] = chemicalProducts.ToDictionary(t => t.Id, t => t.Chemical);

            var statuses = new List<string>()
            {
                ChemicalProductOrderStatus.REQUIRED,
                ChemicalProductOrderStatus.ORDERED,
                ChemicalProductOrderStatus.RECEIVED,
                ChemicalProductOrderStatus.CANCELLED
            };
            ViewData["Statuses"] = statuses.ToDictionary(t => t, t => t);

            return View();
        }

        [HttpGet("History")]
        public async Task<IActionResult> History([DataSourceRequest] DataSourceRequest request)
        {
            var sqlCommand = new SqlCommand();
            var whereClause = request.Filters.ToSqlWhereClause(command:sqlCommand);
            var orderByClause = request.Sorts.ToSqlOrderByClause();

            var orders = await _useCase.GetHistory(sqlCommand.Parameters, whereClause, orderByClause, request.Page, request.PageSize);
            var orderCount = await _useCase.GetHistoryCount(sqlCommand.Parameters, whereClause);

            var viewModelRecords = orders.Select(x => new ViewModel()
            {
                Id = x.Id,
                ChemicalProductId = x.ChemicalProductId,
                Status = x.Status,
                ExpectedDeliveryDate = x.ExpectedDeliveryDate,
                ExpectedDeliveryTime = x.ExpectedDeliveryTime,
                ArrivalAcknowledgedById = x.ArrivalAcknowledgedById,
                IdentifiedAt = x.IdentifiedAt,
                OrderedAt = x.OrderedAt,
                ReceivedAt = x.ReceivedAt,
                Comments = x.Comments
            });

            var result = new DataSourceResult
            {
                Data = viewModelRecords,
                Total = orderCount
            };

            return Json(result);
        }

        [HttpGet]
        public async Task<ActionResult> Get([DataSourceRequest] DataSourceRequest request)
        {
            var projects = await _useCase.GetOpen();

            var viewModelRecords = projects.Select(x => new ViewModel()
            {
                Id = x.Id,
                ChemicalProductId = x.ChemicalProductId,
                Status = x.Status,
                ExpectedDeliveryDate = x.ExpectedDeliveryDate,
                ExpectedDeliveryTime = x.ExpectedDeliveryTime,
                ArrivalAcknowledgedById = x.ArrivalAcknowledgedById,
                IdentifiedAt = x.IdentifiedAt,
                OrderedAt = x.OrderedAt,
                ReceivedAt = x.ReceivedAt,
                Comments = x.Comments
            });

            var result = new DataSourceResult
            {
                Data = viewModelRecords,
                Total = viewModelRecords.Count()
            };

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> Post(ViewModel record)
        {
            var order = ChemicalProductOrder.Load
                (record.Id, record.ChemicalProductId, record.Status, record.ExpectedDeliveryDate, record.ExpectedDeliveryTime,
                record.ArrivalAcknowledgedById, record.IdentifiedAt, record.OrderedAt, record.ReceivedAt, record.Comments);

            var newOrder = await _useCase.Create(order);

            return Json(newOrder);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, ViewModel record)
        {
            var order = ChemicalProductOrder.Load
                (record.Id, record.ChemicalProductId, record.Status, record.ExpectedDeliveryDate, record.ExpectedDeliveryTime,
                record.ArrivalAcknowledgedById, record.IdentifiedAt, record.OrderedAt, record.ReceivedAt, record.Comments);

            await _useCase.Update(order);

            return Json(record);
        }



    }
}