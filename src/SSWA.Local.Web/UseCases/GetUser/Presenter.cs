﻿using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.GetUser
{
    public class Presenter
    {
        public IActionResult ViewModel { get; private set; }

        public void Populate(User output, Controller controller)
        {
            var roles = new Dictionary<string, bool>();
            roles.Add("Operator", output.Roles.Contains("operator"));
            roles.Add("Admin", output.Roles.Contains("admin"));
            roles.Add("Approver", output.Roles.Contains("approver"));

            UserOutput userOutput = new UserOutput(output.Id, output.Name, roles);

            controller.ViewBag.Roles = roles;
            ViewModel = controller.View(userOutput);
        }
    }
}
