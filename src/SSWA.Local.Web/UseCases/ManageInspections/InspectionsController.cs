﻿using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application;
using SSWA.Local.Application.UseCases.ManageInspections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageInspections
{
    [Route("[controller]")]
    public class InspectionsController : Controller
    {
        private readonly IManageInspectionsUseCase _useCase;
        private readonly Presenter _presenter;

        public InspectionsController(
            IManageInspectionsUseCase useCase,
            Presenter presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet(""), Authorize]
        public async Task<IActionResult> ManageInspections()
        {
            var projects = await _useCase.GetProjects();
            var areas = await _useCase.GetAreas();
            ViewData["Projects"] = projects.ToDictionary(t => t.Name, t => t.Name);
            ViewData["Areas"] = areas.ToDictionary(t => t.Id, t => t.Name);

            InspectionOutput output = await _useCase.Execute(null, null, null);
            _presenter.Populate(output, this);
            return _presenter.ViewModel;
        }

        [HttpGet("{date}")]
        public async Task<IActionResult> ManageInspections(DateTime? date)
        {
            var projects = await _useCase.GetProjects();
            var areas = await _useCase.GetAreas();
            ViewData["Projects"] = projects.ToDictionary(t => t.Name, t => t.Name);
            ViewData["Areas"] = areas.ToDictionary(t => t.Id, t => t.Name);

            InspectionOutput output = await _useCase.Execute(date, null, null);
            _presenter.Populate(output, this);
            return _presenter.ViewModel;
        }

        [HttpGet("{date}/{shiftType}")]
        public async Task<IActionResult> ManageInspections(DateTime? date, string shiftType)
        {
            var projects = await _useCase.GetProjects();
            var areas = await _useCase.GetAreas();
            ViewData["Projects"] = projects.ToDictionary(t => t.Name, t => t.Name);
            ViewData["Areas"] = areas.ToDictionary(t => t.Id, t => t.Name);

            InspectionOutput output = await _useCase.Execute(date, shiftType, null);
            _presenter.Populate(output, this);
            return _presenter.ViewModel;
        }

        [HttpGet("{date}/{shiftType}/{filter}")]
        public async Task<IActionResult> ManageInspections(DateTime? date, string shiftType, string filter)
        {
            var projects = await _useCase.GetProjects();
            var areas = await _useCase.GetAreas(filter.Split(',')[0]);
            ViewData["Projects"] = projects.ToDictionary(t => t.Name, t => t.Name);
            ViewData["Areas"] = areas.ToDictionary(t => t.Id, t => t.Name);

            InspectionOutput output = await _useCase.Execute(date, shiftType, filter);
            _presenter.Populate(output, this);

            return _presenter.ViewModel;
        }

        [HttpGet("Search")]
        public IActionResult SearchShift([FromQuery] DateTime date, [FromQuery] string shiftType, [FromQuery] string project, [FromQuery] string areaId)
        {
            var filter = $"{project},{areaId}";
            return RedirectToAction("ManageInspections", new { date = date.ToString("yyyy-MM-dd"), shiftType, filter });
        }


    }
}
