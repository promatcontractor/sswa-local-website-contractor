﻿using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application;
using SSWA.Local.Application.UseCases.ManageInspections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageInspections
{
    public class InspectionLogsController : Controller
    {
        private readonly IManageInspectionsUseCase _useCase;

        public InspectionLogsController(IManageInspectionsUseCase useCase)
        {
            _useCase = useCase;
        }

        [HttpGet]
        public async Task<IActionResult> Get(
        [DataSourceRequest] DataSourceRequest request,
        [FromQuery] DateTime date,
        [FromQuery] string shiftType,
        [FromQuery] string project,
        [FromQuery] string areaId)
        {
            var inspections = await _useCase.GetLogs(date, shiftType, project, areaId);

            var result = new DataSourceResult
            {
                Data = inspections,
                Total = inspections.Count()
            };

            return Json(result);
        }

        [HttpPut]
        public async Task<IActionResult> Put(
            [FromQuery] DateTime date,
            [FromQuery] string shiftType,
            [FromQuery] int itemId,
            [FromBody] InspectionLogOutput viewModel)
        {

            await _useCase.Update(date, shiftType, itemId, viewModel);

            return Json(viewModel);
        }
    }
}

