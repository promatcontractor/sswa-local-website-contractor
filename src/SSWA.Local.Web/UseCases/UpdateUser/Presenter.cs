﻿using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.UpdateUser
{
    public class Presenter
    {
        public IActionResult ViewModel { get; private set; }

        public void Populate(User output, Controller controller)
        {
            ViewModel = new RedirectToActionResult("GetUsers", "Users", null);

        }
    }
}
