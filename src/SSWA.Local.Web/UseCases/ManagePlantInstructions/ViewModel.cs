﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManagePlantInstructions
{
    public sealed class ViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [StringLength(64)]
        public string Status { get; set; }

        public int? Duration { get; set; }

        public string DurationUnits { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public string OperatorUserId { get; set; }

        [Required]
        public int InstructionFromRoleId { get; set; }

        [Required]
        public string AuthorisedByUserId { get; set; }
    }
}
