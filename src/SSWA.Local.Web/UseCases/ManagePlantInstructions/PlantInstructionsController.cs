﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.ManagePlantInstructions;
using SSWA.Local.Core.PlantInstructions;
using SSWA.Local.Web.UseCases.Shared;

namespace SSWA.Local.Web.UseCases.ManagePlantInstructions
{
    [Authorize]
    [Route("[controller]")]
    public sealed class PlantInstructionsController : Controller
    {
        private readonly IManagePlantInstructionsUseCase _useCase;
        private readonly Presenter _presenter;

        public PlantInstructionsController(
            IManagePlantInstructionsUseCase useCase,
            Presenter presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet("Manage")]
        public async Task<IActionResult> Manage()
        {
            var users = await _useCase.GetUsers();
            ViewData["Users"] = users.ToDictionary(t => t.Id, t => t.Name);

            var positions = await _useCase.GetPositions();
            ViewData["Positions"] = positions.ToDictionary(t => t.Id, t => t.Name);

            var statuses = new List<string>()
            {
                PlantInstructionStatus.ACTIVE,
                PlantInstructionStatus.INACTIVE,
            };
            ViewData["Statuses"] = statuses.ToDictionary(t => t, t => t);

            var units = new List<string>()
            {
                PlantInstructionDurationUnit.HOURS,
                PlantInstructionDurationUnit.DAYS,
                PlantInstructionDurationUnit.WEEKS,
                PlantInstructionDurationUnit.MONTHS,
                PlantInstructionDurationUnit.YEARS
            };
            ViewData["Units"] = units.ToDictionary(t => t, t => t);

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Get([DataSourceRequest] DataSourceRequest request)
        {
            var sqlCommand = new SqlCommand();
            var whereClause = request.Filters.ToSqlWhereClause(command:sqlCommand);
            var orderByClause = request.Sorts.ToSqlOrderByClause();

            var orders = await _useCase.GetAll(sqlCommand.Parameters, whereClause, orderByClause);

            var viewModelRecords = orders.Select(x => new ViewModel()
            {
                Id = x.Id,
                Title = x.Title,
                Description = x.Description,
                Status = x.Status,
                Duration = x.Duration,
                DurationUnits = x.DurationUnits,
                DateCreated = x.DateCreated,
                OperatorUserId = x.OperatorUserId,
                InstructionFromRoleId = x.InstructionFromRoleId,
                AuthorisedByUserId = x.AuthorisedByUserId
            });

            var result = new DataSourceResult
            {
                Data = viewModelRecords,
                Total = viewModelRecords.Count()
            };

            return Json(result);
        }


        [HttpPost]
        public ActionResult Post(ViewModel record)
        {
            var instruction = PlantInstruction.Load
                (record.Id, record.Title, record.Description, record.Status, record.Duration, record.DurationUnits,
                record.DateCreated, record.OperatorUserId, record.InstructionFromRoleId, record.AuthorisedByUserId);

            _useCase.Create(instruction);

            return Json(record);
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, ViewModel record)
        {
            var instruction = PlantInstruction.Load
                 (record.Id, record.Title, record.Description, record.Status, record.Duration, record.DurationUnits,
                 record.DateCreated, record.OperatorUserId, record.InstructionFromRoleId, record.AuthorisedByUserId);

            _useCase.Update(instruction);

            return Json(record);
        }



    }
}