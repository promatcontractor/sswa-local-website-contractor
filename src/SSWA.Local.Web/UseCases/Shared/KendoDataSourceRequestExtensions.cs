﻿using Kendo.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.Shared
{
    public static class KendoDataSourceRequestExtensions
    {
        public static string ToSqlOrderByClause(this IList<SortDescriptor> sorts)
        {

            string orderClause = "";
            List<string> orderedMembers = new List<string>();

            string comma = "";
            if (sorts != null && sorts.Count > 0)
            {
                foreach (var sort in sorts)
                {
                    if (orderedMembers.Contains(sort.Member)) continue;

                    orderClause += comma
                        + sort.Member
                        + ((sort.SortDirection == ListSortDirection.Ascending) ? " ASC" : " DESC");

                    comma = ",";
                    orderedMembers.Add(sort.Member);
                }
            }
            else
            {
                orderClause = "Id ASC";
            }

            if (orderClause.Length > 0) orderClause = " ORDER BY " + orderClause;
            return orderClause;
        }


        public static string ToSqlWhereClause(this IList<IFilterDescriptor> filters, FilterCompositionLogicalOperator compositionOperator = FilterCompositionLogicalOperator.And, SqlCommand command = null, bool outermostFilter = true)
        {
            if (filters.Count == 0) return "";

            string result = "(";
            string combineWith = "";

            foreach (var filter in filters)
            {
                if (filter is FilterDescriptor fd)
                {
                    result +=
                        combineWith + "("
                        + DescriptorToSqlServerQuery(fd, command)
                        + ")"
                        ;
                }
                else if (filter is CompositeFilterDescriptor cfd)
                {
                    result +=
                        combineWith + "("
                        + ToSqlWhereClause(cfd.FilterDescriptors, cfd.LogicalOperator, command, false)
                        + ")"
                        ;
                }

                combineWith =
                    (compositionOperator == FilterCompositionLogicalOperator.And)
                    ? " and "
                    : " or "
                    ;
            }

            result += ")";

            if (result.Length > 0 && outermostFilter) result = " WHERE " + result;
            return result;
        }

        private static string DescriptorToSqlServerQuery(FilterDescriptor fd, SqlCommand command)
        {
            string parameterName = "@PARAMETER" + command.Parameters.Count;
            string result;

            // Some string filter values are modified for use as parameteres in a SQL LIKE clause, thus work with a copy.
            // The original value must remain unchanged for when ToDataSourceResult(request) is used later.

            Object filterValue = fd.Value;

            switch (fd.Operator)
            {
                case FilterOperator.IsLessThan: result = "[" + fd.Member + "]" + " < " + parameterName; break;
                case FilterOperator.IsLessThanOrEqualTo: result = "[" + fd.Member + "]" + " <= " + parameterName; break;
                case FilterOperator.IsEqualTo:
                    if (fd.Value is DateTime)
                    {
                        result = "cast([" + fd.Member + "] as date) = " + parameterName;
                    }
                    else
                    {
                        result = "[" + fd.Member + "] = " + parameterName;
                    }
                    break;
                case FilterOperator.IsNotEqualTo: result = "[" + fd.Member + "]" + " <> " + parameterName; break;
                case FilterOperator.IsGreaterThanOrEqualTo: result = "[" + fd.Member + "]" + " >= " + parameterName; break;
                case FilterOperator.IsGreaterThan: result = "[" + fd.Member + "]" + " > " + parameterName; break;
                case FilterOperator.StartsWith:
                    filterValue = fd.Value.ToString().ToSqlSafeLikeData() + "%";
                    result = "[" + fd.Member + "]" + " like " + parameterName; break;
                case FilterOperator.EndsWith:
                    filterValue = "%" + fd.Value.ToString().ToSqlSafeLikeData();
                    result = "[" + fd.Member + "]" + " like " + parameterName; break;
                case FilterOperator.Contains:
                    filterValue = "%" + fd.Value.ToString().ToSqlSafeLikeData() + "%";
                    result = "[" + fd.Member + "]" + " like " + parameterName; break;
                case FilterOperator.IsContainedIn:
                    throw new Exception("There is no translator for [" + fd.Member + "]" + " " + fd.Operator + " " + fd.Value);
                case FilterOperator.DoesNotContain:
                    filterValue = "%" + fd.Value.ToString().ToSqlSafeLikeData();
                    result = "[" + fd.Member + "]" + " not like " + parameterName; break;
                case FilterOperator.IsNull: result = "[" + fd.Member + "]" + " IS NULL"; break;
                case FilterOperator.IsNotNull: result = "[" + fd.Member + "]" + " IS NOT NULL"; break;
                case FilterOperator.IsEmpty: result = "[" + fd.Member + "]" + " = ''"; break;
                case FilterOperator.IsNotEmpty: result = "[" + fd.Member + "]" + " <> ''"; break;
                default:
                    throw new Exception("There is no translator for [" + fd.Member + "]" + " " + fd.Operator + " " + fd.Value);
            }

            command.Parameters.Add(new SqlParameter(parameterName, filterValue));

            return result;
        }

        private static string ToSqlSafeLikeData(this string val)
        {
            return Regex.Replace(val, @"([%_\[])", @"[$1]").Replace("'", "''");
        }

        private static string ToSqlSafeString(this string val)
        {
            return "'" + val.Replace("'", "''") + "'";
        }
    }
}
