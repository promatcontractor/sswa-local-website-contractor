﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageShifts
{
    public class ShiftNavigationTagHelper : TagHelper
    {

        public DateTime Date { get; set; }
        public string Shift { get; set; }
        public string FilterId { get; set; }
        public string Controller { get; set; }
        
        public string Position { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var address = $"/{Controller}/{Date.Date.ToString("yyyy-MM-dd")}/{Shift}/{FilterId}";
            var date = Date.Date.ToString("dd MMM").ToUpper();
            var icon = Shift.ToLowerInvariant() == "day" ? "sun" : "moon";
            var bg = Shift.ToLowerInvariant() == "day" ? "warning" : "primary";
            var text = Shift.ToLowerInvariant() == "day" ? "light" : "light";
            var shiftText = Shift.ToLowerInvariant() == "day" ? "dark" : "light";
            var shiftLabel = Shift.ToUpper();

            output.TagName = "div";



            if (Position.ToLowerInvariant() == "left")
            {
                output.Attributes.Add("class", "d-flex h-100");

                output.Content.SetHtmlContent($@"
                    <a class=""btn btn-lg btn-outline-secondary justify-content-center align-self-center"" href=""{address}"">
                        <div class=""row d-flex h-100"">
                            <div class=""col-4 justify-content-center align-self-center"">
                                <i class=""fas fa-angle-left""></i>
                            </div>
                            <div class=""col-8 justify-content-center border-left"">
                                {date}<br />
                                <small class=""bg-{bg} text-{shiftText} rounded py-1 px-2""><i class=""far fa-{icon}""></i> {shiftLabel}</small>                            
                            </div>
                        </div>
                    </a>
                ");
            }
            else 
            {
                output.Attributes.Add("class", "d-flex h-100 flex-row-reverse");


                output.Content.SetHtmlContent($@"
                    <a class=""btn btn-lg btn-outline-secondary justify-content-center align-self-center"" href=""{address}"">
                        <div class=""row d-flex h-100"">
                            <div class=""col-8 justify-content-center border-right"">
                                {date}<br />
                                <small class=""bg-{bg} text-{shiftText} r rounded py-1 px-2""><i class=""far fa-{icon}""></i> {shiftLabel}</small>                            
                            </div>
                            <div class=""col-4 justify-content-center align-self-center"">
                                <i class=""fas fa-angle-right""></i>
                            </div>

                        </div>
                    </a>
                ");
            }
          
            output.TagMode = TagMode.StartTagAndEndTag;

            base.Process(context, output);
        }
    }
}
