﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageLab
{
    public sealed class ViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [StringLength(64)]
        public string Chemical { get; set; }

        [Required]
        [StringLength(255)]
        public string Type { get; set; }

        [StringLength(255)]
        public string Supplier { get; set; }

        [Required]
        [StringLength(64)]
        public string Quantity { get; set; }
    }
}
