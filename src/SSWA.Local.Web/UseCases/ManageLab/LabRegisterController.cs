﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Application.Services;
using SSWA.Local.Application.UseCases.ManageAreas;
using SSWA.Local.Application.UseCases.ManageChemicalProducts;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.ChemicalProducts;
using SSWA.Local.Core.Lab;
using SSWA.Local.Infrastructure;
using SSWA.Local.Web.UseCases.Shared;

namespace SSWA.Local.Web.UseCases.ManageLab
{
    [Authorize]
    [Route("[controller]")]
    public sealed class LabRegisterController : Controller
    {
        private readonly ILabSampleTemplateRepository _templateRepo;
        private readonly ILabSampleRepository _sampleRepo;
        private readonly IManageLabUseCase _labData;
        private readonly ICurrentUserService _userService;
        private readonly IHostingEnvironment _hostingEnvironment;

        public LabRegisterController(
            ILabSampleTemplateRepository templateRepo,
            ILabSampleRepository sampleRepo,
            IManageLabUseCase labData,
            ICurrentUserService userService,
            IHostingEnvironment hostingEnvironment
            )
        {
            _templateRepo = templateRepo;
            _sampleRepo = sampleRepo;
            _labData = labData;
            _userService = userService;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet("Manage/Templates")]
        public async Task<IActionResult> ManageTemplates()
        {
            var locations = await _labData.GetLocations();
            ViewData["Locations"] = locations.ToDictionary(p => p.Id, p => p.Location);

            var units = await _labData.GetUnits();
            ViewData["Units"] = units.ToDictionary(y => y.Id, y => y.Unit);

            var sampleTypes = await _labData.GetSampleTypes();
            ViewData["SampleTypes"] = sampleTypes.ToDictionary(y => y.Name, y => y.Name);

            var analytes = await _labData.GetAnalytes();
            ViewData["Analytes"] = analytes.ToDictionary(y => y.Id, y => y.Symbol);

            ViewData["DefaultAnalyteUnits"] = analytes.ToDictionary(y => y.Id, y => y.UnitId.ToString());


            return View("Templates");
        }

        [HttpGet("Manage/Samples")]
        public async Task<IActionResult> ManageSamples()
        {

            var locations = await _labData.GetLocations();
            ViewData["Locations"] = locations.ToDictionary(p => p.Id, p => p.Location);

            var units = await _labData.GetUnits();
            ViewData["Units"] = units.OrderBy(x => x.Unit).ToDictionary(y => y.Id, y => y.Unit);

            var areas = await _labData.GetAreas();
            ViewData["Areas"] = areas.ToDictionary(y => y.Id, y => y.Name);

            var analytes = await _labData.GetAnalytes();
            ViewData["Analytes"] = analytes.OrderBy(x => x.Title).ToDictionary(y => y.Id, y => y.Symbol);

            ViewData["DefaultAnalyteUnits"] = analytes.ToDictionary(y => y.Id, y => y.UnitId.ToString());

            var sampleTypes = await _labData.GetSampleTypes();
            ViewData["SampleTypes"] = sampleTypes.ToDictionary(y => y.Name, y => y.Name);

            var users = await _labData.GetUsers();
            ViewData["Users"] = users.OrderBy(x => x.Name).ToDictionary(y => y.Id, y => y.Name);

            var labs = await _labData.GetLaboratories();
            ViewData["Labs"] = labs.OrderBy(x => x.Name).ToDictionary(y => y.Id, y => y.Name);

            return View("Samples");
        }

        [HttpPost("Manage/Samples")]
        public async Task<IActionResult> ManageSamples(string type, int templateId)
        {
            var user = _userService.GetUser();
            var labs = await _labData.GetLaboratories();
            var onsiteLab = labs.FirstOrDefault(x => x.Name?.ToLowerInvariant()?.Contains("onsite") == true);

            var sampleHeader = new LabSampleHeader()
            {
                SampleDateTime = DateTime.Now,
                SampledBy = user.Id,
                AnalysedBy = user.Id,
                LaboratoryId = onsiteLab?.Id,
            };

            if (templateId != 0)
            {
                var header = await _templateRepo.Get(templateId);
                var details = await _templateRepo.GetDetails(templateId);

                sampleHeader = new LabSampleHeader()
                {
                    SampleDateTime = DateTime.Now,
                    SampledBy = user.Id,
                    AnalysedBy = user.Id,
                    External = header.External,
                    LaboratoryId = header.External ? null : onsiteLab?.Id,
                    TemplateId = templateId,
                    LocationId = header.LocationId,
                    SampleType = header.SampleType,
                };

                sampleHeader = await _sampleRepo.Add(sampleHeader);

                foreach (var detail in details)
                {
                    var sampleDetail = new LabSampleDetail()
                    {
                        SampleId = sampleHeader.Id,
                        AnalyserId = detail.AnalyserId,
                        AnalyteId = detail.AnalyteId,
                        UnitId = detail.UnitId,
                        LowerLimit = detail.ExpectedMinimum,
                        UpperLimit = detail.ExpectedMaximum,
                        TemplateDetailId = detail.Id
                    };

                    await _sampleRepo.AddDetail(sampleDetail);

                }
            }
            else
            {
                sampleHeader = await _sampleRepo.Add(sampleHeader);

            }

            return RedirectToAction("ManageSamples", new { id = sampleHeader.Id });
        }

        [HttpGet("GetSamples")]
        public async Task<ActionResult> GetSamples([DataSourceRequest] DataSourceRequest request)
        {
            var sqlCommand = new SqlCommand();
            var whereClause = request.Filters.ToSqlWhereClause(command: sqlCommand);
            var orderByClause = request.Sorts.ToSqlOrderByClause();

            var templates = await _sampleRepo.GetList(sqlCommand.Parameters, whereClause, orderByClause, request.Page, request.PageSize);
            var templateCount = await _sampleRepo.GetListCount(sqlCommand.Parameters, whereClause);

            var result = new DataSourceResult
            {
                Data = templates,
                Total = templateCount
            };

            return Json(result);
        }

        [HttpPut("PutSample/{id}")]
        public async Task<ActionResult> PutSample(int id, LabSampleHeader record)
        {
            await _sampleRepo.Update(record);
            return Json(new { Data = record });
        }

        [HttpDelete("DeleteSample/{id}")]
        public async Task<ActionResult> DeleteSample(int id, LabSampleHeader record)
        {
            await _sampleRepo.Delete(record);
            return Json(new { Data = record });
        }

        [HttpGet("GetSampleDetails")]
        public async Task<ActionResult> GetSampleDetails([FromQuery] int sampleId, [DataSourceRequest] DataSourceRequest request)
        {
            var templates = await _sampleRepo.GetDetails(sampleId);

            var result = new DataSourceResult
            {
                Data = templates,
                Total = templates.Count
            };

            return Json(result);
        }

        [HttpPost("PostSampleDetail")]
        public async Task<ActionResult> PostSampleDetail([FromQuery] int sampleId, LabSampleDetail record)
        {
            if (record.SampleId == 0)
                record.SampleId = sampleId;

            record = await _sampleRepo.AddDetail(record);
            return Json(new { Data = record });
        }

        [HttpPut("PutSampleDetail/{id}")]
        public async Task<ActionResult> PutSampleDetail([FromQuery] int templateId, int id, LabSampleDetail record)
        {
            if (record.SampleId == 0)
                record.SampleId = templateId;

            await _sampleRepo.UpdateDetail(record);

            return Json(new { Data = record });
        }

        [HttpDelete("DeleteSampleDetail/{id}")]
        public async Task<ActionResult> DeleteSampleDetail([FromQuery] int sampleId, int id, LabSampleDetail record)
        {
            if (record.SampleId == 0)
                record.SampleId = sampleId;

            await _sampleRepo.DeleteDetail(record);

            return Json(new { Data = record });
        }

        [HttpGet("GetSampleAttachments")]
        public ActionResult GetSampleAttachments([FromQuery] int sampleId)
        {
            var directory = Path.Combine(_hostingEnvironment.WebRootPath, $"uploads/{sampleId}");
            var records = new List<LabSampleAttachment>();
            if (!Directory.Exists(directory))
            {
                return Json(new { Data = records });
            }
            else
            {
                string[] filePaths = Directory.GetFiles(directory);

                foreach (var file in filePaths)
                {
                    records.Add(new LabSampleAttachment()
                    {
                        Description = Path.GetFileName(file),
                        LinkAddress = $"uploads/{sampleId}/{Path.GetFileName(file)}",
                        FileSize = FileSizeFormatter.FormatSize(new FileInfo(file).Length),
                        Uploaded = new FileInfo(file).CreationTime,
                        SampleId = sampleId
                    });
                }

                return Json(new { Data = records });
            }
        }


        [HttpPost("PostSampleAttachment")]
        public ActionResult PostSampleAttachment([FromQuery] int sampleId, IEnumerable<IFormFile> files)
        {
            if (files != null)
            {
                foreach (var file in files)
                {
                    var uniqueFileName = GetUniqueFileName(file.FileName);
                    var uploads = Path.Combine(_hostingEnvironment.WebRootPath, $"uploads/{sampleId}");

                    if (!Directory.Exists(uploads))
                    {
                        Directory.CreateDirectory(uploads);
                    }

                    var filePath = Path.Combine(uploads, uniqueFileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyTo(fileStream);
                    }                
                }
            }

            return RedirectToAction("ManageSamples", new { id = sampleId });
        }

        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName);
        }


    [HttpGet("GetTemplates")]
        public async Task<ActionResult> GetTemplates([DataSourceRequest] DataSourceRequest request)
        {
            var sqlCommand = new SqlCommand();
            var whereClause = request.Filters.ToSqlWhereClause(command: sqlCommand);
            var orderByClause = request.Sorts.ToSqlOrderByClause();

            var templates = await _templateRepo.GetList(sqlCommand.Parameters, whereClause, orderByClause, request.Page, request.PageSize);
            var templateCount = await _templateRepo.GetListCount(sqlCommand.Parameters, whereClause);

            var result = new DataSourceResult
            {
                Data = templates,
                Total = templateCount
            };

            return Json(result);
        }

        public async Task<ActionResult> GetTemplateLookup(bool external)
        {
            var sqlCommand = new SqlCommand();
            var templates = await _templateRepo.GetList(sqlCommand.Parameters, "", "ORDER BY Name", 1, 500);
            return Json(templates.Where(x => x.External == external).Select(x => new { x.Id, x.Name }));
        }

        [HttpPost("PostTemplate")]
        public async Task<ActionResult> PostTemplate(LabSampleTemplateHeader record)
        {
            record = await _templateRepo.Add(record);
            return Json(new { Data = record });
        }

        [HttpPut("PutTemplate/{id}")]
        public async Task<ActionResult> PutTemplate(int id, LabSampleTemplateHeader record)
        {
            await _templateRepo.Update(record);
            return Json(new { Data = record });
        }

        [HttpGet("GetTemplateDetails")]
        public async Task<ActionResult> GetTemplateDetails([FromQuery] int templateId, [DataSourceRequest] DataSourceRequest request)
        {
            var templates = await _templateRepo.GetDetails(templateId);

            var result = new DataSourceResult
            {
                Data = templates,
                Total = templates.Count
            };

            return Json(result);
        }

        [HttpPost("PostTemplateDetail")]
        public async Task<ActionResult> PostTemplateDetail([FromQuery] int templateId, LabSampleTemplateDetail record)
        {
            if (record.TemplateId == 0)
                record.TemplateId = templateId;

            record = await _templateRepo.AddDetail(record);
            return Json(new { Data = record });
        }

        [HttpPut("PutTemplateDetail/{id}")]
        public async Task<ActionResult> PutTemplateDetail([FromQuery] int templateId, int id, LabSampleTemplateDetail record)
        {
            if (record.TemplateId == 0)
                record.TemplateId = templateId;

            await _templateRepo.UpdateDetail(record);

            return Json(new { Data = record });
        }




    }
}