﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.ManageAreas;
using SSWA.Local.Application.UseCases.ManageChemicalProducts;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.ChemicalProducts;
using SSWA.Local.Core.Lab;
using SSWA.Local.Core.Projects;

namespace SSWA.Local.Web.UseCases.ManageLab
{
    [Authorize]
    [Route("[controller]")]
    public sealed class LabController : Controller
    {
        private readonly IManageLabUseCase _useCase;
        private readonly Presenter _presenter;

        public LabController(
            IManageLabUseCase useCase,
            Presenter presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet("Manage/{entityType}")]
        public async Task<IActionResult> Manage(string entityType)
        {
            var locations = await _useCase.GetLocations();
            ViewData["Locations"] = locations.ToDictionary(p => p.Id, p => p.Location);

            var units = await _useCase.GetUnits();
            ViewData["Units"] = units.ToDictionary(y => y.Id, y => y.Unit);

            var areas = await _useCase.GetAreas();
            ViewData["Areas"] = areas.ToDictionary(y => y.Id, y => y.Name);

            return View(entityType);
        }

        [HttpGet("GetAnalysers")]
        public async Task<ActionResult> GetAnalysers([DataSourceRequest] DataSourceRequest request)
        {
            var labs = await _useCase.GetAnalysers();

            var result = new DataSourceResult
            {
                Data = labs,
                Total = labs.Count()
            };

            return Json(result);
        }

        [HttpGet("GetAnalysersByLocation")]
        public async Task<IActionResult> GetAnalysersByLocation(int locationId)
        {

            var analysers = await _useCase.GetAnalysers();

            return Json(
                analysers.Select(x => new { x.Id, Description = $"{x.Id}: {x.Description} ", x.LocationId }).Where(x => x.LocationId == locationId));
        }

        [HttpPost("PostAnalyser")]
        public async Task<ActionResult> PostAnalyser(LabAnalyser record)
        {
            record = await _useCase.Create(record);


            return Json(new { Data = record });
        }

        [HttpPut("PutAnalyser/{id}")]
        public async Task<ActionResult> PutAnalyser(string id, LabAnalyser record)
        {
            await _useCase.Update(record);

            return Json(new { Data = record });
        }

        [HttpGet("GetAnalytes")]
        public async Task<ActionResult> GetAnalytes([DataSourceRequest] DataSourceRequest request)
        {
            var labs = await _useCase.GetAnalytes();

            var result = new DataSourceResult
            {
                Data = labs,
                Total = labs.Count()
            };

            return Json(result);
        }

        [HttpPost("PostAnalyte")]
        public async Task<ActionResult> PostAnalyte(LabAnalyte record)
        {
            record = await _useCase.Create(record);


            return Json(new { Data = record });
        }

        [HttpPut("PutAnalyte/{id}")]
        public async Task<ActionResult> PutAnalyte(int id, LabAnalyte record)
        {
            await _useCase.Update(record);

            return Json(new { Data = record });
        }


        [HttpGet("GetAreas")]
        public async Task<ActionResult> GetAreas([DataSourceRequest] DataSourceRequest request)
        {
            var labs = await _useCase.GetAreas();

            var result = new DataSourceResult
            {
                Data = labs,
                Total = labs.Count()
            };

            return Json(result);
        }

        [HttpPost("PostArea")]
        public async Task<ActionResult> PostArea(LabArea record)
        {
            record = await _useCase.Create(record);


            return Json(new { Data = record });
        }

        [HttpPut("PutArea/{id}")]
        public async Task<ActionResult> PutArea(int id, LabArea record)
        {
            await _useCase.Update(record);

            return Json(new { Data = record });
        }



        [HttpGet("GetLaboratories")]
        public async Task<ActionResult> GetLaboratories([DataSourceRequest] DataSourceRequest request)
        {
            var labs = await _useCase.GetLaboratories();

            var result = new DataSourceResult
            {
                Data = labs,
                Total = labs.Count()
            };

            return Json(result);
        }

        [HttpPost("PostLaboratory")]
        public async Task<ActionResult> PostLaboratory(LabLaboratory record)
        {
            record = await _useCase.Create(record);

            return Json(new { Data = record });
        }

        [HttpPut("PutLaboratory/{id}")]
        public async Task<ActionResult> PutLaboratory(int id, LabLaboratory record)
        {
            await _useCase.Update(record);

            return Json(new { Data = record });
        }


        [HttpGet("GetLocations")]
        public async Task<ActionResult> GetLocations([DataSourceRequest] DataSourceRequest request)
        {
            var labs = await _useCase.GetLocations();

            var result = new DataSourceResult
            {
                Data = labs,
                Total = labs.Count()
            };

            return Json(result);
        }

        [HttpPost("PostLocation")]
        public async Task<ActionResult> PostLocation(LabLocation record)
        {
            record = await _useCase.Create(record);


            return Json(new { Data = record });
        }

        [HttpPut("PutLocation/{id}")]
        public async Task<ActionResult> PutLocation(int id, LabLocation record)
        {
            await _useCase.Update(record);

            return Json(new { Data = record });
        }




        [HttpGet("GetSampleTypes")]
        public async Task<ActionResult> GetSampleTypes([DataSourceRequest] DataSourceRequest request)
        {
            var labs = await _useCase.GetSampleTypes();

            var result = new DataSourceResult
            {
                Data = labs,
                Total = labs.Count()
            };

            return Json(result);
        }

        [HttpPost("PostSampleType")]
        public async Task<ActionResult> PostSampleType(LabSampleType record)
        {
            record = await _useCase.Create(record);


            return Json(new { Data = record });
        }

        [HttpPut("PutSampleType/{id}")]
        public async Task<ActionResult> PutSampleType(int id, LabSampleType record)
        {
            await _useCase.Update(record);

            return Json(new { Data = record });
        }




        [HttpGet("GetUnits")]
        public async Task<ActionResult> GetUnits([DataSourceRequest] DataSourceRequest request)
        {
            var labs = await _useCase.GetUnits();

            var result = new DataSourceResult
            {
                Data = labs,
                Total = labs.Count()
            };

            return Json(result);
        }

        [HttpPost("PostUnit")]
        public async Task<ActionResult> PostUnit(LabUnit record)
        {
            record = await _useCase.Create(record);

            return Json(new { Data = record });
        }

        [HttpPut("PutUnit/{id}")]
        public async Task<ActionResult> PutUnit(int id, LabUnit record)
        {
            await _useCase.Update(record);

            return Json(new { Data = record });
        }


    }
}