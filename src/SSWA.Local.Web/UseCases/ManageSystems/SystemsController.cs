﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.ManageSystems;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.Projects;

namespace SSWA.Local.Web.UseCases.ManageSystems
{
    [Authorize]
    [Route("[controller]")]
    public sealed class SystemsController : Controller
    {
        private readonly IManageSystemsUseCase _useCase;
        private readonly Presenter _presenter;

        public SystemsController(
            IManageSystemsUseCase useCase,
            Presenter presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet("Manage")]
        public async Task<IActionResult> Manage()
        {
            var areas = await _useCase.GetAreas();
            ViewData["Areas"] = areas.ToDictionary(y => y.Id, y => y.Name);

            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Get([DataSourceRequest] DataSourceRequest request)
        {
            var systems = await _useCase.Get();

            var viewModelRecords = systems.Select(x => new ViewModel()
            {
                Id = x.Id,
                Name = x.SystemName,
                Notes = x.Notes,
                Area = x.Area
            });

            var result = new DataSourceResult
            {
                Data = viewModelRecords,
                Total = viewModelRecords.Count()
            };

            return Json(result);
        }

        [HttpPost]
        public ActionResult Post(ViewModel record)
        {
            var area = Core.Systems.System.Load(record.Id, record.Name, record.Notes, record.Area);

            _useCase.Create(area);

            return Json(record);
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, ViewModel record)
        {
            var area = Core.Systems.System.Load(record.Id, record.Name, record.Notes, record.Area);

            _useCase.Update(area);

            return Json(record);
        }



    }
}