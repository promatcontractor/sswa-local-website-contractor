﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageSystems
{
    public sealed class ViewModel
    {
        [Required]
        [Key]
        public string Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public string Notes { get; set; }

        public string Area { get; set; }
    }
}
