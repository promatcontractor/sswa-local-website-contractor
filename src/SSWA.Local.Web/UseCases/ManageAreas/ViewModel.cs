﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Web.UseCases.ManageAreas
{
    public sealed class ViewModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public string Title { get; set; }

        public string Comments { get; set; }

        public bool UseArea { get; set; }
    }
}
