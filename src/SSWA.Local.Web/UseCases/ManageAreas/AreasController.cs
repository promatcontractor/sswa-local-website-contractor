﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSWA.Local.Application.UseCases.ManageAreas;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.Projects;

namespace SSWA.Local.Web.UseCases.ManageAreas
{
    [Authorize]
    [Route("[controller]")]
    public sealed class AreasController : Controller
    {
        private readonly IManageAreasUseCase _useCase;
        private readonly Presenter _presenter;

        public AreasController(
            IManageAreasUseCase useCase,
            Presenter presenter)
        {
            _useCase = useCase;
            _presenter = presenter;
        }

        [HttpGet("Manage")]
        public IActionResult Manage()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> Get([DataSourceRequest] DataSourceRequest request)
        {
            var projects = await _useCase.Get();

            var viewModelRecords = projects.Select(x => new ViewModel()
            {
                Id = x.Id,
                Name = x.Name,
                Title = x.Title,
                Comments = x.Comments,
                UseArea = x.UseArea
            });

            var result = new DataSourceResult
            {
                Data = viewModelRecords,
                Total = viewModelRecords.Count()
            };

            return Json(result);
        }

        [HttpPost]
        public ActionResult Post(ViewModel record)
        {
            var area = Area.Load(record.Id, record.Name, record.Title, record.Comments, record.UseArea);

            _useCase.Create(area);

            return Json(record);
        }

        [HttpPut("{id}")]
        public ActionResult Put(string id, ViewModel record)
        {
            var area = Area.Load(id, record.Name, record.Title, record.Comments, record.UseArea);

            _useCase.Update(area);

            return Json(record);
        }



    }
}