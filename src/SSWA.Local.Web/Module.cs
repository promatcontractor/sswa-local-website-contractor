﻿using Autofac;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;

namespace SSWA.Local.Web
{
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //
            // Register all Types in SSWA.Local.Web
            //
            builder.RegisterAssemblyTypes(typeof(Startup).Assembly)
                .AsSelf()
                .InstancePerLifetimeScope();

            builder.RegisterType<HttpContextAccessor>()
                .As<IHttpContextAccessor>()
                .InstancePerLifetimeScope();

            //builder.RegisterType<IPrincipal>()
                

            //services.AddSingleton
            //services.AddTransient<IPrincipal>(
                //provider => provider.GetService<IHttpContextAccessor>().HttpContext.User);
        }
    }
}
