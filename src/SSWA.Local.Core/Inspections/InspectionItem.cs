﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Inspections
{
    public class Inspection
    {
        public int ShiftId { get; set; }

        public int ItemId { get; set; }

        public string AreaId { get; set; }

        public string Type { get; set; }

        public IReadOnlyCollection<string> Options { get; set; }

        public string Value { get; set; }
    }
}
