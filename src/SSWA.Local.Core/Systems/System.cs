﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Systems
{
    public sealed class System : IEntity<string>, IAggregateRoot<string>
    {
        public string Id { get; private set; }

        public string SystemName { get; private set; }

        public string Notes { get; private set; }

        public string Area { get; private set; }

        public System(string id, string name, string notes, string area)
        {
            Id = id;
            SystemName = name;
            Notes = notes;
            Area = area;
        }

        private System() { }

        public static System Load(string id, string name, string notes, string area)
        {
            System system = new System
            {
                Id = id,
                SystemName = name,
                Notes = notes,
                Area = area,
            };

            return system;
        }
    }
}
