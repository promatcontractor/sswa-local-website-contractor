﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Lab
{
    public class LabSampleTemplateHeader
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int LocationId { get; set; }

        public string SampleType { get; set; }

        public bool External { get; set; }

        public string Comments { get; set; }
    }

    public class LabSampleTemplateDetail
    {
        public int Id { get; set; }

        public int TemplateId { get; set; }

        public string AnalyserId { get; set; }

        public int? AnalyteId { get; set; }

        public decimal? ExpectedMinimum { get; set; }

        public decimal? ExpectedMaximum { get; set; }

        public decimal? LowerLimit { get; set; }

        public decimal? UpperLimit { get; set; }

        public int? UnitId { get; set; }

        public string Comments { get; set; }

        public int? DefaultUnitId { get; set; }
    }

    public class LabSampleHeader
    {
        public int Id { get; set; }

        public int? TemplateId { get; set; }

        public DateTime? SampleDateTime { get; set; }

        public int LocationId { get; set; }

        public string SampleType { get; set; }

        public string SampledBy { get; set; }

        public string AnalysedBy { get; set; }

        public int? LaboratoryId { get; set; }

        public bool External { get; set; }

        public string LabReference { get; set; }

        public DateTime? DateSamplesSent { get; set; }

        public DateTime? DateResultsReceived { get; set; }

        public string ResultsCheckedBy { get; set; }

        public string Comments { get; set; }
    }

    public class LabSampleDetail
    {
        public int Id { get; set; }

        public int SampleId { get; set; }

        public int? TemplateDetailId { get; set; }

        public string AnalyserId { get; set; }

        public int? AnalyteId { get; set; }

        public int? UnitId { get; set; }

        public decimal? AnalyserResult { get; set; }

        public decimal? LabResult { get; set; }

        public decimal? ExternalResult { get; set; }

        public string Symbol { get; set; }

        public bool? OutOfRange
        {
            get
            {
                if (AnalyserResult == null)
                    return null;

                if (LowerLimit == null && UpperLimit == null)
                    return null;

                if (AnalyserResult < LowerLimit)
                    return true;

                if (AnalyserResult > UpperLimit)
                    return true;

                return false;
            }
        }

        public bool? LabOutOfRange
        {
            get
            {
                if (LabResult == null)
                    return null;

                if (LowerLimit == null && UpperLimit == null)
                    return null;

                if (LabResult < LowerLimit)
                    return true;

                if (LabResult > UpperLimit)
                    return true;

                return false;
            }
        }


        public decimal? LowerLimit { get; set; }

        public decimal? UpperLimit { get; set; }

        public string ExpectedRange
        {
            get
            {
                if (LowerLimit == null && UpperLimit == null)
                    return "--";

                if (LowerLimit.HasValue && UpperLimit == null)
                    return $"> {LowerLimit}";

                if (UpperLimit.HasValue && LowerLimit == null)
                    return $"< {UpperLimit}";
           
                return $"{LowerLimit} to {UpperLimit}";
            }
        }


        public string Comments { get; set; }

        public int? DefaultUnitId { get; set; }
    }

    public class LabSampleAttachment
    {
        public int Id { get; set; }

        public int SampleId { get; set; }

        public string Description { get; set; }

        public string FileSize { get; set; }

        public DateTime Uploaded { get; set; }

        public string LinkAddress { get; set; }
    }

}
