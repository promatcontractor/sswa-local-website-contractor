﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Lab
{
    public class LabLaboratory : IEntity<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Suburb { get; set; }

        public string State { get; set; }

        public string Postcode { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }

        public string Notes { get; set; }
    }

    public class LabAnalyser : IEntity<string>
    {
        public string Id { get; set; }

        public string Description { get; set; }

        public int LocationId { get; set; }

        public string Comments { get; set; }
    }

    public class LabAnalyte : IEntity<int>
    {
        public int Id { get; set; }

        public string Symbol { get; set; }

        public string Title { get; set; }

        public int UnitId { get; set; }

        public string Comments { get; set; }
    }

    public class LabUnit : IEntity<int>
    {
        public int Id { get; set; }

        public string Unit { get; set; }

        public decimal? Factor { get; set; }

        public decimal? Order { get; set; }

        public string Comments { get; set; }
    }

    public class LabArea : IEntity<int>
    { 
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Comments { get; set; }
    }

    public class LabLocation : IEntity<int>
    {
        public int Id { get; set; }

        public string Tag { get; set; }

        public string Location { get; set; }

        public int AreaId { get; set; }

        public string Comments { get; set; }
    }

    public class LabSampleType : IEntity<int>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int SortOrder { get; set; }
    }
}
