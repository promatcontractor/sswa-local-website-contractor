﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.PIDs
{
    public class PID : IEntity<string>, IAggregateRoot<string>
    {
        public string Id { get; private set; }

        public string PIDTitle { get; private set; }

        public string Revision { get; private set; }

        public DateTime DateModified { get; private set; }

        public string Notes { get; private set; }

        public int ProjectId { get; private set; }

        public PID(string id, string title, string revision, DateTime dateModified, string notes, int projectId)
        {
            Id = id;
            PIDTitle = title;
            Revision = revision;
            DateModified = dateModified;
            Notes = notes;
            ProjectId = projectId;
        }

        private PID() { }

        public static PID Load(string id, string title, string revision, DateTime dateModified, string notes, int projectId)
        {
            PID pid = new PID
            {
                Id = id,
                PIDTitle = title,
                Revision = revision,
                DateModified = dateModified,
                Notes = notes,
                ProjectId = projectId,
            };

            return pid;
        }
    }
}
