﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.ChemicalProducts
{
    public sealed class ChemicalProduct
    {
        public int Id { get; private set; }

        public string Chemical { get; private set; }

        public string Type { get; private set; }

        public string Supplier { get; private set; }

        public string Quantity { get; private set; }

        public ChemicalProduct(int id, string chemical, string type, string supplier, string quantity)
        {
            Id = id;
            Chemical = chemical;
            Type = type;
            Supplier = supplier;
            Quantity = quantity;
        }

        private ChemicalProduct()
        { }

        public static ChemicalProduct Load(int id, string chemical, string type, string supplier, string quantity)
        {
            return new ChemicalProduct(id, chemical, type, supplier, quantity);
        }
    }
}
