﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.ChemicalProducts
{
    public class ChemicalProductOrderStatus
    {
        public static string REQUIRED = "Required but not yet ordered";
        public static string ORDERED = "Ordered";
        public static string RECEIVED = "Received";
        public static string CANCELLED = "Cancelled";
    }
}
