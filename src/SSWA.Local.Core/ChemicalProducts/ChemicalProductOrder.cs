﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.ChemicalProducts
{
    public class ChemicalProductOrder
    {
        public int Id { get; private set; }

        public int ChemicalProductId { get; private set; }

        public string Status { get; private set; }

        public DateTime? ExpectedDeliveryDate { get; private set; }

        public DateTime? ExpectedDeliveryTime { get; private set; }

        public string ArrivalAcknowledgedById { get; private set; }

        public DateTime? IdentifiedAt { get; private set; }

        public DateTime? OrderedAt { get; private set; }

        public DateTime? ReceivedAt { get; private set; }

        public string Comments { get; private set; }

        public ChemicalProductOrder(
            int id,
            int chemicalProductId,
            string status,
            DateTime? expectedDeliveryDate,
            DateTime? expectedDeliveryTime,
            string arrivalAcknowledgedById,
            DateTime? identifiedAt,
            DateTime? orderedAt,
            DateTime? receivedAt,
            string comments)
        {
            Id = id;
            ChemicalProductId = chemicalProductId;
            Status = status;
            ExpectedDeliveryDate = expectedDeliveryDate;
            ExpectedDeliveryTime = expectedDeliveryTime;
            ArrivalAcknowledgedById = arrivalAcknowledgedById;
            IdentifiedAt = identifiedAt;
            OrderedAt = orderedAt;
            ReceivedAt = receivedAt;
            Comments = comments;
        }

        private ChemicalProductOrder() { }

        public void Identify()
        {
            Status = ChemicalProductOrderStatus.REQUIRED;
            IdentifiedAt = DateTime.Now;
        }

        public void Order()
        {
            Status = ChemicalProductOrderStatus.ORDERED;
            OrderedAt = DateTime.Now;
        }

        public void Receive()
        {
            Status = ChemicalProductOrderStatus.RECEIVED;
            ReceivedAt = DateTime.Now;
        }

        public void SetTime()
        {
            if (ExpectedDeliveryTime == null) return;

            var time = ExpectedDeliveryTime.Value.TimeOfDay;
            ExpectedDeliveryTime = DateTime.Today.Add(time);
        }

        public static ChemicalProductOrder Load(
            int id,
            int chemicalProductId,
            string status,
            DateTime? expectedDeliveryDate,
            DateTime? expectedDeliveryTime,
            string arrivalAcknowledgedById,
            DateTime? identifiedAt,
            DateTime? orderedAt,
            DateTime? receivedAt,
            string comments
        )
        {
            return new ChemicalProductOrder(
                id,
                chemicalProductId,
                status,
                expectedDeliveryDate,
                expectedDeliveryTime,
                arrivalAcknowledgedById,
                identifiedAt,
                orderedAt,
                receivedAt,
                comments);
        }
    }
}
