﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Equipment
{
    public sealed class Equipment : IEntity<string>, IAggregateRoot<string>
    {
        public string Id { get; private set; }

        public string Description { get; private set; }

        public string Prefix { get; private set; }

        public string Inum { get; private set; }

        public string Type { get; private set; }

        public string Notes { get; private set; }

        public string TagOriginal { get; private set; }

        public string Area { get; private set; }

        public string PID { get; private set; }

        public int ProjectId { get; private set; }

        public Equipment(
            string id, 
            string description, 
            string prefix, 
            string inum,
            string type,
            string notes,
            string tagOrginal,
            string area,
            string pid,
            int projectId)
        {
            Id = id;
            Description = description;
            Prefix = prefix;
            Inum = inum;
            Type = type;
            Notes = notes;
            TagOriginal = tagOrginal;
            Area = area;
            PID = pid;
            ProjectId = projectId;
        }

        private Equipment() { }

        public static Equipment Load(
            string id,
            string description,
            string prefix,
            string inum,
            string type,
            string notes,
            string tagOrginal,
            string area,
            string pid,
            int projectId
            )
        {
            Equipment equipment = new Equipment
            {
                Id = id,
                Description = description,
                Prefix = prefix,
                Inum = inum,
                Type = type,
                Notes = notes,
                TagOriginal = tagOrginal,
                Area = area,
                PID = pid,
                ProjectId = projectId,
            };

            return equipment;
        }
    }
}
