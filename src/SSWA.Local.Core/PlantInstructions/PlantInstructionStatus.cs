﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.PlantInstructions
{
    public class PlantInstructionStatus
    {
        public static string ACTIVE = "Active";
        public static string INACTIVE = "Inactive";
    }
}
