﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.PlantInstructions
{
    public class PlantInstruction
    {
        public int Id { get; private set; }

        public string Title { get; private set; }

        public string Description { get; private set; }

        public string Status { get; private set; }

        public int? Duration { get; private set; }
        
        public string DurationUnits { get; private set; }

        public DateTime DateCreated { get; private set; }

        public string OperatorUserId { get; private set; }

        public int InstructionFromRoleId { get; private set; }

        public string AuthorisedByUserId { get; private set; }

        private PlantInstruction() { }

        public static PlantInstruction Load(int id, string title, string description, string status, int? duration, string durationUnits, DateTime dateCreated, string operatorId, int instructionFromId, string authorisedById)
        {
            return new PlantInstruction()
            {
                Id = id,
                Title = title,
                Description = description,
                Status = status,
                Duration = duration,
                DurationUnits = durationUnits,
                DateCreated = dateCreated,
                OperatorUserId = operatorId,
                InstructionFromRoleId = instructionFromId,
                AuthorisedByUserId = authorisedById
            };
        }
    }
}
