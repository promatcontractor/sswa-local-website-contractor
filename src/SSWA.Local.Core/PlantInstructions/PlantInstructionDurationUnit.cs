﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SSWA.Local.Core.PlantInstructions
{
    public class PlantInstructionDurationUnit
    {
        public static string HOURS = "Hours";
        public static string DAYS = "Days";
        public static string WEEKS = "Weeks";
        public static string MONTHS = "Months";
        public static string YEARS = "Years";
    }

    public enum PlatInstructionUnit
    {
        [Description("Hours")]
        Hours,

        [Description("Hours")]
        Days,

        [Description("Hours")]
        Weeks,

        [Description("Months")]
        Months,

        [Description("Years")]
        Years
    }
}
