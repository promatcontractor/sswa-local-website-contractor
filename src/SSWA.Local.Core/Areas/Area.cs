﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Areas
{
    public sealed class Area : IEntity<string>, IAggregateRoot<string>
    {
        public string Id { get; private set; }

        public string Name { get; private set; }

        public string Title { get; private set; }

        public string Comments { get; private set; }

        public bool UseArea { get; private set; }

        public Area(string name = "New Area", string title = "New Area", string comments = "", bool useArea = true)
        {
            Id = "";
            Name = name;
            Title = title;
            Comments = comments;
            UseArea = useArea;
        }

        private Area() { }

        public static Area Load(string id, string name = "New Area", string title = "New Area", string comments = "", bool useArea = true)
        {
            Area area = new Area
            {
                Id = id,
                Name = name,
                Title = title,
                Comments = comments,
                UseArea = useArea
        };

            return area;
        }
    }
}
