﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Projects
{
    public sealed class Project : IEntity<int>, IAggregateRoot<int>
    {
        public int Id { get; private set; }

        public string Name { get; private set; }

        public Project(string name)
        {
            Id = 0;
            Name = name;
        }

        private Project() {  }

        public static Project Load(int id, string name)
        {
            Project project = new Project
            {
                Id = id,
                Name = name,
            };

            return project;
        }
    }
}
