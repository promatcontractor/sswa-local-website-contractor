﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Users
{
    public class Position
    {
        public int Id { get; private set; }

        public string Name { get; private set; }

        private Position() { }

        public static Position Load(int id, string name)
        {
            return new Position()
            {
                Id = id,
                Name = name
            };
        }
    }
}
