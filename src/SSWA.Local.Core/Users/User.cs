﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Core.Users
{
    public sealed class User : IEntity<string>, IAggregateRoot<string>
    {
        public string Id { get; private set; }

        public int? Reference { get; private set; }

        public string Name { get; private set; }

        public IReadOnlyCollection<string> Roles
        {
            get
            {
                IReadOnlyCollection<string> roles = _roles.GetRoles();
                return roles;
            }
        }

        private RolesCollection _roles;


        public User(string name, int? reference)
        {
            Id = string.Empty;
            Name = name;
            Reference = reference;
            _roles = new RolesCollection();
        }

        private User() { }

        public void AddRole(string role)
        {
            _roles.Add(role);
        }

        public static User Load(string id, int? reference, string name, RolesCollection roles)
        {
            User user = new User
            {
                Id = id,
                Reference = reference,
                Name = name,
                _roles = roles
            };

            return user;
        }
    }
}
