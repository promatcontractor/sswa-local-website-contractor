﻿using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Application.Services
{
    public interface ICurrentUserService
    {
        User GetUser();
    }
}