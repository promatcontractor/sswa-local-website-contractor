﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Application
{
    public sealed class UserOutput
    {
        public string UserId { get; set; }
        public string Name { get; set;  }
        public Dictionary<string, bool> Roles { get; set; }

        public UserOutput(string userId, string name, Dictionary<string, bool> roles)
        {
            UserId = userId;
            Name = name;
            Roles = roles;
        }

        public UserOutput()
        {

        }
    }
}
