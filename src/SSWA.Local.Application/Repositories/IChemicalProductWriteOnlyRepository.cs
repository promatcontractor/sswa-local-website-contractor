﻿using SSWA.Local.Core.ChemicalProducts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IChemicalProductWriteOnlyRepository
    {
        Task Add(ChemicalProduct chemicalProduct);
        Task Update(ChemicalProduct chemicalProduct);
        Task Delete(ChemicalProduct chemicalProduct);
    }
}
