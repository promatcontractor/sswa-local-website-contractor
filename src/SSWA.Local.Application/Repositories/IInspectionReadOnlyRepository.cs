﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IInspectionReadOnlyRepository
    {
        Task<int?> GetInspectionItemIdForShiftAsync(int shiftId, int itemId);

        Task<IEnumerable<InspectionLogOutput>> GetInspectionLogs(DateTime date, string shiftType, string project, string areaId);
    }
}
