﻿using SSWA.Local.Core.ChemicalProducts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IChemicalProductOrderWriteOnlyRepository
    {
        Task<ChemicalProductOrder> Add(ChemicalProductOrder order);
        Task Update(ChemicalProductOrder order);
        Task Delete(ChemicalProductOrder order);
    }
}
