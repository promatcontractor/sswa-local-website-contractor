﻿using SSWA.Local.Core.ChemicalProducts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IChemicalProductReadOnlyRepository
    {
        Task<IReadOnlyCollection<ChemicalProduct>> GetAll();
        Task<ChemicalProduct> Get(int id);
    }
}
