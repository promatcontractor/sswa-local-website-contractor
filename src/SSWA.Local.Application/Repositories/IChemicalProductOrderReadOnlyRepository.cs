﻿using SSWA.Local.Core.ChemicalProducts;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IChemicalProductOrderReadOnlyRepository
    {
        Task<IReadOnlyCollection<ChemicalProductOrder>> GetAllOpen();

        Task<IReadOnlyCollection<ChemicalProductOrder>> GetHistory(SqlParameterCollection parameters, string whereClause, string orderByClause, int page, int pageSize);

        Task<int> GetHistoryCount(SqlParameterCollection parameters, string whereClause);
    }
}
