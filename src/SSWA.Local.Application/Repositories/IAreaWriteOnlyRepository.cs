﻿using SSWA.Local.Core.Areas;
using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IAreaWriteOnlyRepository
    {
        Task Add(Area area);
        Task Update(Area area);
        Task Delete(Area area);
    }
}
