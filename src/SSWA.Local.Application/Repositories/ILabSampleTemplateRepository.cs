﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using SSWA.Local.Core.Lab;

namespace SSWA.Local.Application.Repositories
{
    public interface ILabSampleTemplateRepository
    {
        Task<LabSampleTemplateHeader> Add(LabSampleTemplateHeader header);
        Task<LabSampleTemplateDetail> AddDetail(LabSampleTemplateDetail detail);
        Task Delete(LabSampleTemplateHeader header);
        Task DeleteDetail(LabSampleTemplateDetail detail);
        Task<LabSampleTemplateHeader> Get(int id);
        Task<IReadOnlyCollection<LabSampleTemplateDetail>> GetDetails(int templateId);
        Task<IReadOnlyCollection<LabSampleTemplateHeader>> GetList(SqlParameterCollection parameters, string whereClause, string orderByClause, int page, int pageSize);
        Task<int> GetListCount(SqlParameterCollection parameters, string whereClause);
        Task Update(LabSampleTemplateHeader header);
        Task UpdateDetail(LabSampleTemplateDetail detail);
    }
}