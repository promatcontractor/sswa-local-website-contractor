﻿using SSWA.Local.Core.Areas;
using SSWA.Local.Core.Equipment;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IEquipmentReadOnlyRepository
    {
        Task<IReadOnlyCollection<Equipment>> GetAllAsync(int page, int pageSize);
        Task<IReadOnlyCollection<Equipment>> GetByArea(string areaId, string tag);
        Task<Equipment> Get(string id);
        Task<int> CountAsync();
    }
}
