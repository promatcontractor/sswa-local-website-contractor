﻿using SSWA.Local.Core.PlantInstructions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IPlantInstructionWriteOnlyRepository
    {
        Task Add(PlantInstruction instruction);
        Task Update(PlantInstruction instruction);
        Task Delete(PlantInstruction instruction);
    }
}
