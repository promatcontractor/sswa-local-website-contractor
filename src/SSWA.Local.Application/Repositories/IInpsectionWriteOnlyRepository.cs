﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IInpsectionWriteOnlyRepository
    {
        Task AddInspectionLog(int shiftId, int inspectionItemId, InspectionLogOutput record);

        Task UpdateInpectionLog(int shiftId, int inspectionItemId, InspectionLogOutput record);
    }
}
