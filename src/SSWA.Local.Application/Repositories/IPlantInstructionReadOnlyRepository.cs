﻿using SSWA.Local.Core.PlantInstructions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IPlantInstructionReadOnlyRepository
    {
        Task<IReadOnlyCollection<PlantInstruction>> GetAll(SqlParameterCollection parameters, string whereClause, string orderByClause);
        Task<PlantInstruction> Get(int id);
    }
}
