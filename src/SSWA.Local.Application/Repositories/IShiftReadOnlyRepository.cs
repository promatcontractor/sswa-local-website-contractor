﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IShiftReadOnlyRepository
    {
        Task<int?> GetShiftId(DateTime date, string shiftType);

        Task<ShiftOutput> GetShift(DateTime date, string shiftType, string operatorId);

        Task<IEnumerable<ShiftLogOutput>> GetShiftLogs(DateTime date, string shiftType, string operatorId);
    }
}
