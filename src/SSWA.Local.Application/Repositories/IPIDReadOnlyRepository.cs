﻿using SSWA.Local.Core.Areas;
using SSWA.Local.Core.PIDs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IPIDReadOnlyRepository
    {
        Task<IReadOnlyCollection<PID>> GetAll();
        Task<IReadOnlyCollection<PID>> GetByEquipmentId(string equipmentId);
        Task<PID> Get(string id);
    }
}
