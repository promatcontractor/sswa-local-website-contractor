﻿using SSWA.Local.Core.Areas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IAreaReadOnlyRepository
    {
        Task<IReadOnlyCollection<Area>> GetAll();
        Task<IReadOnlyCollection<Area>> GetByProjectId(string projectId, bool inspectionAreasOnly = false);
        Task<Area> Get(string id);
    }
}
