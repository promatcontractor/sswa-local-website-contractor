﻿using SSWA.Local.Core.ChemicalProducts;
using SSWA.Local.Core.Lab;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface ILabSampleRepository
    {
        Task<LabSampleHeader> Add(LabSampleHeader header);
        Task<LabSampleAttachment> AddAttachment(LabSampleAttachment attachment);
        Task<LabSampleDetail> AddDetail(LabSampleDetail detail);
        Task Delete(LabSampleHeader header);
        Task DeleteAttachment(LabSampleAttachment attachment);
        Task DeleteDetail(LabSampleDetail detail);
        Task<LabSampleHeader> Get(int id);
        Task<IReadOnlyCollection<LabSampleAttachment>> GetAttachments(int sampleId);
        Task<IReadOnlyCollection<LabSampleDetail>> GetDetails(int sampleId);
        Task<IReadOnlyCollection<LabSampleHeader>> GetList(SqlParameterCollection parameters, string whereClause, string orderByClause, int page, int pageSize);
        Task<int> GetListCount(SqlParameterCollection parameters, string whereClause);
        Task Update(LabSampleHeader header);
        Task UpdateAttachment(LabSampleAttachment attachment);
        Task UpdateDetail(LabSampleDetail detail);
    }
}
