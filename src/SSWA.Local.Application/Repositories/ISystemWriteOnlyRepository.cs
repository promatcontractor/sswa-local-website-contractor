﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface ISystemWriteOnlyRepository
    {
        Task Add(Core.Systems.System system);
        Task Update(Core.Systems.System system);
        Task Delete(Core.Systems.System system);
    }
}
