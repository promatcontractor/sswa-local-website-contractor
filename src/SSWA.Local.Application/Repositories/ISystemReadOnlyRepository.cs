﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface ISystemReadOnlyRepository
    {
        Task<IReadOnlyCollection<Core.Systems.System>> GetAll();
        Task<Core.Systems.System> Get(string id);
    }
}
