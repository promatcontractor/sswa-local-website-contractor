﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Core;

namespace SSWA.Local.Application.Repositories
{
    public interface IReadOnlyRepository<T,U> where T : IEntity<U>
    {
        Task<IReadOnlyCollection<T>> GetAll();
        Task<T> Get(U id);
    }

    public interface IWriteOnlyRepository<T, U> where T : IEntity<U>
    {
        Task<U> Add(T entity);
        Task Update(T entity);
        Task Delete(U id);
    }
}
