﻿using SSWA.Local.Core.Areas;
using SSWA.Local.Core.Equipment;
using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IEquipmentWriteOnlyRepository
    {
        Task Add(Equipment equipment);
        Task Update(Equipment equipment);
        Task Delete(Equipment equipment);
    }
}
