﻿using SSWA.Local.Core.Areas;
using SSWA.Local.Core.PIDs;
using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IPIDWriteOnlyRepository
    {
        Task Add(PID pid);
        Task Update(PID pid);
        Task Delete(PID pid);
    }
}
