﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.Repositories
{
    public interface IShiftWriteOnlyRepository
    {
        Task<int> AddShift(DateTime date, string shiftType);

        Task<int> UpdateShift(ShiftOutput shift);

        Task<int> AddShiftLog(int shiftId, ShiftLogOutput log);

        Task<int> UpdateShiftLog(ShiftLogOutput log);
    }
}
