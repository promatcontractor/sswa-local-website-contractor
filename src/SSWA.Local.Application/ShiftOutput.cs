﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Application
{
    public sealed class ShiftOutput
    {
        public int? ShiftId { get; set; }

        public DateTime Date { get; set; }

        public string ShiftType { get; set;  }

        public decimal? Swell { get; set; }

        public decimal? MaxForecastWindSpeed { get; set; }

        public DateTime? LowTideTime { get; set; }

        public decimal? RainForecast { get; set; }

        public string Comments { get; set; }

        public string OHSEComments { get; set; }

        public UserOutput Operator { get; set; }

        public string OperatorId { get; set; }

        public ShiftNavigationOutput ThisShift { get; set; }

        public ShiftNavigationOutput NextShift { get; set; }

        public ShiftNavigationOutput PreviousShift { get; set; }


        public ShiftOutput(
            int shiftId, 
            DateTime date, 
            string shiftType,
            decimal? swell,
            decimal? maxForecastWindSpeed,
            DateTime? lowTideTime,
            decimal? rainForecast,
            string comments,
            string ohseComments,
            UserOutput @operator,
            ShiftNavigationOutput thisShift,
            ShiftNavigationOutput nextShift,
            ShiftNavigationOutput previousShift)
        {
            ShiftId = shiftId;
            Date = date;
            ShiftType = shiftType;
            Swell = swell;
            MaxForecastWindSpeed = maxForecastWindSpeed;
            LowTideTime = lowTideTime;
            RainForecast = rainForecast;
            Comments = comments;
            OHSEComments = ohseComments;
            Operator = @operator;
            ThisShift = thisShift;
            NextShift = nextShift;
            PreviousShift = previousShift;
        }

        public ShiftOutput() { }
    }
}
