﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Application
{
    public sealed class ShiftNavigationOutput
    {
        public DateTime Date { get; }

        public string Shift { get; }

        public string FilterId { get; }

        public bool IsCurrentShift { get; private set; }

        public bool IsFutureShift { get; private set; }

        public bool IsPastShift { get; private set; }

        public const string DAY_SHIFT_START_TIME = "05:30:00";
        public const string NIGHT_SHIFT_START_TIME = "17:30:00";

        public const string DAY_SHIFT = "Day";
        public const string NIGHT_SHIFT = "Night";

        private ShiftNavigationOutput(DateTime date, string shift, string filterId)
        {
            Date = date.Date;
            Shift = shift;
            FilterId = filterId;

            var currentShift = CurrentShift();

            IsCurrentShift = currentShift.Date.Date == Date.Date && currentShift.Shift == Shift;
            IsFutureShift = !IsCurrentShift && (Date.Date > currentShift.Date.Date || (Date.Date == currentShift.Date.Date && Shift == NIGHT_SHIFT));
            IsPastShift = !IsCurrentShift && !IsFutureShift;
        }

        private static (DateTime Date, string Shift) CurrentShift()
        {
            var date = DateTime.Now;
            var shiftType = "Day";

            if (date.TimeOfDay >= TimeSpan.Parse(DAY_SHIFT_START_TIME) && date.TimeOfDay < TimeSpan.Parse(NIGHT_SHIFT_START_TIME))
            {
                shiftType = "Day";
            }
            else
            {
                shiftType = "Night";
            }

            return (date, shiftType);
        }

        public static ShiftNavigationOutput ThisShift(DateTime? date, string shiftType, string filterId, string defaultFilterId)
        {
            if (date == null)
            {
                date = DateTime.Now;
            }

            if (shiftType == null)
            {
                if (date.Value.TimeOfDay >= TimeSpan.Parse(DAY_SHIFT_START_TIME) && date.Value.TimeOfDay < TimeSpan.Parse(NIGHT_SHIFT_START_TIME))
                {
                    shiftType = "Day";
                }
                else
                {
                    shiftType = "Night";
                }
            }

            if (filterId == null)
            {
                filterId = defaultFilterId;
            }

            return new ShiftNavigationOutput(date.Value.Date, shiftType, filterId);
        }

        public static ShiftNavigationOutput NextShift(DateTime? date, string shiftType, string filterId, string defaultFilterId)
        {

            ShiftNavigationOutput currentShift = ThisShift(date, shiftType, filterId, defaultFilterId);

            if (currentShift.Shift == "Day")
            {
                return new ShiftNavigationOutput(currentShift.Date, "Night", currentShift.FilterId);
            }
            else
            {
                return new ShiftNavigationOutput(currentShift.Date.AddDays(1), "Day", currentShift.FilterId);
            }
        }

        public static ShiftNavigationOutput PreviousShift(DateTime? date, string shiftType, string filterId, string defaultFilterId)
        {
            ShiftNavigationOutput currentShift = ThisShift(date, shiftType, filterId, defaultFilterId);

            if (currentShift.Shift == "Day")
            {
                return new ShiftNavigationOutput(currentShift.Date.AddDays(-1), "Night", currentShift.FilterId);
            }
            else
            {
                return new ShiftNavigationOutput(currentShift.Date, "Day", currentShift.FilterId);
            }
        }
    }
}
