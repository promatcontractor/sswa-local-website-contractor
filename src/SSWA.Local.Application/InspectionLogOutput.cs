﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Application
{
    public class InspectionLogOutput
    {
        public string AreaId { get; set; }

        public string AreaName { get; set; }

        public int InspectionItemId { get; set; }

        public string InspectionItemName { get; set; }

        public InspectionLogValue Value { get; set; }
    }


    public class InspectionLogValue
    {
        public string Type { get; set; }

        public IReadOnlyCollection<string> Options { get; set; }

        public dynamic Value { get; set; }
    }
}
