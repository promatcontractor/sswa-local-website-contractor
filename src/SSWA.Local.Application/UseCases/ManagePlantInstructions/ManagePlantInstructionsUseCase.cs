﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.PlantInstructions;
using SSWA.Local.Core.Users;

namespace SSWA.Local.Application.UseCases.ManagePlantInstructions
{
    public class ManagePlantInstructionsUseCase : IManagePlantInstructionsUseCase
    {
        private readonly IPlantInstructionReadOnlyRepository _readOnlyRepo;
        private readonly IPlantInstructionWriteOnlyRepository _writeOnlyRepo;
        private readonly IUserReadOnlyRepository _userRepo;
        private readonly IPositionReadOnlyRepository _positionRepo;

        public ManagePlantInstructionsUseCase(
            IPlantInstructionReadOnlyRepository readOnlyRepo,
            IPlantInstructionWriteOnlyRepository writeOnlyRepo,
            IUserReadOnlyRepository userRepo,
            IPositionReadOnlyRepository positionRepo
            )
        {
            _readOnlyRepo = readOnlyRepo;
            _writeOnlyRepo = writeOnlyRepo;
            _userRepo = userRepo;
            _positionRepo = positionRepo;
        }

        public async Task<PlantInstruction> Create(PlantInstruction instruction)
        {
            await _writeOnlyRepo.Add(instruction);

            return instruction;
        }

        public async Task<IEnumerable<PlantInstruction>> GetAll(SqlParameterCollection parameters, string whereClause, string orderByClause)
        {
            return await _readOnlyRepo.GetAll(parameters, whereClause, orderByClause);
        }

        public async Task<IEnumerable<Position>> GetPositions()
        {
            return await _positionRepo.GetAll();
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _userRepo.GetAll();
        }

        public async Task<PlantInstruction> Update(PlantInstruction instruction)
        {
            await _writeOnlyRepo.Update(instruction);

            return instruction;
        }
    }
}
