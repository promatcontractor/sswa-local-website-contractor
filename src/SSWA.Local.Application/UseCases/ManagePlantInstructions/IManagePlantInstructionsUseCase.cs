﻿using SSWA.Local.Core.PlantInstructions;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManagePlantInstructions
{
    public interface IManagePlantInstructionsUseCase
    {
        Task<IEnumerable<PlantInstruction>> GetAll(SqlParameterCollection parameters, string whereClause, string orderByClause);

        Task<PlantInstruction> Create(PlantInstruction instruction);

        Task<PlantInstruction> Update(PlantInstruction instruction);

        Task<IEnumerable<Position>> GetPositions();

        Task<IEnumerable<User>> GetUsers();
    }
}
