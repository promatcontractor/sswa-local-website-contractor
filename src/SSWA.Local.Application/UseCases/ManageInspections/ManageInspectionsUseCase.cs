﻿using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Areas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManageInspections
{
    public class ManageInspectionsUseCase : IManageInspectionsUseCase
    {
        private readonly IInspectionReadOnlyRepository _inspectionReadOnlyRepository;
        private readonly IInpsectionWriteOnlyRepository _inpsectionWriteOnlyRepository;
        private readonly IAreaReadOnlyRepository _areaReadOnlyRepository;
        private readonly IShiftReadOnlyRepository _shiftReadOnlyRepository;
        private readonly IShiftWriteOnlyRepository _shiftWriteOnlyRepository;

        public ManageInspectionsUseCase(
            IInspectionReadOnlyRepository inspectionReadOnlyRepository,
            IInpsectionWriteOnlyRepository inpsectionWriteOnlyRepository,
            IAreaReadOnlyRepository areaReadOnlyRepository,
            IShiftReadOnlyRepository shiftReadOnlyRepository,
            IShiftWriteOnlyRepository shiftWriteOnlyRepository)
        {
            _inspectionReadOnlyRepository = inspectionReadOnlyRepository;
            _inpsectionWriteOnlyRepository = inpsectionWriteOnlyRepository;
            _areaReadOnlyRepository = areaReadOnlyRepository;
            _shiftReadOnlyRepository = shiftReadOnlyRepository;
            _shiftWriteOnlyRepository = shiftWriteOnlyRepository;
        }

        public async Task<InspectionOutput> Execute(DateTime? date, string shiftType, string filter)
        {
            var defaultFilter = "All,All";
            var thisShift = ShiftNavigationOutput.ThisShift(date, shiftType, filter, defaultFilter);
            var nextShift = ShiftNavigationOutput.NextShift(date, shiftType, filter, defaultFilter);
            var prevShift = ShiftNavigationOutput.PreviousShift(date, shiftType, filter, defaultFilter);

            return new InspectionOutput()
            {
                ThisShift = thisShift,
                NextShift = nextShift,
                PreviousShift = prevShift,
                Date = thisShift.Date,
                Project = thisShift.FilterId.Split(',')[0],
                AreaId = thisShift.FilterId.Split(',')[1],
                ShiftType = thisShift.Shift
            };
        }

        public async Task<IEnumerable<ProjectOutput>> GetProjects()
        {
            List<ProjectOutput> projects = new List<ProjectOutput>
            {
                new ProjectOutput(0, "All"),
                new ProjectOutput(1, "Stage 1"),
                new ProjectOutput(2, "Expansion Project"),
                new ProjectOutput(3, "Common")
            };

            return projects;
        }

        public async Task<IEnumerable<Area>> GetAreas(string project = "All")
        {
            List<Area> areas = new List<Area>();

            areas.Add(Area.Load("All", "All Areas", "All Areas", "", false));
            areas.AddRange(await _areaReadOnlyRepository.GetByProjectId(project, true));

            return areas;
        }

        public async Task<IEnumerable<InspectionLogOutput>> GetLogs(DateTime date, string shiftType, string project, string areaId)
        {
            var logs = await _inspectionReadOnlyRepository.GetInspectionLogs(date, shiftType, project, areaId);

            foreach (var log in logs)
            {
                if (log.Value.Type == "lookup" && log.Value.Value != null)
                {
                    log.Value.Value = (log.Value.Value as string).Split(',').Select(x => new { value = x });
                }
            }

            return logs;
        }

        public async Task<InspectionLogOutput> Update(DateTime date, string shiftType, int inspectionItemId, InspectionLogOutput record)
        {
            date = date.Date;
            int? shiftId = null;

            // get shift id
            shiftId = await _shiftReadOnlyRepository.GetShiftId(date, shiftType);

            // if not exists create new shift and then get shift id
            if (shiftId == null)
            {
                shiftId = await _shiftWriteOnlyRepository.AddShift(date, shiftType);
            }

            // get inspection item id for shift id
            int? checkInspectionItemId = null;

            checkInspectionItemId = await _inspectionReadOnlyRepository.GetInspectionItemIdForShiftAsync(shiftId.Value, inspectionItemId);

            if (record.Value.Type == "lookup")
            {
                List<string> values = new List<string>();
                foreach (dynamic outerValue in record.Value.Value)
                {
                    foreach (dynamic innerValue in outerValue)
                    {
                        string formattedValue = innerValue.Value.ToString();
                        values.Add(formattedValue);
                    }

                }
                record.Value.Value = string.Join(',', values);
            }
            else
            {
                record.Value.Value = record.Value.Value.ToString();
            }



            // if not exists create new inspection item log
            if (checkInspectionItemId == null)
            {
                await _inpsectionWriteOnlyRepository.AddInspectionLog(shiftId.Value, inspectionItemId, record);
            }

            // update inspection log
            await _inpsectionWriteOnlyRepository.UpdateInpectionLog(shiftId.Value, inspectionItemId, record);

            return record;
        }
    }
}
