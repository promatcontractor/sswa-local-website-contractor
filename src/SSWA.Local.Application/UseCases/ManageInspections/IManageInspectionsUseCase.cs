﻿using SSWA.Local.Core.Areas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManageInspections
{
    public interface IManageInspectionsUseCase
    {
        Task<InspectionOutput> Execute(DateTime? date, string shiftType, string filter = "All,All");

        Task<IEnumerable<InspectionLogOutput>> GetLogs(DateTime date, string shiftType, string project, string areaId);

        Task<InspectionLogOutput> Update(DateTime date, string shiftType, int inspectionItemId, InspectionLogOutput record);

        Task<IEnumerable<ProjectOutput>> GetProjects();

        Task<IEnumerable<Area>> GetAreas(string project = "All");
    }
}
