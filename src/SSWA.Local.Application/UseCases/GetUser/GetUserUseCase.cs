﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Users;

namespace SSWA.Local.Application.UseCases.GetUser
{
    public sealed class GetUsersUseCase : IGetUserUseCase
    {
        private readonly IUserReadOnlyRepository _repo;

        public GetUsersUseCase(IUserReadOnlyRepository repo)
        {
            this._repo = repo;
        }

        public Task<User> Execute(string id)
        {
            return _repo.Get(id);
        }
    }
}
