﻿using SSWA.Local.Application.Repositories;
using SSWA.Local.Application.Services;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.Equipment;
using SSWA.Local.Core.PIDs;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManageShifts
{
    public sealed class ManageShiftsUseCase : IManageShiftsUseCase
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IShiftReadOnlyRepository _shiftReadOnlyRepository;
        private readonly IShiftWriteOnlyRepository _shiftWriteOnlyRepository;
        private readonly IAreaReadOnlyRepository _areaReadOnlyRepository;
        private readonly IEquipmentReadOnlyRepository _equipmentReadOnlyRepository;
        private readonly IPIDReadOnlyRepository _pIDReadOnlyRepository;
        private readonly IUserReadOnlyRepository _userReadOnlyRepository;

        public ManageShiftsUseCase(
            ICurrentUserService currentUserService,
            IShiftReadOnlyRepository shiftReadOnlyRepository,
            IShiftWriteOnlyRepository shiftWriteOnlyRepository,
            IAreaReadOnlyRepository areaReadOnlyRepository,
            IEquipmentReadOnlyRepository equipmentReadOnlyRepository,
            IPIDReadOnlyRepository pIDReadOnlyRepository,
            IUserReadOnlyRepository userReadOnlyRepository)
        {
            _currentUserService = currentUserService;
            _shiftReadOnlyRepository = shiftReadOnlyRepository;
            _shiftWriteOnlyRepository = shiftWriteOnlyRepository;
            _areaReadOnlyRepository = areaReadOnlyRepository;
            _equipmentReadOnlyRepository = equipmentReadOnlyRepository;
            _pIDReadOnlyRepository = pIDReadOnlyRepository;
            _userReadOnlyRepository = userReadOnlyRepository;
        }

        public async Task<ShiftOutput> Execute(DateTime? date, string shiftType, string operatorId)
        {
            var defaultOperatorId = _currentUserService.GetUser().Id;
            var thisShift = ShiftNavigationOutput.ThisShift(date, shiftType, operatorId, defaultOperatorId);
            var nextShift = ShiftNavigationOutput.NextShift(date, shiftType, operatorId, defaultOperatorId);
            var prevShift = ShiftNavigationOutput.PreviousShift(date, shiftType, operatorId, defaultOperatorId);

            var output = await _shiftReadOnlyRepository.GetShift(thisShift.Date, thisShift.Shift, thisShift.FilterId);

            if (output == null)
                output = new ShiftOutput();

            output.Operator = new UserOutput(thisShift.FilterId, null, null);

            output.Date = thisShift.Date;
            output.ShiftType = thisShift.Shift;
            output.ThisShift = thisShift;
            output.NextShift = nextShift;
            output.PreviousShift = prevShift;
            output.OperatorId = thisShift.FilterId;

            return output;
        }

        public async Task AddShift(DateTime date, string shiftType, string operatorId, ShiftOutput viewModel)
        {
            int? shiftId = null;
            viewModel.Operator.UserId = operatorId;

            // get shift id
            shiftId = await _shiftReadOnlyRepository.GetShiftId(date, shiftType);

            // if not exists create new shift and then get shift id
            if (shiftId == null)
            {
                shiftId = await _shiftWriteOnlyRepository.AddShift(date, shiftType);
            }

            viewModel.ShiftId = shiftId;
            await _shiftWriteOnlyRepository.UpdateShift(viewModel);

        }

        public async Task UpdateShift(int id, ShiftOutput viewModel)
        {
            var shiftId = await _shiftWriteOnlyRepository.UpdateShift(viewModel);
        }

        public async Task AddLog(DateTime date, string shiftType, string operatorId, ShiftLogOutput viewModel)
        {
            date = date.Date;
            int? shiftId = null;
            viewModel.Operator.UserId = operatorId;
            viewModel.Operator.Name = _currentUserService.GetUser().Name;
            viewModel.Time = date.Add(viewModel.Time.TimeOfDay);

            // get shift id
            shiftId = await _shiftReadOnlyRepository.GetShiftId(date, shiftType);

            // if not exists create new shift and then get shift id
            if (shiftId == null)
            {
                shiftId = await _shiftWriteOnlyRepository.AddShift(date, shiftType);
            }


            await _shiftWriteOnlyRepository.AddShiftLog(shiftId.Value, viewModel);  
        }

        public async Task UpdateLog(int id, ShiftLogOutput viewModel)
        {
            viewModel.Time = DateTime.Today.Add(viewModel.Time.TimeOfDay);
            var shiftLogId = await _shiftWriteOnlyRepository.UpdateShiftLog(viewModel);
        }

        public Task<IEnumerable<ShiftLogOutput>> GetLogs(DateTime date, string shiftType, string operatorId)
        {
            return _shiftReadOnlyRepository.GetShiftLogs(date, shiftType, operatorId);
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _userReadOnlyRepository.GetAll();
        }


        public async Task<IEnumerable<Area>> GetAreas()
        {
            List<Area> areas = new List<Area>();

            areas.Add(Area.Load("N/A", "N/A", "N/A", "", false));

            areas.AddRange(await _areaReadOnlyRepository.GetAll());

            return areas;
        }

        public async Task<IEnumerable<Equipment>> GetEquipment(string areaId, string tag)
        {
            return await _equipmentReadOnlyRepository.GetByArea(areaId, tag);
        }

        public async Task<IEnumerable<PID>> GetPIDs(string equipmentId)
        {
            return await _pIDReadOnlyRepository.GetByEquipmentId(equipmentId);
        }




    }
}
