﻿using SSWA.Local.Core.Areas;
using SSWA.Local.Core.Equipment;
using SSWA.Local.Core.PIDs;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManageShifts
{
    public interface IManageShiftsUseCase
    {
        Task<ShiftOutput> Execute(DateTime? date, string shiftType, string operatorId);

        Task<IEnumerable<Area>> GetAreas();

        Task<IEnumerable<User>> GetUsers();

        Task<IEnumerable<Equipment>> GetEquipment(string areaId, string tag);

        Task<IEnumerable<PID>> GetPIDs(string equipmentId);

        Task<IEnumerable<ShiftLogOutput>> GetLogs(DateTime date, string shiftType, string operatorId);

        Task AddShift(DateTime date, string shiftType, string operatorId, ShiftOutput viewModel);

        Task UpdateShift(int id, ShiftOutput viewModel);
        Task AddLog(DateTime date, string shiftType, string operatorId, ShiftLogOutput viewModel);
        Task UpdateLog(int id, ShiftLogOutput viewModel);
    }
}
