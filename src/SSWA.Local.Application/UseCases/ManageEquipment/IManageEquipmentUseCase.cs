﻿using SSWA.Local.Core.Areas;
using SSWA.Local.Core.Equipment;
using SSWA.Local.Core.PIDs;
using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManageEquipment
{
    public interface IManageEquipmentUseCase
    {
        Task<IEnumerable<Equipment>> Get(int page, int pageSize);

        Task<IEnumerable<PID>> GetPIDs();

        Task<IEnumerable<Area>> GetAreas();

        Task<IEnumerable<Project>> GetProjects();

        Task<Equipment> Create(Equipment equipment);

        Task<Equipment> Update(Equipment equipment);

        Task<int> Count();
    }
}
