﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.Equipment;
using SSWA.Local.Core.PIDs;
using SSWA.Local.Core.Projects;

namespace SSWA.Local.Application.UseCases.ManageEquipment
{
    public sealed class ManageEquipmentUseCase : IManageEquipmentUseCase
    {
        private readonly IEquipmentReadOnlyRepository _equipmentReadOnlyRepository;
        private readonly IEquipmentWriteOnlyRepository _equipmentWriteOnlyRepository;
        private readonly IAreaReadOnlyRepository _areaReadOnlyRepository;
        private readonly IPIDReadOnlyRepository _pIDReadOnlyRepository;
        private readonly IProjectReadOnlyRepository _projectReadOnlyRepository;

        public ManageEquipmentUseCase(
            IEquipmentReadOnlyRepository equipmentReadOnlyRepository,
            IEquipmentWriteOnlyRepository equipmentWriteOnlyRepository,
            IAreaReadOnlyRepository areaReadOnlyRepository,
            IPIDReadOnlyRepository pIDReadOnlyRepository,
            IProjectReadOnlyRepository projectReadOnlyRepository)
        {
            _equipmentReadOnlyRepository = equipmentReadOnlyRepository;
            _equipmentWriteOnlyRepository = equipmentWriteOnlyRepository;
            _areaReadOnlyRepository = areaReadOnlyRepository;
            _pIDReadOnlyRepository = pIDReadOnlyRepository;
            _projectReadOnlyRepository = projectReadOnlyRepository;
        }

        public async Task<Equipment> Create(Equipment equipment)
        {
            await _equipmentWriteOnlyRepository.Add(equipment);

            return equipment;
        }

        public async Task<IEnumerable<Equipment>> Get(int page = 1, int pageSize = 20)
        {
            return await _equipmentReadOnlyRepository.GetAllAsync(page, pageSize);
        }

        public async Task<Equipment> Update(Equipment equipment)
        {
            await _equipmentWriteOnlyRepository.Update(equipment);

            return equipment;
        }

        public Task<int> Count()
        {
            return _equipmentReadOnlyRepository.CountAsync();
        }

        public async Task<IEnumerable<PID>> GetPIDs()
        {
            return await _pIDReadOnlyRepository.GetAll();
        }

        public async Task<IEnumerable<Area>> GetAreas()
        {
            return await _areaReadOnlyRepository.GetAll();
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            return await _projectReadOnlyRepository.GetAll();
        }
    }
}
