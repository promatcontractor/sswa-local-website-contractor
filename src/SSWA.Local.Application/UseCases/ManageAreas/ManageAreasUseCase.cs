﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Areas;

namespace SSWA.Local.Application.UseCases.ManageAreas
{
    public sealed class ManageAreasUseCase : IManageAreasUseCase
    {
        private readonly IAreaReadOnlyRepository _areaReadOnlyRepository;
        private readonly IAreaWriteOnlyRepository _areaWriteOnlyRepository;

        public ManageAreasUseCase(
            IAreaReadOnlyRepository areaReadOnlyRepository,
            IAreaWriteOnlyRepository areaWriteOnlyRepository
            )
        {
            _areaReadOnlyRepository = areaReadOnlyRepository;
            _areaWriteOnlyRepository = areaWriteOnlyRepository;
        }

        public async Task<Area> Create(Area area)
        {
            await _areaWriteOnlyRepository.Add(area);
            return area;
        }

        public async Task<IEnumerable<Area>> Get()
        {
            return await _areaReadOnlyRepository.GetAll();
        }

        public async Task<Area> Update(Area area)
        {
            await _areaWriteOnlyRepository.Update(area);
            return area;
        }
    }
}
