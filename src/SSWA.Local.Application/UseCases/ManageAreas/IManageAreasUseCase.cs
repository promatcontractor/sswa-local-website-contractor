﻿using SSWA.Local.Core.Areas;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManageAreas
{
    public interface IManageAreasUseCase
    {
        Task<IEnumerable<Area>> Get();

        Task<Area> Create(Area area);

        Task<Area> Update(Area area);

    }
}
