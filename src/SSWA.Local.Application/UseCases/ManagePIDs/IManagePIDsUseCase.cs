﻿using SSWA.Local.Core.PIDs;
using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManagePIDs
{
    public interface IManagePIDsUseCase
    {
        Task<IEnumerable<PID>> Get();

        Task<IEnumerable<Project>> GetProjects();

        Task<PID> Create(PID pid);

        Task<PID> Update(PID pid);
    }
}
