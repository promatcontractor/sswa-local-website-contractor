﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.PIDs;
using SSWA.Local.Core.Projects;

namespace SSWA.Local.Application.UseCases.ManagePIDs
{
    public class ManagePIDsUseCase : IManagePIDsUseCase
    {
        private readonly IPIDReadOnlyRepository _pidReadOnlyRepository;
        private readonly IPIDWriteOnlyRepository _pidWriteOnlyRepository;
        private readonly IProjectReadOnlyRepository _projectReadOnlyRepository;

        public ManagePIDsUseCase(
            IPIDReadOnlyRepository pidReadOnlyRepository,
            IPIDWriteOnlyRepository pidWriteOnlyRepository,
            IProjectReadOnlyRepository projectReadOnlyRepository)
        {
            _pidReadOnlyRepository = pidReadOnlyRepository;
            _pidWriteOnlyRepository = pidWriteOnlyRepository;
            _projectReadOnlyRepository = projectReadOnlyRepository;
        }

        public async Task<PID> Create(PID pid)
        {
            await _pidWriteOnlyRepository.Add(pid);
            return pid;
        }

        public async Task<IEnumerable<PID>> Get()
        {
            return await _pidReadOnlyRepository.GetAll();
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            return await _projectReadOnlyRepository.GetAll();
        }

        public async Task<PID> Update(PID pid)
        {
            await _pidWriteOnlyRepository.Update(pid);
            return pid;
        }
    }
}
