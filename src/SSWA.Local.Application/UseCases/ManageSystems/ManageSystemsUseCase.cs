﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Systems;

namespace SSWA.Local.Application.UseCases.ManageSystems
{
    public sealed class ManageSystemsUseCase : IManageSystemsUseCase
    {
        private readonly ISystemReadOnlyRepository _systemReadOnlyRepository;
        private readonly ISystemWriteOnlyRepository _systemWriteOnlyRepository;
        private readonly IAreaReadOnlyRepository _areaReadOnlyRepository;

        public ManageSystemsUseCase(
            ISystemReadOnlyRepository systemReadOnlyRepository,
            ISystemWriteOnlyRepository systemWriteOnlyRepository,
            IAreaReadOnlyRepository areaReadOnlyRepository)
        {
            _systemReadOnlyRepository = systemReadOnlyRepository;
            _systemWriteOnlyRepository = systemWriteOnlyRepository;
            _areaReadOnlyRepository = areaReadOnlyRepository;
        }

        public async Task<Core.Systems.System> Create(Core.Systems.System system)
        {
            await _systemWriteOnlyRepository.Add(system);

            return system;
        }

        public async Task<IEnumerable<Core.Systems.System>> Get()
        {
            return await _systemReadOnlyRepository.GetAll();
        }

        public async Task<IEnumerable<Core.Areas.Area>> GetAreas()
        {
            return await _areaReadOnlyRepository.GetAll();
        }

        public async Task<Core.Systems.System> Update(Core.Systems.System system)
        {
            await _systemWriteOnlyRepository.Update(system);

            return system;
        }
    }
}
