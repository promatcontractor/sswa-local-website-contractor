﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManageSystems
{
    public interface IManageSystemsUseCase
    {
        Task<IEnumerable<Core.Systems.System>> Get();

        Task<IEnumerable<Core.Areas.Area>> GetAreas();

        Task<Core.Systems.System> Create(Core.Systems.System system);

        Task<Core.Systems.System> Update(Core.Systems.System system);
    }
}
