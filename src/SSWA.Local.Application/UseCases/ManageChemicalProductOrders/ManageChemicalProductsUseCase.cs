﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.ChemicalProducts;
using SSWA.Local.Core.Users;

namespace SSWA.Local.Application.UseCases.ManageChemicalProductOrders
{
    public sealed class ManageChemicalProductOrderssUseCase : IManageChemicalProductOrdersUseCase
    {
        private readonly IChemicalProductOrderReadOnlyRepository _readOnlyRepository;
        private readonly IChemicalProductOrderWriteOnlyRepository _writeOnlyRepository;
        private readonly IChemicalProductReadOnlyRepository _chemicalProductRepository;
        private readonly IUserReadOnlyRepository _userRepository;

        public ManageChemicalProductOrderssUseCase(
            IChemicalProductOrderReadOnlyRepository readOnlyRepository,
            IChemicalProductOrderWriteOnlyRepository writeOnlyRepository,
            IChemicalProductReadOnlyRepository chemicalProductRepository,
            IUserReadOnlyRepository userRepository
            )
        {
            _readOnlyRepository = readOnlyRepository;
            _writeOnlyRepository = writeOnlyRepository;
            _chemicalProductRepository = chemicalProductRepository;
            _userRepository = userRepository; 
        }

        public async Task<IEnumerable<ChemicalProductOrder>> GetOpen()
        {
            return await _readOnlyRepository.GetAllOpen();
        }

        public async Task<ChemicalProductOrder> Create(ChemicalProductOrder order)
        {
            order.SetTime();
            ActionOrderStatus(order);

            var newOrder = await _writeOnlyRepository.Add(order);
            return newOrder;
        }



        public async Task<ChemicalProductOrder> Update(ChemicalProductOrder order)
        {
            order.SetTime();
            ActionOrderStatus(order);

            await _writeOnlyRepository.Update(order);
            return order;
        }

        private static void ActionOrderStatus(ChemicalProductOrder order)
        {

            if (order.Status == ChemicalProductOrderStatus.REQUIRED)
            {
                if (order.IdentifiedAt == null)
                {
                    order.Identify();
                }
            }

            if (order.Status == ChemicalProductOrderStatus.ORDERED)
            {
                if (order.IdentifiedAt == null)
                {
                    order.Identify();
                }

                if (order.OrderedAt == null)
                {
                    order.Order();
                }
            }

            if (order.Status == ChemicalProductOrderStatus.RECEIVED)
            {
                if (order.IdentifiedAt == null)
                {
                    order.Identify();
                }

                if (order.OrderedAt == null)
                {
                    order.Order();
                }

                if (order.ReceivedAt == null)
                {
                    order.Receive();
                }
            }

            if (!string.IsNullOrWhiteSpace(order.ArrivalAcknowledgedById) && order.Status != ChemicalProductOrderStatus.RECEIVED)
            {
                order.Identify();
                order.Order();
                order.Receive();
            }
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _userRepository.GetAll();
        }

        public async Task<IEnumerable<ChemicalProduct>> GetChemicalProducts()
        {
            return await _chemicalProductRepository.GetAll();
        }

        public async Task<IEnumerable<ChemicalProductOrder>> GetHistory(SqlParameterCollection parameters, string whereClause, string orderByClause, int page, int pageSize)
        {
            return await _readOnlyRepository.GetHistory(parameters, whereClause, orderByClause, page, pageSize);
        }

        public Task<int> GetHistoryCount(SqlParameterCollection parameters, string whereClause)
        {
            return _readOnlyRepository.GetHistoryCount(parameters, whereClause);
        }
    }
}
