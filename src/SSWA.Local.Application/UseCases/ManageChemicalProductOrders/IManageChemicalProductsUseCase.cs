﻿using SSWA.Local.Core.ChemicalProducts;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManageChemicalProductOrders
{
    public interface IManageChemicalProductOrdersUseCase
    {
        Task<IEnumerable<ChemicalProductOrder>> GetOpen();

        Task<IEnumerable<ChemicalProductOrder>> GetHistory(SqlParameterCollection parameters, string whereClause, string orderByClause, int page, int pageSize);

        Task<int> GetHistoryCount(SqlParameterCollection parameters, string whereClause);

        Task<ChemicalProductOrder> Create(ChemicalProductOrder order);

        Task<ChemicalProductOrder> Update(ChemicalProductOrder order);

        Task<IEnumerable<User>> GetUsers();

        Task<IEnumerable<ChemicalProduct>> GetChemicalProducts();
    }
}
