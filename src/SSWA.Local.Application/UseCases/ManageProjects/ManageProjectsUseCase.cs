﻿using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.CreateProject
{
    public sealed class ManageProjectsUseCase : IManageProjectsUseCase
    {
        private readonly IProjectWriteOnlyRepository _projectWriteOnlyRepository;
        private readonly IProjectReadOnlyRepository _projectReadOnlyRepository;

        public ManageProjectsUseCase(
            IProjectWriteOnlyRepository projectWriteOnlyRepository, 
            IProjectReadOnlyRepository projectReadOnlyRepository)
        {
            _projectWriteOnlyRepository = projectWriteOnlyRepository;
            _projectReadOnlyRepository = projectReadOnlyRepository;
        }

        public async Task<ProjectOutput> Create(Project project)
        {
            await _projectWriteOnlyRepository.Add(project);
            return new ProjectOutput(project);
        }

        public async Task<IEnumerable<ProjectOutput>> Get()
        {

            var projects = await _projectReadOnlyRepository.GetAll();

            return projects.Select(p => new ProjectOutput(p));
        }

        public async Task<ProjectOutput> Update(Project project)
        {
            await _projectWriteOnlyRepository.Update(project);
            return new ProjectOutput(project);
        }
    }
}
