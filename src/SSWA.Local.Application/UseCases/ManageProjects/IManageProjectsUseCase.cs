﻿using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.CreateProject
{
    public interface IManageProjectsUseCase
    {
        Task<IEnumerable<ProjectOutput>> Get();

        Task<ProjectOutput> Create(Project project);

        Task<ProjectOutput> Update(Project project);

    }
}
