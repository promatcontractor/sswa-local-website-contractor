﻿using SSWA.Local.Core.ChemicalProducts;
using SSWA.Local.Core.Lab;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManageChemicalProducts
{
    public interface IManageLabUseCase
    {
        Task<IEnumerable<LabLaboratory>> GetLaboratories();
        Task<LabLaboratory> Create(LabLaboratory entity);
        Task<LabLaboratory> Update(LabLaboratory entity);

        Task<IEnumerable<LabAnalyser>> GetAnalysers();
        Task<LabAnalyser> Create(LabAnalyser entity);
        Task<LabAnalyser> Update(LabAnalyser entity);

        Task<IEnumerable<LabAnalyte>> GetAnalytes();
        Task<LabAnalyte> Create(LabAnalyte entity);
        Task<LabAnalyte> Update(LabAnalyte entity);

        Task<IEnumerable<LabUnit>> GetUnits();
        Task<LabUnit> Create(LabUnit entity);
        Task<LabUnit> Update(LabUnit entity);

        Task<IEnumerable<LabArea>> GetAreas();
        Task<LabArea> Create(LabArea entity);
        Task<LabArea> Update(LabArea entity);

        Task<IEnumerable<LabLocation>> GetLocations();
        Task<LabLocation> Create(LabLocation entity);
        Task<LabLocation> Update(LabLocation entity);

        Task<IEnumerable<LabSampleType>> GetSampleTypes();
        Task<LabSampleType> Create(LabSampleType entity);
        Task<LabSampleType> Update(LabSampleType entity);

        Task<IEnumerable<User>> GetUsers();
    }
}
