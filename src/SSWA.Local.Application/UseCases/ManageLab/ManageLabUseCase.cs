﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.ChemicalProducts;
using SSWA.Local.Core.Lab;
using SSWA.Local.Core.Users;

namespace SSWA.Local.Application.UseCases.ManageChemicalProducts
{
    public sealed class ManageLabUseCase : IManageLabUseCase
    {
        private readonly IReadOnlyRepository<LabAnalyser, string> _readOnlyAnalyserRepository;
        private readonly IWriteOnlyRepository<LabAnalyser, string> _writeOnlyAnalyserRepository;
        private readonly IReadOnlyRepository<LabAnalyte, int> _readOnlyAnalyteRepository;
        private readonly IWriteOnlyRepository<LabAnalyte, int> _writeOnlyAnalyteRepository;
        private readonly IReadOnlyRepository<LabArea, int> _readOnlyAreaRepository;
        private readonly IWriteOnlyRepository<LabArea, int> _writeOnlyAreaRepository;
        private readonly IReadOnlyRepository<LabLocation, int> _readOnlyLocationRepository;
        private readonly IWriteOnlyRepository<LabLocation, int> _writeOnlyLocationRepository;
        private readonly IReadOnlyRepository<LabLaboratory, int> _readOnlyLaboratoryRepository;
        private readonly IWriteOnlyRepository<LabLaboratory, int> _writeOnlyLaboratoryRepository;
        private readonly IReadOnlyRepository<LabUnit, int> _readOnlyUnitRepository;
        private readonly IWriteOnlyRepository<LabUnit, int> _writeOnlyUnitRepository;
        private readonly IReadOnlyRepository<LabSampleType, int> _readOnlySampleTypeRepository;
        private readonly IWriteOnlyRepository<LabSampleType, int> _writeOnlySampleTypeRepository;
        private readonly IUserReadOnlyRepository _userRepository;

        public ManageLabUseCase(
            IReadOnlyRepository<LabAnalyser, string> readOnlyAnalyserRepository,
            IWriteOnlyRepository<LabAnalyser, string> writeOnlyAnalyserRepository,

            IReadOnlyRepository<LabAnalyte, int> readOnlyAnalyteRepository,
            IWriteOnlyRepository<LabAnalyte, int> writeOnlyAnalyteRepository,

            IReadOnlyRepository<LabArea, int> readOnlyAreaRepository,
            IWriteOnlyRepository<LabArea, int> writeOnlyAreaRepository,

            IReadOnlyRepository<LabLocation, int> readOnlyLocationRepository,
            IWriteOnlyRepository<LabLocation, int> writeOnlyLocationRepository,

            IReadOnlyRepository<LabLaboratory, int> readOnlyLaboratoryRepository,
            IWriteOnlyRepository<LabLaboratory, int> writeOnlyLaboratoryRepository,

            IReadOnlyRepository<LabUnit, int> readOnlyUnitRepository,
            IWriteOnlyRepository<LabUnit, int> writeOnlyUnitRepository,

            IReadOnlyRepository<LabSampleType, int> readOnlySampleTypeRepository,
            IWriteOnlyRepository<LabSampleType, int> writeOnlySampleTypeRepository,

            IUserReadOnlyRepository userRepository
            )
        {
            _readOnlyAnalyserRepository = readOnlyAnalyserRepository;
            _writeOnlyAnalyserRepository = writeOnlyAnalyserRepository;
            _readOnlyAnalyteRepository = readOnlyAnalyteRepository;
            _writeOnlyAnalyteRepository = writeOnlyAnalyteRepository;
            _readOnlyAreaRepository = readOnlyAreaRepository;
            _writeOnlyAreaRepository = writeOnlyAreaRepository;
            _readOnlyLocationRepository = readOnlyLocationRepository;
            _writeOnlyLocationRepository = writeOnlyLocationRepository;
            _readOnlyLaboratoryRepository = readOnlyLaboratoryRepository;
            _writeOnlyLaboratoryRepository = writeOnlyLaboratoryRepository;
            _readOnlyUnitRepository = readOnlyUnitRepository;
            _writeOnlyUnitRepository = writeOnlyUnitRepository;
            _readOnlySampleTypeRepository = readOnlySampleTypeRepository;
            _writeOnlySampleTypeRepository = writeOnlySampleTypeRepository;
            _userRepository = userRepository;
        }

        public async Task<LabLaboratory> Create(LabLaboratory entity)
        {
            entity.Id = await _writeOnlyLaboratoryRepository.Add(entity);
            return entity;
        }
        public async Task<LabAnalyser> Create(LabAnalyser entity)
        {
            entity.Id = await _writeOnlyAnalyserRepository.Add(entity);
            return entity;
        }
        public async Task<LabAnalyte> Create(LabAnalyte entity)
        {
            entity.Id = await _writeOnlyAnalyteRepository.Add(entity);
            return entity;
        }
        public async Task<LabUnit> Create(LabUnit entity)
        {
            entity.Id = await _writeOnlyUnitRepository.Add(entity);
            return entity;
        }
        public async Task<LabArea> Create(LabArea entity)
        {
            entity.Id = await _writeOnlyAreaRepository.Add(entity);
            return entity;
        }
        public async Task<LabLocation> Create(LabLocation entity)
        {
            entity.Id = await _writeOnlyLocationRepository.Add(entity);
            return entity;
        }

        public async Task<LabSampleType> Create(LabSampleType entity)
        {
            entity.Id = await _writeOnlySampleTypeRepository.Add(entity);
            return entity;
        }


        public async Task<IEnumerable<LabAnalyser>> GetAnalysers()
        {
            return await _readOnlyAnalyserRepository.GetAll();
        }

        public async Task<IEnumerable<LabAnalyte>> GetAnalytes()
        {
            return await _readOnlyAnalyteRepository.GetAll();
        }

        public async Task<IEnumerable<LabArea>> GetAreas()
        {
            return await _readOnlyAreaRepository.GetAll();
        }

        public async Task<IEnumerable<LabLaboratory>> GetLaboratories()
        {
            return await _readOnlyLaboratoryRepository.GetAll();
        }

        public async Task<IEnumerable<LabLocation>> GetLocations()
        {
            return await _readOnlyLocationRepository.GetAll();
        }

        public async Task<IEnumerable<LabUnit>> GetUnits()
        {
            return await _readOnlyUnitRepository.GetAll();
        }

        public async Task<IEnumerable<LabSampleType>> GetSampleTypes()
        {
            return await _readOnlySampleTypeRepository.GetAll();
        }

        public async Task<LabLaboratory> Update(LabLaboratory entity)
        {
            await _writeOnlyLaboratoryRepository.Update(entity);
            return entity;
        }

        public async Task<LabAnalyser> Update(LabAnalyser entity)
        {
            await _writeOnlyAnalyserRepository.Update(entity);
            return entity;
        }

        public async Task<LabAnalyte> Update(LabAnalyte entity)
        {
            await _writeOnlyAnalyteRepository.Update(entity);
            return entity;
        }

        public async Task<LabUnit> Update(LabUnit entity)
        {
            await _writeOnlyUnitRepository.Update(entity);
            return entity;
        }

        public async Task<LabArea> Update(LabArea entity)
        {
            await _writeOnlyAreaRepository.Update(entity);
            return entity;
        }

        public async Task<LabLocation> Update(LabLocation entity)
        {
            await _writeOnlyLocationRepository.Update(entity);
            return entity;
        }

        public async Task<LabSampleType> Update(LabSampleType entity)
        {
            await _writeOnlySampleTypeRepository.Update(entity);
            return entity;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _userRepository.GetAll();
        }
    }
}
