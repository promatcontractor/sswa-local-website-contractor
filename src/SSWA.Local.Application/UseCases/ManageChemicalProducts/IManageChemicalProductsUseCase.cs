﻿using SSWA.Local.Core.ChemicalProducts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Application.UseCases.ManageChemicalProducts
{
    public interface IManageChemicalProductsUseCase
    {
        Task<IEnumerable<ChemicalProduct>> Get();

        Task<ChemicalProduct> Create(ChemicalProduct chemicalProduct);

        Task<ChemicalProduct> Update(ChemicalProduct chemicalProduct);

    }
}
