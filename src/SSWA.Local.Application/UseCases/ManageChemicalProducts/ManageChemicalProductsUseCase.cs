﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.ChemicalProducts;

namespace SSWA.Local.Application.UseCases.ManageChemicalProducts
{
    public sealed class ManageChemicalProductsUseCase : IManageChemicalProductsUseCase
    {
        private readonly IChemicalProductReadOnlyRepository _readOnlyRepository;
        private readonly IChemicalProductWriteOnlyRepository _writeOnlyRepository;

        public ManageChemicalProductsUseCase(
            IChemicalProductReadOnlyRepository readOnlyRepository,
            IChemicalProductWriteOnlyRepository writeOnlyRepository
            )
        {
            _readOnlyRepository = readOnlyRepository;
            _writeOnlyRepository = writeOnlyRepository;
        }

        public async Task<ChemicalProduct> Create(ChemicalProduct chemicalProduct)
        {
            await _writeOnlyRepository.Add(chemicalProduct);
            return chemicalProduct;
        }

        public async Task<IEnumerable<ChemicalProduct>> Get()
        {
            return await _readOnlyRepository.GetAll();
        }

        public async Task<ChemicalProduct> Update(ChemicalProduct chemicalProduct)
        {
            await _writeOnlyRepository.Update(chemicalProduct);
            return chemicalProduct;
        }
    }
}
