﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SSWA.Local.Application
{
    public sealed class ShiftLogOutput
    {
        public int LogId { get; set; }

        public UserOutput Operator { get; set; }

        [Required]
        public DateTime Time { get; set;  }

        [Required]
        public string AreaId { get; set; }

        public string AreaName { get; set; }

        public string Tag { get; set; }

        public string PID { get; set; }

        public string Description { get; set; }

        public bool MaintenanceRequired { get; set; }

        public string MaintenanceDescription { get; set; }

        public string MaximoRefNo { get; set; }

        public string Comments { get; set; }

        public ShiftLogOutput() { }

        public ShiftLogOutput(
            int logId,
            UserOutput @operator,
            DateTime time,
            string areaId,
            string areaName,
            string tag,
            string pid,
            string decription,
            bool maintenanceRequired,
            string maintenanceDescription,
            string maximoRefNo,
            string comments)
        {
            LogId = logId;
            Operator = @operator;
            Time = time;
            AreaId = areaId;
            AreaName = areaName;
            Tag = tag;
            PID = pid;
            Description = Description;
            MaintenanceRequired = maintenanceRequired;
            MaintenanceDescription = maintenanceDescription;
            MaximoRefNo = maximoRefNo;
            Comments = comments;
        }
    }
}
