﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Application
{
    public class InspectionOutput
    {
        public int? ShiftId { get; set; }

        public DateTime Date { get; set; }

        public string ShiftType { get; set; }

        public string Project { get; set; }

        public string AreaId { get; set; }

        public ShiftNavigationOutput ThisShift { get; set; }

        public ShiftNavigationOutput NextShift { get; set; }

        public ShiftNavigationOutput PreviousShift { get; set; }
    }
}
