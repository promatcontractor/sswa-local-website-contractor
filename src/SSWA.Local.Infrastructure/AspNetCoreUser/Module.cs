﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.AspNetCoreUser
{
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //
            // Register all Types in MongoDataAccess namespace
            //
            builder.RegisterAssemblyTypes(typeof(InfrastructureException).Assembly)
                .Where(type => type.Namespace.Contains("AspNetCoreUser"))
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
