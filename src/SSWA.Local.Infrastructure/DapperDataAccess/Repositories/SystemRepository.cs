﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Areas;
using SSWA.Local.Core.Systems;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class SystemRepository : ISystemReadOnlyRepository, ISystemWriteOnlyRepository
    {
        private readonly string _connectionString;

        public SystemRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Add(Core.Systems.System equipment)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@id", equipment.Id);

                string checkExists = "Select Count(*) From [System] Where SystemId = @Id";

                var count = (await db.QueryAsync<int>(checkExists, param)).First();

                if (count != 0) throw new InfrastructureException("A System with this ID already exists.");

                string sql = "INSERT INTO [System] (SystemId, SystemName, Notes, Area) " +
                    "Values (@Id, @SystemName, @Notes,  @Area)";


                param.Add("@SystemName", equipment.SystemName);
                param.Add("@Notes", equipment.Notes);
                param.Add("@Area", equipment.Area);

                int rowsAffected = await db.ExecuteAsync(sql, param);
            }
        }

        public Task Delete(Core.Systems.System equipment)
        {
            throw new NotImplementedException();
        }

        public Task<Core.Systems.System> Get(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<IReadOnlyCollection<Core.Systems.System>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [System]";
                IEnumerable<Entities.System> systems = await db
                    .QueryAsync<Entities.System>(sql);

                if (systems == null)
                    return null;

                List<Core.Systems.System> results = new List<Core.Systems.System>();

                foreach (var sys in systems)
                {
                    results.Add(Core.Systems.System.Load(sys.SystemId, sys.SystemName, sys.Notes, sys.Area));
                }
                return results;
            }
        }

        public async Task Update(Core.Systems.System equipment)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {

                string sql = "UPDATE [System] " +
                    "SET SystemName = @SystemName, Notes = @Notes, Area = @Area " +
                    "WHERE SystemId = @Id";

                DynamicParameters param = new DynamicParameters();
                param.Add("@id", equipment.Id);
                param.Add("@SystemName", equipment.SystemName);
                param.Add("@Notes", equipment.Notes);
                param.Add("@Area", equipment.Area);

                int rowsAffected = await db.ExecuteAsync(sql, param);
            }
        }
    }
}
