﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class UserRepository : IUserReadOnlyRepository, IUserWriteOnlyRepository
    {
        private readonly string _connectionString;

        public UserRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Add(User user)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string insertAccountSQL = 
                    "INSERT INTO [User] (Id, Name, Reference, Operator, Admin, Approver) " +
                    "VALUES (@Id, @Name, @Reference, @Operator, @Admin, @Approver)";

                DynamicParameters UserParameters = new DynamicParameters();
                UserParameters.Add("@id", user.Id);
                UserParameters.Add("@name", user.Name);
                UserParameters.Add("@reference", user.Reference);
                UserParameters.Add("@operator", user.Roles.AsList().Contains("operator"));
                UserParameters.Add("@admin", user.Roles.AsList().Contains("admin"));
                UserParameters.Add("@approver", user.Roles.AsList().Contains("approver"));

                int affectedRows = await db.ExecuteAsync(insertAccountSQL, UserParameters);
            }
        }

        public async Task Delete(User user)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [User] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, user);
            }
        }

        public async Task<User> Get(string id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string userSQL = @"SELECT * FROM [User] WHERE Id = @Id";
                Entities.User user = await db
                    .QueryFirstOrDefaultAsync<Entities.User>(userSQL, new { id });

                if (user == null)
                    return null;

                User result = User.Load(user.Id, user.Reference, user.Name, ToRolesCollection(user));
                return result;
            }
        }

        public async Task<IReadOnlyCollection<User>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string UserSql = @"SELECT * FROM [User]";
                IEnumerable<Entities.User> users = await db
                    .QueryAsync<Entities.User>(UserSql);

                if (users == null)
                    return null;

                List<User> results = new List<User>();

                foreach (var user in users)
                {
                    results.Add(User.Load(user.Id, user.Reference, user.Name, ToRolesCollection(user)));
                }

                return results;
            }
        }

        public async Task Update(User user)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = "UPDATE [User] " +
                    "SET Name = @Name, " +
                    "Operator = @Operator, " +
                    "Admin = @Admin, " +
                    "Approver = @Approver " +
                    "WHERE Id = @Id";

                DynamicParameters transactionParameters = new DynamicParameters();
                transactionParameters.Add("@id", user.Id);
                transactionParameters.Add("@name", user.Name);
                transactionParameters.Add("@operator", user.Roles.AsList().Contains("operator"));
                transactionParameters.Add("@admin", user.Roles.AsList().Contains("admin"));
                transactionParameters.Add("@approver", user.Roles.AsList().Contains("approver"));

                int rowsAffected = await db.ExecuteAsync(sql, transactionParameters);
            }
        }

        private RolesCollection ToRolesCollection(Entities.User user)
        {
            RolesCollection roles = new RolesCollection();
            if (user.Operator)
            {
                roles.Add("operator");
            }

            if(user.Admin)
            {
                roles.Add("admin");
            }

            if (user.Approver)
            {
                roles.Add("approver");
            }

            return roles;
        }
    }

}
