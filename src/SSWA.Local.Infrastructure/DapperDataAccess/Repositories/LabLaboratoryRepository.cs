﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Lab;
using SSWA.Local.Core.PIDs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class LabLaboratoryRepository : IReadOnlyRepository<LabLaboratory, int>, IWriteOnlyRepository<LabLaboratory, int>
    {
        private readonly string _connectionString;

        public LabLaboratoryRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Add(LabLaboratory entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string insertAccountSQL = 
                    "INSERT INTO [LabLaboratory] " +
                    "(Name, AddressLine1, AddressLine2, Suburb, State, Postcode, Phone, Fax, Email, Notes) " +
                    "VALUES (@Name, @AddressLine1, @AddressLine2, @Suburb, @State, @Postcode, @Phone, @Fax, @Email, @Notes);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    return await db.QueryFirstOrDefaultAsync<int>(insertAccountSQL, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabLaboratory] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, id);
            }
        }

        public async Task<LabLaboratory> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabLaboratory] WHERE Id = @Id";
                LabLaboratory entity = await db
                    .QueryFirstOrDefaultAsync<LabLaboratory>(sql, new { id });

                if (entity == null)
                    return null;
               
                return entity;
            }
        }

        public async Task<IReadOnlyCollection<LabLaboratory>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabLaboratory]";
                IEnumerable<LabLaboratory> entities = await db
                    .QueryAsync<LabLaboratory>(sql);

                if (entities == null)
                    return null;

                return entities.ToList();
            }
        }

        public async Task Update(LabLaboratory entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = "UPDATE [LabLaboratory] " +
                    "SET " +
                    "Name = @Name, " +
                    "AddressLine1 = @AddressLine1, " +
                    "AddressLine2 = @AddressLine2, " +
                    "Suburb = @Suburb, " +
                    "State = @State, " +
                    "Postcode = @Postcode, " +
                    "Phone = @Phone, " +
                    "Fax = @Fax, " +
                    "Email = @Email, " +
                    "Notes = @Notes " +
                    "WHERE Id = @Id";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
