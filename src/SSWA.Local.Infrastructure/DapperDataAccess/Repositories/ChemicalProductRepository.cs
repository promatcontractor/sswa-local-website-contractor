﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.ChemicalProducts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class ChemicalProductRepository : IChemicalProductReadOnlyRepository, IChemicalProductWriteOnlyRepository
    {
        private readonly string _connectionString;

        public ChemicalProductRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Add(ChemicalProduct chemicalProduct)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string insertAccountSQL = "INSERT INTO [ChemicalProduct] (Chemical, Type, Supplier, Quantity, _OldId) VALUES (@Chemical, @Type, @Supplier, @Quantity, -1)";

                try
                {
                    int affectedRows = await db.ExecuteAsync(insertAccountSQL, chemicalProduct);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(ChemicalProduct chemicalProduct)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [ChemicalProduct] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, chemicalProduct);
            }
        }

        public async Task<ChemicalProduct> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [ChemicalProduct] WHERE Id = @Id";
                Entities.ChemicalProductRecord chemicalProduct = await db
                    .QueryFirstOrDefaultAsync<Entities.ChemicalProductRecord>(sql, new { id });

                if (chemicalProduct == null)
                    return null;

                ChemicalProduct result = ChemicalProduct
                    .Load(chemicalProduct.Id, chemicalProduct.Chemical, chemicalProduct.Type, chemicalProduct.Supplier, chemicalProduct.Quantity);
                return result;
            }
        }

        public async Task<IReadOnlyCollection<ChemicalProduct>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [ChemicalProduct]";
                IEnumerable<Entities.ChemicalProductRecord> chemicalProducts = await db
                    .QueryAsync<Entities.ChemicalProductRecord>(sql);

                if (chemicalProducts == null)
                    return null;

                List<ChemicalProduct> results = new List<ChemicalProduct>();

                foreach (var chemcialProduct in chemicalProducts)
                {
                    results.Add(ChemicalProduct.Load(chemcialProduct.Id, chemcialProduct.Chemical, chemcialProduct.Type, chemcialProduct.Supplier, chemcialProduct.Quantity));
                }
                return results;
            }
        }

        public async Task Update(ChemicalProduct chemicalProduct)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = "UPDATE [ChemicalProduct] " +
                    "SET Chemical = @Chemical, " +
                    "Type = @Type, " +
                    "Supplier = @Supplier, " +
                    "Quantity = @Quantity " +
                    "WHERE Id = @Id";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, chemicalProduct);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
