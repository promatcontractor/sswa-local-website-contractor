﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.PIDs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class PIDRepository : IPIDReadOnlyRepository, IPIDWriteOnlyRepository
    {
        private readonly string _connectionString;

        public PIDRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Add(PID equipment)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@id", equipment.Id);

                string checkExists = "Select Count(*) From [PID] Where PID = @Id";

                var count = (await db.QueryAsync<int>(checkExists, param)).First();

                if (count != 0) throw new InfrastructureException("A PID with this number already exists.");

                string sql = "INSERT INTO [PID] (PID, PID_Title, Revision, Date_Modified, Notes, ProjectId) " +
                    "Values (@Id, @PIDTitle, @Revision,  @DateModified,  @Notes, @ProjectId)";


                param.Add("@PIDTitle", equipment.PIDTitle);
                param.Add("@Revision", equipment.Revision);
                param.Add("@DateModified", equipment.DateModified);
                param.Add("@Notes", equipment.Notes);
                param.Add("@ProjectId", equipment.ProjectId);

                int rowsAffected = await db.ExecuteAsync(sql, param);
            }
        }

        public Task Delete(PID equipment)
        {
            throw new NotImplementedException();
        }

        public Task<PID> Get(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<IReadOnlyCollection<PID>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT PID, PID_Title as PIDTitle, Revision, Date_Modified As DateModified, Notes, ProjectId FROM [PID]";
                IEnumerable<Entities.PIDRecord> pids = await db
                    .QueryAsync<Entities.PIDRecord>(sql);

                if (pids == null)
                    return null;

                List<PID> results = new List<PID>();

                foreach (var pid in pids)
                {
                    results.Add(PID.Load(pid.PID, pid.PIDTitle, pid.Revision, pid.DateModified, pid.Notes, pid.ProjectId));
                }
                return results;
            }
        }

        public async Task<IReadOnlyCollection<PID>> GetByEquipmentId(string equipmentId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"
                    SELECT p.PID, p.PID_Title as PIDTitle, p.Revision, p.Date_Modified As DateModified, p.Notes, p.ProjectId 
                    FROM [PID] p 
                    INNER JOIN Equipment e ON e.PID = p.PID
                    WHERE e.TAG = @equipment";

                var equipment = equipmentId;


                IEnumerable<Entities.PIDRecord> pids = await db
                    .QueryAsync<Entities.PIDRecord>(sql, new { equipment });

                if (pids == null)
                    return null;

                List<PID> results = new List<PID>();

                foreach (var pid in pids)
                {
                    results.Add(PID.Load(pid.PID, pid.PIDTitle, pid.Revision, pid.DateModified, pid.Notes, pid.ProjectId));
                }
                return results;
            }
        }

        public async Task Update(PID equipment)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {


                string sql = 
                    "UPDATE [PID] " +
                    "SET PID_Title = @PIDTitle, Revision = @Revision, Date_Modified = @DateModified, Notes = @Notes, ProjectId = @ProjectId " +
                    "WHERE PID = @Id";

                DynamicParameters param = new DynamicParameters();
                param.Add("@id", equipment.Id);
                param.Add("@PIDTitle", equipment.PIDTitle);
                param.Add("@Revision", equipment.Revision);
                param.Add("@DateModified", equipment.DateModified);
                param.Add("@Notes", equipment.Notes);
                param.Add("@ProjectId", equipment.ProjectId);

                int rowsAffected = await db.ExecuteAsync(sql, param);
            }
        }
    }
}
