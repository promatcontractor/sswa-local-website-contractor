﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Lab;
using SSWA.Local.Core.PIDs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class LabLocationRepository : IReadOnlyRepository<LabLocation, int>, IWriteOnlyRepository<LabLocation, int>
    {
        private readonly string _connectionString;

        public LabLocationRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Add(LabLocation entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string insertAccountSQL = 
                    "INSERT INTO [LabLocation] " +
                    "(Tag, Location, AreaId, Comments) " +
                    "VALUES (@Tag, @Location, @AreaId, @Comments);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    return await db.QueryFirstOrDefaultAsync<int>(insertAccountSQL, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabLocation] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, id);
            }
        }

        public async Task<LabLocation> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabLocation] WHERE Id = @Id";
                LabLocation entity = await db
                    .QueryFirstOrDefaultAsync<LabLocation>(sql, new { id });

                if (entity == null)
                    return null;
               
                return entity;
            }
        }

        public async Task<IReadOnlyCollection<LabLocation>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabLocation]";
                IEnumerable<LabLocation> entities = await db
                    .QueryAsync<LabLocation>(sql);

                if (entities == null)
                    return null;

                return entities.ToList();
            }
        }

        public async Task Update(LabLocation entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = "UPDATE [LabLocation] " +
                    "SET " +
                    "Tag = @Tag, " +
                    "Location = @Location, " +
                    "AreaId = @AreaId, " +
                    "Comments = @Comments " +
                    "WHERE Id = @Id";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
