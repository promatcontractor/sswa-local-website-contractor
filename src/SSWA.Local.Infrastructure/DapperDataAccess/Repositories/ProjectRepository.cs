﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Projects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class ProjectRepository : IProjectReadOnlyRepository, IProjectWriteOnlyRepository
    {
        private readonly string _connectionString;

        public ProjectRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Add(Project project)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@id", project.Id);
                string checkExists = "Select Count(*) From [Project] Where Id = @Id";

                var count = (await db.QueryAsync<int>(checkExists, param)).First();

                if (count != 0) throw new InfrastructureException("A tag with this ID already exists.");

                string sql = "INSERT INTO [Project] (Id, Name) VALUES (@Id, @Name)";

                param.Add("@name", project.Name);

                int affectedRows = await db.ExecuteAsync(sql, param);
            }
        }

        public async Task Delete(Project project)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [Project] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, project);
            }
        }

        public async Task<Project> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string projectSql = @"SELECT * FROM [Project] WHERE Id = @Id";
                Entities.Project project = await db
                    .QueryFirstOrDefaultAsync<Entities.Project>(projectSql, new { id });

                if (project == null)
                    return null;

                Project result = Project.Load(project.Id, project.Name);
                return result;
            }
        }

        public async Task<IReadOnlyCollection<Project>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string projectSql = @"SELECT * FROM [Project]";
                IEnumerable<Entities.Project> projects = await db
                    .QueryAsync<Entities.Project>(projectSql);

                if (projects == null)
                    return null;

                List<Project> results = new List<Project>();

                foreach (var project in projects)
                {
                    results.Add(Project.Load(project.Id, project.Name));
                }
                return results;
            }
        }

        public async Task Update(Project project)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = "UPDATE [Project] " +
                    "SET Name = @Name " +
                    "WHERE Id = @Id";

                DynamicParameters transactionParameters = new DynamicParameters();
                transactionParameters.Add("@id", project.Id);
                transactionParameters.Add("@name", project.Name);

                int rowsAffected = await db.ExecuteAsync(sql, transactionParameters);
            }
        }
    }
}
