﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Equipment;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class EquipmentRepository : IEquipmentReadOnlyRepository, IEquipmentWriteOnlyRepository
    {
        private readonly string _connectionString;

        public EquipmentRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Add(Equipment equipment)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@id", equipment.Id);

                string checkExists = "Select Count(*) From [Equipment] Where Tag = @Id";

                var count = (await db.QueryAsync<int>(checkExists, param)).First();

                if (count != 0) throw new InfrastructureException("A tag with this ID already exists.");

                string sql = "INSERT INTO [Equipment] (Tag, Area, Description, PID, Prefix, INUM, Type, Notes, ProjectId, TAG_ORIGINAL " +
                    "Values(@Id, @AreaId, @description,  @PID,  @Prefix, @INum, @Type, @Notes, @ProjectId, @TagOriginal)";


                param.Add("@areaid", equipment.Area);
                param.Add("@description", equipment.Description);
                param.Add("@pid", equipment.PID);
                param.Add("@prefix", equipment.Prefix);
                param.Add("@inum", equipment.Inum);
                param.Add("@type", equipment.Type);
                param.Add("@notes", equipment.Notes);
                param.Add("@projectid", equipment.ProjectId);
                param.Add("@tagoriginal", equipment.TagOriginal);

                int rowsAffected = await db.ExecuteAsync(sql, param);
            }
        }

        public Task Delete(Equipment equipment)
        {
            throw new NotImplementedException();
        }

        public Task<Equipment> Get(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<IReadOnlyCollection<Equipment>> GetAllAsync(int page = 1, int pageSize = 20)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"
                    SELECT * FROM ( 
                        SELECT ROW_NUMBER() OVER (ORDER BY TAG ASC) AS Row, TAG As Tag, Area, Description, PID, PREFIX As Prefix, INUM as Inum, Type, Notes, ProjectId, TAG_Original as TagOriginal 
                        FROM [Equipment] 
                    ) u 
                    WHERE u.Row > @start and u.Row <= @end 
                    ORDER BY u.Row
                ";
                var start = (page - 1) * pageSize;
                var end = page * pageSize;


                IEnumerable<Entities.EquipmentRecord> equipment = await db
                    .QueryAsync<Entities.EquipmentRecord>(sql, new { start, end });

                if (equipment == null)
                    return null;

                List<Equipment> results = new List<Equipment>();

                foreach (var item in equipment)
                {
                    results.Add(Equipment.Load(item.Tag, item.Description, item.Prefix, item.Inum, item.Type, item.Notes, item.TagOriginal, item.Area, item.PID, item.ProjectId));
                }
                return results;
            }
        }

        public async Task<int> CountAsync()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var count = await db.QueryFirstAsync<int>("SELECT COUNT(1) FROM [Equipment]");

                return count;
            }
        }

        public async Task Update(Equipment equipment)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = "UPDATE [Equipment] " +
                    "SET " +
                    "Area = @AreaId, " +
                    "Descrption = @description, " +
                    "PID = @PID, " +
                    "Prefix = @Prefix, " +
                    "INUM = @INum, " +
                    "Type = @Type, " +
                    "Notes = @Notes," +
                    "ProjectId = @ProjectId, " +
                    "TAG_Original = @TagOriginal " +
                    "WHERE Tag = @Id";

                DynamicParameters param = new DynamicParameters();
                param.Add("@id", equipment.Id);
                param.Add("@areaid", equipment.Area);
                param.Add("@description", equipment.Description);
                param.Add("@pid", equipment.PID);
                param.Add("@prefix", equipment.Prefix);
                param.Add("@inum", equipment.Inum);
                param.Add("@type", equipment.Type);
                param.Add("@notes", equipment.Notes);
                param.Add("@projectid", equipment.ProjectId);
                param.Add("@tagoriginal", equipment.TagOriginal);

                int rowsAffected = await db.ExecuteAsync(sql, param);
            }
        }

        public async Task<IReadOnlyCollection<Equipment>> GetByArea(string areaId, string tag)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string filter = "WHERE (Area = @area) ";
                if (string.IsNullOrWhiteSpace(areaId))
                {
                    filter = "WHERE 1 = 1 ";
                }

                if (!string.IsNullOrWhiteSpace(tag))
                {
                    filter += "AND Tag Like @tag + '%'";
                }

                string sql = $@"
                    SELECT TOP 25 TAG As Tag, Area, Description, PID, PREFIX As Prefix, INUM as Inum, Type, Notes, ProjectId, TAG_Original as TagOriginal 
                    FROM [Equipment] 
                    {filter}
                    ORDER BY Tag
                ";
                var area = areaId;


                IEnumerable<Entities.EquipmentRecord> equipment = await db
                    .QueryAsync<Entities.EquipmentRecord>(sql, new { area, tag});

                if (equipment == null)
                    return null;

                List<Equipment> results = new List<Equipment>();

                foreach (var item in equipment)
                {
                    results.Add(Equipment.Load(item.Tag, item.Description, item.Prefix, item.Inum, item.Type, item.Notes, item.TagOriginal, item.Area, item.PID, item.ProjectId));
                }
                return results;
            }
        }
    }
}
