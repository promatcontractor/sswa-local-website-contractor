﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Lab;
using SSWA.Local.Core.PIDs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class LabAnalyteRepository : IReadOnlyRepository<LabAnalyte, int>, IWriteOnlyRepository<LabAnalyte, int>
    {
        private readonly string _connectionString;

        public LabAnalyteRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Add(LabAnalyte entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string insertAccountSQL = 
                    "INSERT INTO [LabAnalyte] " +
                    "(Symbol, Title, UnitId, Comments) " +
                    "VALUES (@Symbol, @Title, @UnitId, @Comments);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    return await db.QueryFirstOrDefaultAsync<int>(insertAccountSQL, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabAnalyte] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, id);
            }
        }

        public async Task<LabAnalyte> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabAnalyte] WHERE Id = @Id";
                LabAnalyte entity = await db
                    .QueryFirstOrDefaultAsync<LabAnalyte>(sql, new { id });

                if (entity == null)
                    return null;
               
                return entity;
            }
        }

        public async Task<IReadOnlyCollection<LabAnalyte>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabAnalyte]";
                IEnumerable<LabAnalyte> entities = await db
                    .QueryAsync<LabAnalyte>(sql);

                if (entities == null)
                    return null;

                return entities.ToList();
            }
        }

        public async Task Update(LabAnalyte entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = "UPDATE [LabAnalyte] " +
                    "SET " +
                    "Symbol = @Symbol, " +
                    "Title = @Title, " +
                    "UnitId = @UnitId, " +
                    "Comments = @Comments " +
                    "WHERE Id = @Id";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
