﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Lab;
using SSWA.Local.Core.PIDs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class LabAnalyserRepository : IReadOnlyRepository<LabAnalyser, string>, IWriteOnlyRepository<LabAnalyser, string>
    {
        private readonly string _connectionString;

        public LabAnalyserRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<string> Add(LabAnalyser entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@id", entity.Id);

                string checkExists = "Select Count(*) From [LabAnalyser] Where Id = @Id";

                var count = (await db.QueryAsync<int>(checkExists, param)).First();

                if (count != 0) throw new InfrastructureException("An analyser with this ID already exists.");

                string insertAccountSQL = 
                    "INSERT INTO [LabAnalyser] " +
                    "(Id, Description, LocationId, Comments) " +
                    "VALUES (@Id, @Description, @LocationId, @Comments)";

                try
                {
                    int affectedRows = await db.ExecuteAsync(insertAccountSQL, entity);
                    return entity.Id;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(string id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabAnalyser] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, id);
            }
        }

        public async Task<LabAnalyser> Get(string id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabAnalyser] WHERE Id = @Id";
                LabAnalyser entity = await db
                    .QueryFirstOrDefaultAsync<LabAnalyser>(sql, new { id });

                if (entity == null)
                    return null;
               
                return entity;
            }
        }

        public async Task<IReadOnlyCollection<LabAnalyser>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabAnalyser]";
                IEnumerable<LabAnalyser> entities = await db
                    .QueryAsync<LabAnalyser>(sql);

                if (entities == null)
                    return null;

                return entities.ToList();
            }
        }

        public async Task Update(LabAnalyser entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = "UPDATE [LabAnalyser] " +
                    "SET " +
                    "Description = @Description, " +
                    "LocationId = @LocationId, " +
                    "Comments = @Comments " +
                    "WHERE Id = @Id";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
