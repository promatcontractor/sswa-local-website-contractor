﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Areas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class AreaRepository : IAreaReadOnlyRepository, IAreaWriteOnlyRepository
    {
        private readonly string _connectionString;

        public AreaRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Add(Area area)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@id", area.Id);

                string checkExists = "Select Count(*) From [Area] Where AreaId = @Id";

                var count = (await db.QueryAsync<int>(checkExists, param)).First();

                if (count != 0) throw new InfrastructureException("An Area with this ID already exists.");

                string insertAccountSQL = "INSERT INTO [Area] (AreaId, Area, Title, Comments, UseArea) VALUES (@Id, @Name, @Title, @Comments, @UseArea)";

                param.Add("@name", area.Name);
                param.Add("@title", area.Title);
                param.Add("@comments", area.Comments);
                param.Add("@usearea", area.UseArea);

                int affectedRows = await db.ExecuteAsync(insertAccountSQL, param);
            }
        }

        public async Task Delete(Area area)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [Area] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, area);
            }
        }

        public async Task<Area> Get(string id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string areaSql = @"SELECT * FROM [Area] WHERE AreaId = @Id";
                Entities.AreaRecord area = await db
                    .QueryFirstOrDefaultAsync<Entities.AreaRecord>(areaSql, new { id });

                if (area == null)
                    return null;

                Area result = Area.Load(area.AreaId, area.Area, area.Title, area.Comments, area.UseArea);
                return result;
            }
        }

        public async Task<IReadOnlyCollection<Area>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string areaSql = @"SELECT * FROM [Area] Order By ProjectId, Area";
                IEnumerable<Entities.AreaRecord> areas = await db
                    .QueryAsync<Entities.AreaRecord>(areaSql);

                if (areas == null)
                    return null;

                List<Area> results = new List<Area>();

                foreach (var area in areas)
                {
                    results.Add(Area.Load(area.AreaId, area.Area, area.Title, area.Comments, area.UseArea));
                }
                return results;
            }
        }

        public async Task<IReadOnlyCollection<Area>> GetByProjectId(string projectId, bool inspectionAreasOnly = false)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string areaSql = @"SELECT Distinct a.* FROM [Area] a ";
                if (inspectionAreasOnly)
                {
                    areaSql += "INNER JOIN SSWA_ShiftLog.dbo.InspectionItems i On i.Area = a.AreaId ";
                }
                if (!string.IsNullOrWhiteSpace(projectId) && projectId.ToLowerInvariant() != "all")
                {
                    areaSql += "WHERE ProjectName = @projectId And UseArea = 1 ";
                }
                else
                {
                    areaSql += "WHERE UseArea = 1 ";
                }

                areaSql += "Order By ProjectId, Area";

                IEnumerable<Entities.AreaRecord> areas = await db
                    .QueryAsync<Entities.AreaRecord>(areaSql, new { projectId });

                if (areas == null)
                    return null;

                List<Area> results = new List<Area>();

                foreach (var area in areas)
                {
                    results.Add(Area.Load(area.AreaId, area.Area, area.Title, area.Comments, area.UseArea));
                }
                return results;
            }
        }

        public async Task Update(Area area)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = 
                    "UPDATE [Area] " +
                    "SET " +
                        "Area = @Name, " +
                        "Title = @Title, " +
                        "Comments = @Comments, " + 
                        "UseArea = @UseArea " +
                    "WHERE AreaId = @Id";

                DynamicParameters areaParameters = new DynamicParameters();
                areaParameters.Add("@id", area.Id);
                areaParameters.Add("@name", area.Name);
                areaParameters.Add("@title", area.Title);
                areaParameters.Add("@comments", area.Comments);
                areaParameters.Add("@usearea", area.UseArea);

                int rowsAffected = await db.ExecuteAsync(sql, areaParameters);
            }
        }
    }
}
