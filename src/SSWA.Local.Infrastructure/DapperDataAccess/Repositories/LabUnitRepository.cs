﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Lab;
using SSWA.Local.Core.PIDs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class LabUnitRepository : IReadOnlyRepository<LabUnit, int>, IWriteOnlyRepository<LabUnit, int>
    {
        private readonly string _connectionString;

        public LabUnitRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Add(LabUnit entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string insertAccountSQL = 
                    "INSERT INTO [LabUnit] " +
                    "(Unit, Factor, [Order], Comments) " +
                    "VALUES (@Unit, @Factor, @Order, @Comments);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    return await db.QueryFirstOrDefaultAsync<int>(insertAccountSQL, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabUnit] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, id);
            }
        }

        public async Task<LabUnit> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabUnit] WHERE Id = @Id";
                LabUnit entity = await db
                    .QueryFirstOrDefaultAsync<LabUnit>(sql, new { id });

                if (entity == null)
                    return null;
               
                return entity;
            }
        }

        public async Task<IReadOnlyCollection<LabUnit>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabUnit]";
                IEnumerable<LabUnit> entities = await db
                    .QueryAsync<LabUnit>(sql);

                if (entities == null)
                    return null;

                return entities.ToList();
            }
        }

        public async Task Update(LabUnit entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = "UPDATE [LabUnit] " +
                    "SET " +
                    "Unit = @Unit, " +
                    "Factor = @Factor, " +
                    "[Order] = @Order, " +
                    "Comments = @Comments " +
                    "WHERE Id = @Id";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
