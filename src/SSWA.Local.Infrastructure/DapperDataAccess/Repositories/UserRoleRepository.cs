﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class UserRoleRepository : IPositionReadOnlyRepository
    {
        private readonly string _connectionString;

        public UserRoleRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IReadOnlyCollection<Position>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [UserRole] Order By Roles";
                IEnumerable<Entities.UserRoleRecord> positions = await db
                    .QueryAsync<Entities.UserRoleRecord>(sql);

                if (positions == null)
                    return null;

                List<Position> results = new List<Position>();

                foreach (var position in positions)
                {
                    results.Add(Position.Load(position.RoleId, position.Roles));
                }
                return results;
            }
        }
    }
}
