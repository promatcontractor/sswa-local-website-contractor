﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Lab;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Repositories
{
    public class LabAreaRepository : IReadOnlyRepository<LabArea, int>, IWriteOnlyRepository<LabArea, int>
    {
        private readonly string _connectionString;

        public LabAreaRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<int> Add(LabArea entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string insertAccountSQL = 
                    "INSERT INTO [LabArea] " +
                    "(Name, Description, Comments) " +
                    "VALUES (@Name, @Description, @Comments);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    return await db.QueryFirstOrDefaultAsync<int>(insertAccountSQL, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabArea] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, id);
            }
        }

        public async Task<LabArea> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabArea] WHERE Id = @Id";
                LabArea entity = await db
                    .QueryFirstOrDefaultAsync<LabArea>(sql, new { id });

                if (entity == null)
                    return null;
               
                return entity;
            }
        }

        public async Task<IReadOnlyCollection<LabArea>> GetAll()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * FROM [LabArea]";
                IEnumerable<LabArea> entities = await db
                    .QueryAsync<LabArea>(sql);

                if (entities == null)
                    return null;

                return entities.ToList();
            }
        }

        public async Task Update(LabArea entity)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = "UPDATE [LabArea] " +
                    "SET " +
                    "Name = @Name, " +
                    "Description = @Description, " +
                    "Comments = @Comments " +
                    "WHERE Id = @Id";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, entity);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
