﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Entities
{
    public sealed class User
    {
        public string Id { get; set; }

        public int? Reference { get; set; }

        public string Name { get; set; }

        public bool Operator { get; set; }

        public bool Admin { get; set; }

        public bool Approver { get; set; }
    }
}
