﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Entities
{
    public class ChemicalProductRecord
    {
        public int Id { get; set; }

        public string Chemical { get; set; }

        public string Type { get; set; }

        public string Supplier { get; set; }

        public string Quantity { get; set; }
    }

}
