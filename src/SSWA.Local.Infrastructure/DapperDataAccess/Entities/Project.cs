﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Entities
{
    public sealed class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
