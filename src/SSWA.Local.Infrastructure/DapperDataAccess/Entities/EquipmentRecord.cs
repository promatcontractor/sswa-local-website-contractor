﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Entities
{
    public class EquipmentRecord
    {
        public string Tag { get; set; }

        public string Description { get; set; }

        public string Prefix { get; set; }

        public string Inum { get; set; }

        public string Type { get; set; }

        public string Notes { get; set; }

        public string TagOriginal { get; set; }

        public string Area { get; set; }

        public string PID { get; set; }

        public int ProjectId { get; set; }
    }
}
