﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Entities
{
    public class UserRoleRecord
    {
        public int RoleId { get; set; }

        public string Roles { get; set; }
    }
}
