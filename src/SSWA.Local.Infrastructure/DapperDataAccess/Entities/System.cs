﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperDataAccess.Entities
{
    public class System
    {
        // Key
        public string SystemId { get; set; }

        public string SystemName { get; set; }

        public string Notes { get; set; }

        public string Area { get; set; }
    }
}
