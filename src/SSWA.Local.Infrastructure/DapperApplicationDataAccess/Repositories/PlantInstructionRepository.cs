﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.PlantInstructions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Repositories
{
    public class PlantInstructionRepository : IPlantInstructionReadOnlyRepository, IPlantInstructionWriteOnlyRepository
    {
        private readonly string _connectionString;

        public PlantInstructionRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task Add(PlantInstruction instruction)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "INSERT INTO [PlantInstructionRegister] (Title, Description, Status, Duration, DurationUnits, DateCreated, OperatorUserId, InstructionFromRoleId, AuthorisedByUserId)" +
                    "VALUES (@Title, @Description, @Status, @Duration, @DurationUnits, @DateCreated, @OperatorUserId, @InstructionFromRoleId, @AuthorisedByUserId)";

                try
                {
                    await db.ExecuteAsync(sql, instruction);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(PlantInstruction instruction)
        {
            throw new NotImplementedException();
        }

        public async Task<PlantInstruction> Get(int id)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "SELECT * " +
                    "FROM [PlantInstructionRegister] " +
                    "Where Id = @id";

                IEnumerable<Entities.PlantInstructionRecord> instructions =
                    await db.QueryAsync<Entities.PlantInstructionRecord>(sql, id);

                if (instructions == null)
                    return null;

                List<PlantInstruction> results = new List<PlantInstruction>();

                foreach (var instruction in instructions)
                {

                    results.Add(PlantInstruction.Load(
                        instruction.Id,
                        instruction.Title,
                        instruction.Description,
                        instruction.Status,
                        instruction.Duration,
                        instruction.DurationUnits,
                        instruction.DateCreated,
                        instruction.OperatorUserId,
                        instruction.InstructionFromRoleId,
                        instruction.AuthorisedByUserId
                    ));
                }

                return results.FirstOrDefault();
            }
        }

        public async Task<IReadOnlyCollection<PlantInstruction>> GetAll(SqlParameterCollection parameters, string whereClause, string orderByClause)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    $"SELECT * " +
                    $"FROM [PlantInstructionRegister] " +
                    $"{whereClause} " +
                    $"{orderByClause}";

                var args = new DynamicParameters(new { });
                foreach (SqlParameter parameter in parameters)
                {
                    args.Add(parameter.ParameterName, parameter.Value);
                }

                IEnumerable<Entities.PlantInstructionRecord> instructions =
                    await db.QueryAsync<Entities.PlantInstructionRecord>(sql, args);

                if (instructions == null)
                    return null;

                List<PlantInstruction> results = new List<PlantInstruction>();

                foreach (var instruction in instructions)
                {

                    results.Add(PlantInstruction.Load(
                        instruction.Id,
                        instruction.Title,
                        instruction.Description,
                        instruction.Status,
                        instruction.Duration,
                        instruction.DurationUnits,
                        instruction.DateCreated,
                        instruction.OperatorUserId,
                        instruction.InstructionFromRoleId,
                        instruction.AuthorisedByUserId
                    ));
                }

                return results;
            }
        }

        public async Task Update(PlantInstruction instruction)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "UPDATE [PlantInstructionRegister] " +
                    "SET " +
                        "Title = @Title," +
                        "Description = @Description, " +
                        "Status = @Status, " +
                        "Duration = @Duration, " +
                        "DurationUnits = @DurationUnits, " +
                        "DateCreated = @DateCreated, " +
                        "OperatorUserId = @OperatorUserId, " +
                        "InstructionFromRoleId = @InstructionFromRoleId, " +
                        "AuthorisedByUserId = @AuthorisedByUserId " +
                    "WHERE Id = @Id";

                try
                {
                    await db.ExecuteAsync(sql, instruction);
                }
                catch (Exception ex)
                {
                    throw;
                }

            }
        }
    }
}
