﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.Lab;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Repositories
{
    public class LabSampleTemplateRepository : ILabSampleTemplateRepository 
    {
        private readonly string _connectionString;

        public LabSampleTemplateRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<LabSampleTemplateHeader> Get(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                    SELECT * 
                    FROM LabSampleTemplateHeader
                    WHERE Id = @Id
                ";

                var header = await connection.QueryFirstOrDefaultAsync<LabSampleTemplateHeader>(sql, new { id });

                return header ?? null;
            }
        }

        public async Task<IReadOnlyCollection<LabSampleTemplateHeader>> GetList(SqlParameterCollection parameters, string whereClause, string orderByClause, int page, int pageSize)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                    SELECT * FROM (
                        SELECT     
                            ROW_NUMBER() OVER ({orderByClause}) As [Row]
                            ,*
                        FROM [LabSampleTemplateHeader]
                        {whereClause}
                    ) u WHERE u.Row > @start and u.Row <= @end
                    ORDER BY u.Row";

                var args = new DynamicParameters(new { });
                foreach (SqlParameter parameter in parameters)
                {
                    args.Add(parameter.ParameterName, parameter.Value);
                }

                args.Add("start", (page - 1) * pageSize);
                args.Add("end", page * pageSize);

                var templates = await connection.QueryAsync<LabSampleTemplateHeader>(sql, args);

                if (templates == null)
                    return null;

                return templates.ToList();
            }
        }

        public async Task<int> GetListCount(SqlParameterCollection parameters, string whereClause)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                        SELECT     
                            Count(*)
                         FROM [LabSampleTemplateHeader]
                        {whereClause}";

                var args = new DynamicParameters(new { });
                foreach (SqlParameter parameter in parameters)
                {
                    args.Add(parameter.ParameterName, parameter.Value);
                }

                return (await connection.QueryAsync<int>(sql, args)).FirstOrDefault();
            }
        }

        public async Task<LabSampleTemplateHeader> Add(LabSampleTemplateHeader header)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = 
                    "INSERT INTO [LabSampleTemplateHeader] " +
                    "([Name], [LocationId], [SampleType], [External], [Comments]) " +
                    "VALUES " +
                    "(@Name, @LocationId, @SampleType, @External, @Comments);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    int id = (await db.QueryAsync<int>(sql, header)).FirstOrDefault();
                    header.Id = id;

                    return header;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Update(LabSampleTemplateHeader header)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "UPDATE [LabSampleTemplateHeader] " +
                    "SET "  +
                        "[Name] = @Name, " +
                        "[LocationId] = @LocationId, " +
                        "[SampleType] = @SampleType, " +
                        "[External] = @External, " +
                        "[Comments] = @Comments " +
                    "WHERE Id = @Id";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, header);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(LabSampleTemplateHeader header)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabSampleTemplateHeader] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, header);
            }
        }

        public async Task<IReadOnlyCollection<LabSampleTemplateDetail>> GetDetails(int templateId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                        SELECT     
                            *
                        FROM [LabSampleTemplateDetails]
                        WHERE TemplateId = @TemplateId
                    ";

                var details = await connection.QueryAsync<LabSampleTemplateDetail>(sql, new { templateId });

                if (details == null)
                    return null;

                return details.ToList();
            }
        }

        public async Task<LabSampleTemplateDetail> AddDetail(LabSampleTemplateDetail detail)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "INSERT INTO [LabSampleTemplateDetails] " +
                    "([TemplateId], [AnalyserId], [AnalyteId], [ExpectedMinimum], [ExpectedMaximum], LowerLimit, UpperLimit, UnitId, Comments) " +
                    "VALUES " +
                    "(@TemplateId, @AnalyserId, @AnalyteId, @ExpectedMinimum, @ExpectedMaximum, @LowerLimit, @UpperLimit, @UnitId, @Comments);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    int id = (await db.QueryAsync<int>(sql, detail)).FirstOrDefault();
                    detail.Id = id;

                    return detail;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task UpdateDetail(LabSampleTemplateDetail detail)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "UPDATE [LabSampleTemplateDetails] " +
                    "SET " +
                        "[AnalyserId] = @AnalyserId, " +
                        "[AnalyteId] = @AnalyteId, " +
                        "[ExpectedMinimum] = @ExpectedMinimum, " +
                        "[ExpectedMaximum] = @ExpectedMaximum, " +
                        "[LowerLimit] = @LowerLimit, " +
                        "[UpperLimit] = @UpperLimit, " +
                        "[UnitId] = @UnitId, " +
                        "[Comments] = @Comments " +
                    "WHERE Id = @Id And TemplateId = @TemplateId";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, detail);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task DeleteDetail(LabSampleTemplateDetail detail)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabSampleTemplateDetails] WHERE Id = @Id And TemplateId = @TemplateId;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, detail);
            }
        }
    }
}
