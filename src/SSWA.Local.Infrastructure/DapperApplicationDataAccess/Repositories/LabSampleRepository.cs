﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.ChemicalProducts;
using SSWA.Local.Core.Lab;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Repositories
{
    public class LabSampleRepository : ILabSampleRepository 
    {
        private readonly string _connectionString;

        public LabSampleRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<LabSampleHeader> Get(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                    SELECT * 
                    FROM LabSampleHeader
                    WHERE Id = @Id
                ";

                var header = await connection.QueryFirstOrDefaultAsync<LabSampleHeader>(sql, new { id });

                return header ?? null;
            }
        }

        public async Task<IReadOnlyCollection<LabSampleHeader>> GetList(SqlParameterCollection parameters, string whereClause, string orderByClause, int page, int pageSize)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                    SELECT * FROM (
                        SELECT     
                            ROW_NUMBER() OVER ({orderByClause}) As [Row]
                            ,*
                        FROM [LabSampleHeader]
                        {whereClause}
                    ) u WHERE u.Row > @start and u.Row <= @end
                    ORDER BY u.Row";

                var args = new DynamicParameters(new { });
                foreach (SqlParameter parameter in parameters)
                {
                    args.Add(parameter.ParameterName, parameter.Value);
                }

                args.Add("start", (page - 1) * pageSize);
                args.Add("end", page * pageSize);

                var templates = await connection.QueryAsync<LabSampleHeader>(sql, args);

                if (templates == null)
                    return null;

                return templates.ToList();
            }
        }

        public async Task<int> GetListCount(SqlParameterCollection parameters, string whereClause)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                        SELECT     
                            Count(*)
                         FROM [LabSampleHeader]
                        {whereClause}";

                var args = new DynamicParameters(new { });
                foreach (SqlParameter parameter in parameters)
                {
                    args.Add(parameter.ParameterName, parameter.Value);
                }

                return (await connection.QueryAsync<int>(sql, args)).FirstOrDefault();
            }
        }

        public async Task<LabSampleHeader> Add(LabSampleHeader header)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = 
                    "INSERT INTO [LabSampleHeader] " +
                    "([TemplateId], [SampleDateTime], [LocationId], [SampleType], [SampledBy], [AnalysedBy], [LaboratoryId], [External], [LabReference], DateSamplesSent, DateResultsReceived, ResultsCheckedBy, Comments) " +
                    "VALUES " +
                    "(@TemplateId, @SampleDateTime, @LocationId, @SampleType, @SampledBy, @AnalysedBy, @LaboratoryId, @External, @LabReference, @DateSamplesSent, @DateResultsReceived, @ResultsCheckedBy, @Comments);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    int id = (await db.QueryAsync<int>(sql, header)).FirstOrDefault();
                    header.Id = id;

                    return header;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Update(LabSampleHeader header)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "UPDATE [LabSampleHeader] " +
                    "SET "  +
                        "[TemplateId] = @TemplateId, " +
                        "[SampleDateTime] = @SampleDateTime, " +
                        "[LocationId] = @LocationId, " +
                        "[SampleType] = @SampleType, " +
                        "[SampledBy] = @SampledBy, " +
                        "[AnalysedBy] = @AnalysedBy, " +
                        "[LaboratoryId] = @LaboratoryId, " +
                        "[External] = @External, " +
                        "[LabReference] = @LabReference, " +
                        "[DateSamplesSent] = @DateSamplesSent, " +
                        "[DateResultsReceived] = @DateResultsReceived, " +
                        "[ResultsCheckedBy] = @ResultsCheckedBy, " +
                        "[Comments] = @Comments " +
                    "WHERE Id = @Id";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, header);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(LabSampleHeader header)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabSampleHeader] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, header);
            }
        }

        public async Task<IReadOnlyCollection<LabSampleDetail>> GetDetails(int sampleId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                        SELECT     
                            *
                        FROM [LabSampleDetails]
                        WHERE SampleId = @SampleId
                    ";

                var details = await connection.QueryAsync<LabSampleDetail>(sql, new { sampleId });

                if (details == null)
                    return null;

                return details.ToList();
            }
        }

        public async Task<LabSampleDetail> AddDetail(LabSampleDetail detail)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "INSERT INTO [LabSampleDetails] " +
                    "([SampleId], [TemplateDetailId], [AnalyserId], [AnalyteId], [UnitId], AnalyserResult, LabResult, ExternalResult, Symbol, OutOfRange, LowerLimit, UpperLimit, ExpectedRange, Comments) " +
                    "VALUES " +
                    "(@SampleId, @TemplateDetailId, @AnalyserId, @AnalyteId, @UnitId, @AnalyserResult, @LabResult, @ExternalResult, @Symbol, @OutOfRange, @LowerLimit, @UpperLimit, @ExpectedRange, @Comments);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    int id = (await db.QueryAsync<int>(sql, detail)).FirstOrDefault();
                    detail.Id = id;

                    return detail;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task UpdateDetail(LabSampleDetail detail)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "UPDATE [LabSampleDetails] " +
                    "SET " +
                        "[TemplateDetailId] = @TemplateDetailId, " +
                        "[AnalyserId] = @AnalyserId, " +
                        "[AnalyteId] = @AnalyteId, " +
                        "[UnitId] = @UnitId, " +
                        "[AnalyserResult] = @AnalyserResult, " +
                        "[LabResult] = @LabResult, " +
                        "[ExternalResult] = @ExternalResult, " +
                        "[Symbol] = @Symbol, " +
                        "[OutOfRange] = @OutOfRange, " +
                        "[LowerLimit] = @LowerLimit, " +
                        "[UpperLimit] = @UpperLimit, " +
                        "[ExpectedRange] = @ExpectedRange, " +
                        "[Comments] = @Comments " +
                    "WHERE Id = @Id And SampleId = @SampleId";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, detail);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task DeleteDetail(LabSampleDetail detail)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabSampleDetails] WHERE Id = @Id And SampleId = @SampleId;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, detail);
            }
        }


        public async Task<IReadOnlyCollection<LabSampleAttachment>> GetAttachments(int sampleId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                        SELECT     
                            *
                        FROM [LabSampleAttachments]
                        WHERE SampleId = @SampleId
                    ";

                var details = await connection.QueryAsync<LabSampleAttachment>(sql, sampleId);

                if (details == null)
                    return null;

                return details.ToList();
            }
        }

        public async Task<LabSampleAttachment> AddAttachment(LabSampleAttachment attachment)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "INSERT INTO [LabSampleAttachments] " +
                    "([SampleId], [Description], [LinkAddress]) " +
                    "VALUES " +
                    "(@SampleId, @Description, @LinkAddress);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    int id = (await db.QueryAsync<int>(sql, attachment)).FirstOrDefault();
                    attachment.Id = id;

                    return attachment;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task UpdateAttachment(LabSampleAttachment attachment)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "UPDATE [LabSampleAttachments] " +
                    "SET " +
                        "[Description] = @Description, " +
                        "[LinkAddress] = @LinkAddress " +
                    "WHERE Id = @Id And SampleId = @SampleId";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, attachment);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task DeleteAttachment(LabSampleAttachment attachment)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [LabSampleAttachments] WHERE Id = @Id And SampleId = @SampleId;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, attachment);
            }
        }
    }
}
