﻿using Dapper;
using SSWA.Local.Application;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Infrastructure.DapperApplicationDataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Repositories
{
    public class InspectionRepository : IInspectionReadOnlyRepository, IInpsectionWriteOnlyRepository
    {
        private readonly string _connectionString;

        public InspectionRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task AddInspectionLog(int shiftId, int inspectionItemId, InspectionLogOutput record)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {

                string sql =
                    "INSERT INTO [InspectionLog] (ShiftId, InspectionItemId, Value) " +
                    "VALUES (@ShiftId, @InspectionItemId, @Value)";

                DynamicParameters param = new DynamicParameters();
                param.Add("@ShiftId", shiftId);
                param.Add("@InspectionItemId", inspectionItemId);
                param.Add("@Value", record.Value?.Value ?? "");

                try
                {
                    int affectedRows = await db.ExecuteAsync(sql, param);

                }
                catch (Exception ex)
                {
                    throw;
                }


                return;
            }
        }

        public async Task<int?> GetInspectionItemIdForShiftAsync(int shiftId, int inspectionItemId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"
                    SELECT m.InspectionItemId
                    FROM [InspectionLog] m 
                    WHERE m.ShiftId = @shiftId And m.InspectionItemId = @inspectionItemId";
                try
                {

                    int? itemId = (await db.QueryAsync<int?>(sql, new { shiftId, inspectionItemId })).SingleOrDefault();

                    return itemId;
                }
                catch (Exception ex)
                {

                    throw;
                }

            }
        }

        public async Task<IEnumerable<InspectionLogOutput>> GetInspectionLogs(DateTime date, string shiftType, string project, string areaId)
        {
            var results = new List<InspectionLogOutput>();

            var sql =
                "SELECT ii.Id as InspectionItemId, ii.InspectionItemName, a.AreaId, a.Area As AreaName, ii.DataType, il.Value, iio.SelectionOption As [Option] " +
                "FROM [InspectionItems] ii " +
                "INNER JOIN [SSWA_Master].dbo.[Area] a On a.AreaId = ii.Area " +
                "LEFT JOIN [InspectionItemOptions] iio ON ii.Id = iio.InspectionItemId " +
                "LEFT JOIN [ShiftLog_Master] sl ON sl.Date = @Date AND sl.ShiftType = @ShiftType " +
                "LEFT JOIN [InspectionLog] il ON il.ShiftId = sl.ShiftId AND il.InspectionItemId = ii.Id " +
                "WHERE ii.Enabled = 1 "; 

            if (!(string.IsNullOrWhiteSpace(project) || project.ToLowerInvariant() == "all"))
            {
                sql += "AND a.ProjectName = @Project ";

                if (!(string.IsNullOrWhiteSpace(areaId) || areaId.ToLowerInvariant() == "all"))
                {
                    sql += "AND a.AreaId = @AreaId ";
                }
            }
            else if (!(string.IsNullOrWhiteSpace(areaId) || areaId.ToLowerInvariant() == "all"))
            {
                sql += "AND a.AreaId = @AreaId ";
            }
            else
            {
                sql += "AND 1 = 1 ";
            }

            sql += "ORDER BY a.SortOrder, ii.SortOrder, iio.SelectionOption";

            DynamicParameters param = new DynamicParameters();
            param.Add("@Date", date);
            param.Add("@ShiftType", shiftType);
            param.Add("@AreaId", areaId);
            param.Add("@Project", project);

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                var lookup = new Dictionary<int, InspectionLogRecord>();

                try
                {
                    await db.QueryAsync<InspectionLogRecord, string, InspectionLogRecord>(
                        sql,
                        (s, a) =>
                        {
                            if (!lookup.TryGetValue(s.InspectionItemId, out InspectionLogRecord inpsectionItem))
                            {
                                lookup.Add(s.InspectionItemId, inpsectionItem = s);
                            }

                            if (inpsectionItem.Options == null)
                            {
                                inpsectionItem.Options = new List<string>();
                            }

                            inpsectionItem.Options.Add(a);
                            return inpsectionItem;
                        },
                        param: param,
                        splitOn: "Option"
                    );
                }
                catch (Exception ex)
                {
                    throw;
                }


                var recordList = lookup.Values;

                foreach (var record in recordList)
                {
                    results.Add(new InspectionLogOutput
                    {
                        AreaId = record.AreaId,
                        AreaName = record.AreaName,
                        InspectionItemId = record.InspectionItemId,
                        InspectionItemName = record.InspectionItemName,
                        Value = new InspectionLogValue
                        {
                            Options = record.Options,
                            Type = record.DataType,
                            Value = record.Value
                        }
                    });
                }
            }

            return results;
        }

        public async Task UpdateInpectionLog(int shiftId, int inspectionItemId, InspectionLogOutput record)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {

                string sql =
                    "UPDATE [InspectionLog] " +
                    "SET [Value] = @Value " +
                    "WHERE  ShiftId = @ShiftId AND InspectionItemId = @InspectionItemId";

                DynamicParameters param = new DynamicParameters();
                param.Add("@ShiftId", shiftId);
                param.Add("@InspectionItemId", inspectionItemId);
                param.Add("@Value", record.Value?.Value ?? "");

                try
                {
                    int affectedRows = await db.ExecuteAsync(sql, param);

                }
                catch (Exception ex)
                {
                    throw;
                }


                return;
            }
        }
    }
}
