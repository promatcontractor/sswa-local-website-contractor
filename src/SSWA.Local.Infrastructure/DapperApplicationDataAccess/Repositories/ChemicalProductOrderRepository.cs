﻿using Dapper;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Core.ChemicalProducts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Repositories
{
    public class ChemicalProductOrderRepository : IChemicalProductOrderReadOnlyRepository, IChemicalProductOrderWriteOnlyRepository
    {
        private readonly string _connectionString;

        public ChemicalProductOrderRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<ChemicalProductOrder> Add(ChemicalProductOrder order)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = 
                    "INSERT INTO [ChemicalProductOrder] " +
                    "([ChemicalProductId], [Status], [ExpectedDeliveryDate], [ExpectedDeliveryTime], [ArrivalAcknowledgedById], [IdentifiedAt], [OrderedAt], [ReceivedAt], [Comments]) " +
                    "VALUES " +
                    "(@ChemicalProductId, @Status, @ExpectedDeliveryDate, @ExpectedDeliveryTime, @ArrivalAcknowledgedById, @IdentifiedAt, @OrderedAt, @ReceivedAt, @Comments);" +
                    "SELECT SCOPE_IDENTITY();";

                try
                {
                    int id = (await db.QueryAsync<int>(sql, order)).FirstOrDefault();
                    var insertedOrder = ChemicalProductOrder.Load(
                        id, order.ChemicalProductId, order.Status, order.ExpectedDeliveryDate, order.ExpectedDeliveryTime, 
                        order.ArrivalAcknowledgedById, order.IdentifiedAt, order.OrderedAt, order.ReceivedAt, order.Comments);

                    return insertedOrder;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public async Task Delete(ChemicalProductOrder order)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string deleteSQL =
                    @"DELETE FROM [ChemicalProductOrder] WHERE Id = @Id;";

                int rowsAffected = await db.ExecuteAsync(deleteSQL, order);
            }
        }

        public async Task<IReadOnlyCollection<ChemicalProductOrder>> GetAllOpen()
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = 
                    "SELECT * " +
                    "FROM [ChemicalProductOrder] " +
                    "WHERE Status != 'Cancelled' AND (ArrivalAcknowledgedById Is Null And ReceivedAt Is Null) " +
                    "ORDER By Id";

                IEnumerable<Entities.ChemicalProductOrderRecord> orders = 
                    await db.QueryAsync<Entities.ChemicalProductOrderRecord>(sql);

                if (orders == null)
                    return null;

                List<ChemicalProductOrder> results = new List<ChemicalProductOrder>();

                foreach (var order in orders)
                {
                    DateTime? deliveryTime = 
                        order.ExpectedDeliveryTime == null ? 
                        (DateTime?) null : 
                        DateTime.MinValue.Add(order.ExpectedDeliveryTime.Value);

                    results.Add(ChemicalProductOrder.Load(
                        order.Id, 
                        order.ChemicalProductId, 
                        order.Status, 
                        order.ExpectedDeliveryDate,
                        deliveryTime, 
                        order.ArrivalAcknowledgedById, 
                        order.IdentifiedAt, 
                        order.OrderedAt, 
                        order.ReceivedAt, 
                        order.Comments
                    ));
                }

                return results;
            }
        }

        public async Task<IReadOnlyCollection<ChemicalProductOrder>> GetHistory(SqlParameterCollection parameters, string whereClause, string orderByClause, int page, int pageSize)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                    SELECT * FROM (
                        SELECT     
                            ROW_NUMBER() OVER ({orderByClause}) As [Row]
                            ,*
                        FROM [ChemicalProductOrder]
                        {whereClause}
                    ) u WHERE u.Row > @start and u.Row <= @end
                    ORDER BY u.Row";

                var args = new DynamicParameters(new { });
                foreach (SqlParameter parameter in parameters)
                {
                    args.Add(parameter.ParameterName, parameter.Value);
                }

                args.Add("start", (page - 1) * pageSize);
                args.Add("end", page * pageSize);

                var orders = await connection.QueryAsync<Entities.ChemicalProductOrderRecord>(sql, args);

                if (orders == null)
                    return null;

                List<ChemicalProductOrder> results = new List<ChemicalProductOrder>();

                foreach (var order in orders)
                {
                    DateTime? deliveryTime =
                        order.ExpectedDeliveryTime == null ?
                        (DateTime?)null :
                        DateTime.MinValue.Add(order.ExpectedDeliveryTime.Value);

                    results.Add(ChemicalProductOrder.Load(
                        order.Id,
                        order.ChemicalProductId,
                        order.Status,
                        order.ExpectedDeliveryDate,
                        deliveryTime,
                        order.ArrivalAcknowledgedById,
                        order.IdentifiedAt,
                        order.OrderedAt,
                        order.ReceivedAt,
                        order.Comments
                    ));
                }

                return results;
            }
        }

        public async Task<int> GetHistoryCount(SqlParameterCollection parameters, string whereClause)
        {
            using (var connection = new SqlConnection(_connectionString))
            {

                var sql = $@"
                        SELECT     
                            Count(*)
                         FROM [ChemicalProductOrder]
                        {whereClause}";

                var args = new DynamicParameters(new { });
                foreach (SqlParameter parameter in parameters)
                {
                    args.Add(parameter.ParameterName, parameter.Value);
                }

                return (await connection.QueryAsync<int>(sql, args)).FirstOrDefault();
            }
        }

        public async Task Update(ChemicalProductOrder order)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = 
                    "UPDATE [ChemicalProductOrder] " +
                    "SET "  +
                        "[Status] = @Status, " +
                        "[ExpectedDeliveryDate] = @ExpectedDeliveryDate, " +
                        "[ExpectedDeliveryTime] = @ExpectedDeliveryTime, " +
                        "[ArrivalAcknowledgedById] = @ArrivalAcknowledgedById, " +
                        "[IdentifiedAt] = @IdentifiedAt, " +
                        "[OrderedAt] = @OrderedAt, " +
                        "[ReceivedAt] = @ReceivedAt, " +
                        "[Comments] = @Comments " +
                    "WHERE Id = @Id";

                try
                {
                    int rowsAffected = await db.ExecuteAsync(sql, order);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
