﻿using Dapper;
using SSWA.Local.Application;
using SSWA.Local.Application.Repositories;
using SSWA.Local.Infrastructure.DapperApplicationDataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Repositories
{
    public class ShiftRepository : IShiftReadOnlyRepository, IShiftWriteOnlyRepository
    {
        private readonly string _connectionString;

        public ShiftRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<ShiftOutput> GetShift(DateTime date, string shiftType, string operatorId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;

                string sql = @"
                    SELECT m.*
                    FROM [ShiftLog_Master] m 
                    WHERE m.Date = @date And m.ShiftType = @shiftType";


                ShiftMasterRecord shift = (await db
                    .QueryAsync<ShiftMasterRecord>(sql, new { date = date.Date, shiftType })).SingleOrDefault();

                if (shift == null)
                    return null;

                var result = new ShiftOutput();

                if (shift.LowTideTime !=null )
                {
                    result.LowTideTime = DateTime.MinValue.Add(shift.LowTideTime.Value);
                };

                result.Comments = shift.Comments;
                result.OHSEComments = shift.OHSEComments;
                result.MaxForecastWindSpeed = shift.MaxFCastWindSpeed;
                result.Date = date;
                result.OHSEComments = shift.OHSEComments;
                result.RainForecast = shift.RainForecast;
                result.ShiftType = shiftType;
                result.Swell = shift.Swell;
                result.ShiftId = shift.ShiftId;

                return result;
            }
        }

        public async Task<int> AddShift(DateTime date, string shiftType)
        {
            var shiftId = await GetShiftId(date, shiftType);
            if (shiftId != null) return shiftId.Value;

            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string nextId = "SELECT Max(ShiftId) + 1 From [ShiftLog_Master]";
                shiftId = (await db.QueryAsync<int>(nextId)).Single();


                string sql = "INSERT INTO [ShiftLog_Master] (ShiftId, Date, ShiftType) VALUES (@shiftId, @date, @shiftType)";

                DynamicParameters param = new DynamicParameters();
                param.Add("@shiftId", shiftId);
                param.Add("@date", date);
                param.Add("@shiftType", shiftType);

                await db.ExecuteAsync(sql, param);

                return shiftId.Value;
            }
        }

        public async Task<int> AddShiftLog(int shiftId, ShiftLogOutput log)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string nextId = "SELECT Max(LogId) + 1 From [ShiftLog_Detail]";
                var logId = (await db.QueryAsync<int>(nextId)).Single();


                string sql = 
                    "INSERT INTO [ShiftLog_Detail] (LogId, ShiftLogId, Time, Operator, UserId, AreaId, Tag, PID, Description, MaintenanceRequired, MaintenanceDescription, MaximoRefNo, Comments) " +
                    "VALUES (@LogId, @ShiftLogId, @Time, -1, @UserId, @AreaId, @Tag, @Pid, @Description, @MaintenanceRequired, @MaintenanceDescription, @MaximoRefNo, @Comments)";

                DynamicParameters param = new DynamicParameters();
                param.Add("@LogId", logId);
                param.Add("@ShiftLogId", shiftId);
                param.Add("@Time",  log.Time);
                param.Add("@UserId", log.Operator.UserId);
                param.Add("@AreaId", log.AreaId);
                param.Add("@Tag", log.Tag);
                param.Add("@PID", log.PID);
                param.Add("@Description", log.Description);
                param.Add("@MaintenanceRequired", log.MaintenanceRequired);
                param.Add("@MaintenanceDescription", log.MaintenanceDescription);
                param.Add("@MaximoRefNo", log.MaximoRefNo);
                param.Add("@Comments", log.Comments);

                try
                {
                    int affectedRows = await db.ExecuteAsync(sql, param);

                }
                catch (Exception ex)
                {
                    throw;
                }


                return logId;
            }
        }

        public async Task<int?> GetShiftId(DateTime date, string shiftType)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"
                    SELECT m.ShiftId
                    FROM [ShiftLog_Master] m 
                    WHERE m.Date = @date And m.ShiftType = @shiftType";


               int? shiftId = (await db.QueryAsync<int?>(sql, new { date = date.Date, shiftType })).SingleOrDefault();

                return shiftId;
            }
        }

        public async Task<IEnumerable<ShiftLogOutput>> GetShiftLogs(DateTime date, string shiftType, string operatorId)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql = @"
                    SELECT 
	                    d.*,
                        u.Id as UserId, u.Name as UserName, a.Area as AreaName
                    FROM [ShiftLog_Master] m 
                    INNER JOIN [ShiftLog_Detail] d ON m.ShiftId = d.ShiftLogId
                    INNER JOIN [SSWA_Master].dbo.[User] u ON u.Id = d.UserId
                    LEFT JOIN [SSWA_Master].dbo.[Area] a ON a.AreaId = d.AreaId
                    WHERE m.Date = @date AND m.ShiftType = @shiftType
                    ORDER BY 
	                    CASE WHEN m.ShiftType = 'Night' AND d.[Time] >= '12:00:00' AND d.[Time] <= '23:59:59.9999' THEN 1 ELSE 2 END, 
	                    d.[Time]";


                IEnumerable<Entities.ShiftDetailRecord> logs = await db
                    .QueryAsync<Entities.ShiftDetailRecord>(sql, new { date = date.Date, shiftType,  operatorId });

                if (logs == null)
                    return null;

                List<ShiftLogOutput> results = new List<ShiftLogOutput>();

                foreach (var log in logs)
                {
                    results.Add(new ShiftLogOutput()
                    {
                        
                        AreaId = log.AreaId,
                        AreaName = log.AreaName,
                        Comments = log.Comments,
                        Description = log.Description,
                        LogId = log.LogId,
                        MaintenanceDescription = log.MaintenanceDescription,
                        MaintenanceRequired = log.MaintenanceRequired,
                        MaximoRefNo = log.MaximoRefNo,
                        PID = log.PID,
                        Tag = log.Tag,
                        Time = DateTime.MinValue.Add(log.Time),
                        Operator = new UserOutput(log.UserId, log.UserName, new Dictionary<string, bool>())
                    });
                }
                return results;
            }
        }

        public async Task<int> UpdateShift(ShiftOutput shift)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "UPDATE [ShiftLog_Master]" +
                    "SET  Swell = @Swell, MaxFCastWindSpeed = @MaxFCastWindSpeed, LowTideTime = @LowTideTime, Rain_Forecast = @RainForecast, Comments = @Comments, OHSE_Comments = @OHSEComments " +
                    "WHERE ShiftId = @ShiftId";

                DynamicParameters param = new DynamicParameters();
                param.Add("@ShiftId", shift.ShiftId);
                param.Add("@Swell", shift.Swell);
                param.Add("@MaxFCastWindSpeed", shift.MaxForecastWindSpeed);
                param.Add("@LowTideTime", shift.LowTideTime);
                param.Add("@RainForecast", shift.RainForecast);
                param.Add("@Comments", shift.Comments);
                param.Add("@OHSEComments", shift.OHSEComments);

                int affectedRows = await db.ExecuteAsync(sql, param);

                return affectedRows;
            }
        }

        public async Task<int> UpdateShiftLog(ShiftLogOutput log)
        {
            using (IDbConnection db = new SqlConnection(_connectionString))
            {
                string sql =
                    "UPDATE [ShiftLog_Detail] " +
                    "SET Time = @Time, AreaId = @AreaId, Tag = @Tag, PID = @PID, Description = @Description, MaintenanceRequired = @MaintenanceRequired, " +
                    "   MaintenanceDescription = @MaintenanceDescription, MaximoRefNo = @MaximoRefNo, Comments = @Comments " +
                    "WHERE LogId = @LogId";

                DynamicParameters p = new DynamicParameters();
                p.Add("@LogId", log.LogId);
                p.Add("@Time", log.Time);
                p.Add("@AreaId", log.AreaId);
                p.Add("@Tag", log.Tag);
                p.Add("@PID", log.PID);
                p.Add("@Description", log.Description);
                p.Add("@MaintenanceRequired", log.MaintenanceRequired);
                p.Add("@MaintenanceDescription", log.MaintenanceDescription);
                p.Add("@MaximoRefNo", log.MaximoRefNo);
                p.Add("@Comments", log.Comments);

                try
                {
                    int affectedRows = await db.ExecuteAsync(sql, p);
                    return affectedRows;

                }
                catch (Exception ex)
                {

                    throw;
                }


            }
        }
    }
}
