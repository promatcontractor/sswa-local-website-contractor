﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess
{
    public class Module : Autofac.Module
    {
        public string ConnectionString { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            //
            // Register all Types in Application DapperDataAccess namespace
            //
            builder.RegisterAssemblyTypes(typeof(InfrastructureException).Assembly)
                .Where(type => type.Namespace.Contains("DapperApplicationDataAccess"))
                .WithParameter("connectionString", ConnectionString)
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
        }
    }
}
