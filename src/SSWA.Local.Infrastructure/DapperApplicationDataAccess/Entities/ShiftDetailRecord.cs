﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Entities
{
    public class ShiftDetailRecord
    {
        public int LogId { get; set; }
        
        public int ShiftLogId { get; set; }

        public TimeSpan Time { get; set; }

        public int Operator { get; set; }

        public string AreaId { get; set; }

        public string AreaName { get; set; }

        public string Tag { get; set; }

        public string PID { get; set; }

        public string Description { get; set; }

        public bool MaintenanceRequired { get; set; }

        public string MaintenanceDescription { get; set; }

        public string MaximoRefNo { get; set; }

        public string Comments { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }
    }
}
