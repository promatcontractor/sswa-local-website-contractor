﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Entities
{
    public class PlantInstructionRecord
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public int? Duration { get; set; }

        public string DurationUnits { get; set; }

        public DateTime DateCreated { get; set; }

        public string OperatorUserId { get; set; }

        public int InstructionFromRoleId { get; set; }

        public string AuthorisedByUserId { get; set; }
    }
}
