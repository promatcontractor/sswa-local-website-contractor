﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Entities
{
    public class ChemicalProductOrderRecord
    {
        public int Id { get; set; }

        public int ChemicalProductId { get; set; }

        public string Status { get; set; }

        public DateTime? ExpectedDeliveryDate { get; set; }

        public TimeSpan? ExpectedDeliveryTime { get; set; }

        public string ArrivalAcknowledgedById { get; set; }

        public DateTime? IdentifiedAt { get; set; }

        public DateTime? OrderedAt { get; set; }

        public DateTime? ReceivedAt { get; set; }

        public string Comments { get; set; }
    }
}
