﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Entities
{
    public class InspectionOptionRecord
    {
        public string Option { get; set; }
    }
}
