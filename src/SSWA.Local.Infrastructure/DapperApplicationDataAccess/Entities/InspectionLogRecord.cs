﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Entities
{
    public class InspectionLogRecord
    {
        public InspectionLogRecord()
        {
            Options = new List<string>();
        }

        public int InspectionItemId { get; set; }
        public string InspectionItemName { get; set; }
        public string AreaId { get; set; }
        public string AreaName { get; set; }
        public string DataType { get; set; }
        public string Value { get; set; }
        public List<string> Options { get; set; }
    }
}
