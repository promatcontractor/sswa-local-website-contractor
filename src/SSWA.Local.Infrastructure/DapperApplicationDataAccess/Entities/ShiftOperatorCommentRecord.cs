﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SSWA.Local.Infrastructure.DapperApplicationDataAccess.Entities
{
    public class ShiftOperatorCommentRecord
    {
        public int ShiftId { get; set; }

        public int Operator { get; set; }

        public int? ReviewdBy { get; set; }

        public string Comments { get; set; }

        public string OHSEComments { get; set; }
    }
}
